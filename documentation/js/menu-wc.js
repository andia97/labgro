'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">agronomia-adscripcion documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-dacf4eff16c00a12e1e3cc97c0487fbd"' : 'data-target="#xs-components-links-module-AppModule-dacf4eff16c00a12e1e3cc97c0487fbd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-dacf4eff16c00a12e1e3cc97c0487fbd"' :
                                            'id="xs-components-links-module-AppModule-dacf4eff16c00a12e1e3cc97c0487fbd"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AreaModule.html" data-type="entity-link" >AreaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AreaModule-f3d8a3ac96f00a2d69dfe84df4dfb8c0"' : 'data-target="#xs-components-links-module-AreaModule-f3d8a3ac96f00a2d69dfe84df4dfb8c0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AreaModule-f3d8a3ac96f00a2d69dfe84df4dfb8c0"' :
                                            'id="xs-components-links-module-AreaModule-f3d8a3ac96f00a2d69dfe84df4dfb8c0"' }>
                                            <li class="link">
                                                <a href="components/AddeditAreaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddeditAreaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AreaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AreaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleEntryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleEntryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeliveryNoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DeliveryNoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AreaRoutingModule.html" data-type="entity-link" >AreaRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ArticleModule.html" data-type="entity-link" >ArticleModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ArticleModule-7d24f0c26a128382bcb133c95408c227"' : 'data-target="#xs-components-links-module-ArticleModule-7d24f0c26a128382bcb133c95408c227"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ArticleModule-7d24f0c26a128382bcb133c95408c227"' :
                                            'id="xs-components-links-module-ArticleModule-7d24f0c26a128382bcb133c95408c227"' }>
                                            <li class="link">
                                                <a href="components/AddArticuloComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddArticuloComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditEntryarticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddEditEntryarticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddeditCategoriaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddeditCategoriaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TblEntryArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TblEntryArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TblouputComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TblouputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TblsaldosComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TblsaldosComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ArticleRoutingModule.html" data-type="entity-link" >ArticleRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConsolidationModule.html" data-type="entity-link" >ConsolidationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConsolidationModule-b0b73ef07927231e789dff7bf3798bc5"' : 'data-target="#xs-components-links-module-ConsolidationModule-b0b73ef07927231e789dff7bf3798bc5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConsolidationModule-b0b73ef07927231e789dff7bf3798bc5"' :
                                            'id="xs-components-links-module-ConsolidationModule-b0b73ef07927231e789dff7bf3798bc5"' }>
                                            <li class="link">
                                                <a href="components/ConsolidationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConsolidationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConsolidationRoutingModule.html" data-type="entity-link" >ConsolidationRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link" >HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-43ba5a5063ff14e51ea941f22bbd0696"' : 'data-target="#xs-components-links-module-HomeModule-43ba5a5063ff14e51ea941f22bbd0696"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-43ba5a5063ff14e51ea941f22bbd0696"' :
                                            'id="xs-components-links-module-HomeModule-43ba5a5063ff14e51ea941f22bbd0696"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductionHistoryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductionHistoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RawMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RawMaterialComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SaleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SaleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SaleDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SaleDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StatisticalDataComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StatisticalDataComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link" >HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/InformesModule.html" data-type="entity-link" >InformesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-InformesModule-79c0229df0a8cec43ee333abac30c79d"' : 'data-target="#xs-components-links-module-InformesModule-79c0229df0a8cec43ee333abac30c79d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InformesModule-79c0229df0a8cec43ee333abac30c79d"' :
                                            'id="xs-components-links-module-InformesModule-79c0229df0a8cec43ee333abac30c79d"' }>
                                            <li class="link">
                                                <a href="components/InformesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InformesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PeripheralInventoryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PeripheralInventoryComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InformesRoutingModule.html" data-type="entity-link" >InformesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/InputOutputModule.html" data-type="entity-link" >InputOutputModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-InputOutputModule-73365179c91f7c308592117ef4260479"' : 'data-target="#xs-components-links-module-InputOutputModule-73365179c91f7c308592117ef4260479"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InputOutputModule-73365179c91f7c308592117ef4260479"' :
                                            'id="xs-components-links-module-InputOutputModule-73365179c91f7c308592117ef4260479"' }>
                                            <li class="link">
                                                <a href="components/AddIncomeArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddIncomeArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateNoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CreateNoteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DataListNoteContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DataListNoteContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditIncomeArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditIncomeArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditNoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditNoteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FirstStepComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FirstStepComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IncomeDetailsContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IncomeDetailsContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LayoutNotesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LayoutNotesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SecondStepComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SecondStepComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InputOutputRoutingModule.html" data-type="entity-link" >InputOutputRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link" >MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NotificacionesRoutingModule.html" data-type="entity-link" >NotificacionesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationModule.html" data-type="entity-link" >NotificationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationModule-e3d8e9b3c7e5562d8962de5db87f5112"' : 'data-target="#xs-components-links-module-NotificationModule-e3d8e9b3c7e5562d8962de5db87f5112"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationModule-e3d8e9b3c7e5562d8962de5db87f5112"' :
                                            'id="xs-components-links-module-NotificationModule-e3d8e9b3c7e5562d8962de5db87f5112"' }>
                                            <li class="link">
                                                <a href="components/NotificationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NotificationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotificationDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NotificationDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OutputModule.html" data-type="entity-link" >OutputModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-OutputModule-d3c7f8927caaa79446849eaef973197e"' : 'data-target="#xs-components-links-module-OutputModule-d3c7f8927caaa79446849eaef973197e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-OutputModule-d3c7f8927caaa79446849eaef973197e"' :
                                            'id="xs-components-links-module-OutputModule-d3c7f8927caaa79446849eaef973197e"' }>
                                            <li class="link">
                                                <a href="components/AddOutArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddOutArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditOutArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditOutArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OutDetailsContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OutDetailsContainer</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OutputRoutingModule.html" data-type="entity-link" >OutputRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PerfilModule.html" data-type="entity-link" >PerfilModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PerfilModule-81646b7946cf198857ddce87d32351cf"' : 'data-target="#xs-components-links-module-PerfilModule-81646b7946cf198857ddce87d32351cf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PerfilModule-81646b7946cf198857ddce87d32351cf"' :
                                            'id="xs-components-links-module-PerfilModule-81646b7946cf198857ddce87d32351cf"' }>
                                            <li class="link">
                                                <a href="components/PerfilComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PerfilComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PerfilRoutingModule.html" data-type="entity-link" >PerfilRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProducerModule.html" data-type="entity-link" >ProducerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProducerModule-811584ebd522249d0bc93088d19a27ae"' : 'data-target="#xs-components-links-module-ProducerModule-811584ebd522249d0bc93088d19a27ae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProducerModule-811584ebd522249d0bc93088d19a27ae"' :
                                            'id="xs-components-links-module-ProducerModule-811584ebd522249d0bc93088d19a27ae"' }>
                                            <li class="link">
                                                <a href="components/LayoutProducerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LayoutProducerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProducerRoutingModule.html" data-type="entity-link" >ProducerRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionDetailModule.html" data-type="entity-link" >ProductionDetailModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductionDetailModule-9173b8418c573d655b4cb4d6ef4aec68"' : 'data-target="#xs-components-links-module-ProductionDetailModule-9173b8418c573d655b4cb4d6ef4aec68"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductionDetailModule-9173b8418c573d655b4cb4d6ef4aec68"' :
                                            'id="xs-components-links-module-ProductionDetailModule-9173b8418c573d655b4cb4d6ef4aec68"' }>
                                            <li class="link">
                                                <a href="components/AddUnitProductionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddUnitProductionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CalendarProductionContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CalendarProductionContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditUnitProductionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditUnitProductionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/KardexProductionContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >KardexProductionContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductionDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductionDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionDetailRoutingModule.html" data-type="entity-link" >ProductionDetailRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionModule.html" data-type="entity-link" >ProductionModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductionModule-df891c338439f4d16f72d907f3b399aa"' : 'data-target="#xs-components-links-module-ProductionModule-df891c338439f4d16f72d907f3b399aa"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductionModule-df891c338439f4d16f72d907f3b399aa"' :
                                            'id="xs-components-links-module-ProductionModule-df891c338439f4d16f72d907f3b399aa"' }>
                                            <li class="link">
                                                <a href="components/AddEditArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddEditArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddPresentationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddPresentationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddeditUnitComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddeditUnitComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticlesSuppliesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticlesSuppliesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateProductComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CreateProductComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DataArticlesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DataArticlesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DataPresentationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DataPresentationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditPresentationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditPresentationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditProductComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditProductComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GeneralDataComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GeneralDataComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HistoryDataComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HistoryDataComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductLayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UnitsPresentationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UnitsPresentationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionRoutingModule.html" data-type="entity-link" >ProductionRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionSummaryModule.html" data-type="entity-link" >ProductionSummaryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductionSummaryModule-e29358a71c057c8d96762fad92079416"' : 'data-target="#xs-components-links-module-ProductionSummaryModule-e29358a71c057c8d96762fad92079416"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductionSummaryModule-e29358a71c057c8d96762fad92079416"' :
                                            'id="xs-components-links-module-ProductionSummaryModule-e29358a71c057c8d96762fad92079416"' }>
                                            <li class="link">
                                                <a href="components/ProductionSummaryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductionSummaryComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductionSummaryRoutingModule.html" data-type="entity-link" >ProductionSummaryRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SalesDetailModule.html" data-type="entity-link" >SalesDetailModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SalesDetailModule-30c840872e2bb896552e881b05b81255"' : 'data-target="#xs-components-links-module-SalesDetailModule-30c840872e2bb896552e881b05b81255"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SalesDetailModule-30c840872e2bb896552e881b05b81255"' :
                                            'id="xs-components-links-module-SalesDetailModule-30c840872e2bb896552e881b05b81255"' }>
                                            <li class="link">
                                                <a href="components/AddSaleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddSaleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditSaleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditSaleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SalesDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SalesDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SalesDetailRoutingModule.html" data-type="entity-link" >SalesDetailRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SalesSummaryModule.html" data-type="entity-link" >SalesSummaryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SalesSummaryModule-0e55d62f62afc838b03d8a29fa7e0863"' : 'data-target="#xs-components-links-module-SalesSummaryModule-0e55d62f62afc838b03d8a29fa7e0863"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SalesSummaryModule-0e55d62f62afc838b03d8a29fa7e0863"' :
                                            'id="xs-components-links-module-SalesSummaryModule-0e55d62f62afc838b03d8a29fa7e0863"' }>
                                            <li class="link">
                                                <a href="components/SalesSummaryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SalesSummaryComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SalesSummaryRoutingModule.html" data-type="entity-link" >SalesSummaryRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' : 'data-target="#xs-components-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' :
                                            'id="xs-components-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                            <li class="link">
                                                <a href="components/ConstructionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConstructionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EmptyStateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EmptyStateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/KardexConfigurationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >KardexConfigurationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MainNavComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MainNavComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MonthComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MonthComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/YearComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >YearComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' : 'data-target="#xs-directives-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' :
                                        'id="xs-directives-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                        <li class="link">
                                            <a href="directives/AppRoleDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppRoleDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/AppRolePDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppRolePDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/AutofocusDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AutofocusDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' : 'data-target="#xs-injectables-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' :
                                        'id="xs-injectables-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                        <li class="link">
                                            <a href="injectables/SideNavService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SideNavService</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' : 'data-target="#xs-pipes-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' :
                                            'id="xs-pipes-links-module-SharedModule-85542b47ae7549e403b26ee1af9e76a8"' }>
                                            <li class="link">
                                                <a href="pipes/SearchPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SearchPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WarehouseModule.html" data-type="entity-link" >WarehouseModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' : 'data-target="#xs-components-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' :
                                            'id="xs-components-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' }>
                                            <li class="link">
                                                <a href="components/AddeditUnitmasureComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddeditUnitmasureComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LayoutWarehouseComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LayoutWarehouseComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' : 'data-target="#xs-injectables-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' :
                                        'id="xs-injectables-links-module-WarehouseModule-eaea2d5d71b1a3bf87f233a8e24263c9"' }>
                                        <li class="link">
                                            <a href="injectables/SideNavService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SideNavService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/WarehouseRoutingModule.html" data-type="entity-link" >WarehouseRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/WarehousesModule.html" data-type="entity-link" >WarehousesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-WarehousesModule-f2d74d8d38815f6bff9eb2b9e9c6bfb1"' : 'data-target="#xs-components-links-module-WarehousesModule-f2d74d8d38815f6bff9eb2b9e9c6bfb1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-WarehousesModule-f2d74d8d38815f6bff9eb2b9e9c6bfb1"' :
                                            'id="xs-components-links-module-WarehousesModule-f2d74d8d38815f6bff9eb2b9e9c6bfb1"' }>
                                            <li class="link">
                                                <a href="components/AddArticleComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddArticleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleDetailComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleIncomeContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleIncomeContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleLayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleMonthDataComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleMonthDataComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ArticleOutputContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ArticleOutputContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditArticleCartComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditArticleCartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditArticleWarehouseComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditArticleWarehouseComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PresentationContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PresentationContainer</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShoppingCartComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ShoppingCartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SuppliesContainer.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SuppliesContainer</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WarehousesRoutingModule.html" data-type="entity-link" >WarehousesRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/AddArticleComponent-1.html" data-type="entity-link" >AddArticleComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/AddPresentationComponent-1.html" data-type="entity-link" >AddPresentationComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/CreateNoteComponent-1.html" data-type="entity-link" >CreateNoteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DataListNoteContainer-1.html" data-type="entity-link" >DataListNoteContainer</a>
                            </li>
                            <li class="link">
                                <a href="components/EditNoteComponent-1.html" data-type="entity-link" >EditNoteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/EditPresentationComponent-1.html" data-type="entity-link" >EditPresentationComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FirstStepComponent-1.html" data-type="entity-link" >FirstStepComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/HomeComponent-1.html" data-type="entity-link" >HomeComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/LayoutNotesComponent-1.html" data-type="entity-link" >LayoutNotesComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ProductionSummaryComponent-1.html" data-type="entity-link" >ProductionSummaryComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/SecondStepComponent-1.html" data-type="entity-link" >SecondStepComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthConstants.html" data-type="entity-link" >AuthConstants</a>
                            </li>
                            <li class="link">
                                <a href="classes/Rol.html" data-type="entity-link" >Rol</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AreaService.html" data-type="entity-link" >AreaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ArticleService.html" data-type="entity-link" >ArticleService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CategoriaService.html" data-type="entity-link" >CategoriaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FirestorageService.html" data-type="entity-link" >FirestorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HttpService.html" data-type="entity-link" >HttpService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/IncomesArticleService.html" data-type="entity-link" >IncomesArticleService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoginService.html" data-type="entity-link" >LoginService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MaterialService.html" data-type="entity-link" >MaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessageService.html" data-type="entity-link" >MessageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrderService.html" data-type="entity-link" >OrderService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OutputArticleService.html" data-type="entity-link" >OutputArticleService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PresentationService.html" data-type="entity-link" >PresentationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProductionService.html" data-type="entity-link" >ProductionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProductService.html" data-type="entity-link" >ProductService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ShoppingCartService.html" data-type="entity-link" >ShoppingCartService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SideNavService.html" data-type="entity-link" >SideNavService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageService.html" data-type="entity-link" >StorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TokenService.html" data-type="entity-link" >TokenService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UnitMeasureService.html" data-type="entity-link" >UnitMeasureService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UploadService.html" data-type="entity-link" >UploadService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/InterceptorMessage.html" data-type="entity-link" >InterceptorMessage</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/InterceptorMessage-1.html" data-type="entity-link" >InterceptorMessage</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/InterceptorToken.html" data-type="entity-link" >InterceptorToken</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/UserGuardGuard.html" data-type="entity-link" >UserGuardGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/UserGuestGuard.html" data-type="entity-link" >UserGuestGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Area.html" data-type="entity-link" >Area</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Article.html" data-type="entity-link" >Article</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ArticleArea.html" data-type="entity-link" >ArticleArea</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ArticleIncome.html" data-type="entity-link" >ArticleIncome</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ArticleOut.html" data-type="entity-link" >ArticleOut</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ArticleProducer.html" data-type="entity-link" >ArticleProducer</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthUser.html" data-type="entity-link" >AuthUser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Category.html" data-type="entity-link" >Category</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EmptyData.html" data-type="entity-link" >EmptyData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/InputNote.html" data-type="entity-link" >InputNote</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MaterialProducer.html" data-type="entity-link" >MaterialProducer</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/OutputNote.html" data-type="entity-link" >OutputNote</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-1.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-2.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-3.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-4.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-5.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PeriodicElement-6.html" data-type="entity-link" >PeriodicElement</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Presentation.html" data-type="entity-link" >Presentation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PresentationProducer.html" data-type="entity-link" >PresentationProducer</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PresentationUnitProduct.html" data-type="entity-link" >PresentationUnitProduct</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Product.html" data-type="entity-link" >Product</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/tipo.html" data-type="entity-link" >tipo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/tipo-1.html" data-type="entity-link" >tipo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/unidad.html" data-type="entity-link" >unidad</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UnitMeasure.html" data-type="entity-link" >UnitMeasure</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});