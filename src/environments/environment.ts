// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiUrl: 'https://test-api-agrosoft.herokuapp.com/api/',
  // apiUrl: 'http://127.0.0.1:8000/api/',
  production: false,
  firebase: {
    apiKey: "AIzaSyAUoz1FSRapOXH0n_D5rJUxX9b-6GeVU5A",
    authDomain: "agrosoft-44415.firebaseapp.com",
    projectId: "agrosoft-44415",
    storageBucket: "agrosoft-44415.appspot.com",
    messagingSenderId: "1078863829669",
    appId: "1:1078863829669:web:e52a490d8f69f9feca4b4c",
    measurementId: "G-CVWHG33G6L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
