import { Component, OnInit,Inject} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2'

import { Category } from '@core/models/category.model';
import { CategoriaService } from '@core/services/categoria/categoria.service';
import { HomeComponent } from '@producer/components/home/home.component';
import { MessageService } from '@core/services/menssage/message.service';


@Component({
  selector: 'app-addedit-categoria',
  templateUrl: './addedit-categoria.component.html',
  styleUrls: ['./addedit-categoria.component.css']
})
export class AddeditCategoriaComponent implements OnInit {

  main!:HomeComponent;
  name!: string ;
  public  fromCategory!: FormGroup;
  
  errorMessage: string = '';
  stateError: Boolean = false;


 category! : Category;
 categories!: Category[];
 mensagge!: boolean;
  
  constructor(
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AddeditCategoriaComponent>,
    private formBuilder: FormBuilder,
    private serviceCategory: CategoriaService,
    private route:Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private serviceMessage: MessageService
  ) { 
    this.main = this.dialogRef._containerInstance._config.data
    this.category = data.obj
  }

  
  ngOnInit(): void {
    this.buildForm();
    this.getCategories();
    if(this.data.name === "Modificar"){
        this.setValue();
    }
    console.log(this.data.obj);
  }


  onClose(accion : string) {
     this.dialogRef.close({event:accion,data:this.data.obj});
  }

  onSubmit(accion : string,name: string){
     console.log(name);
    console.log(accion);
    switch (accion) { 
      case 'Crear':
        this.saveCategory(name);
      break;

      case 'Modificar':
        this.editCategory(name);
      break;

      case 'Eliminar':
       this.deleteCategory();
      break; 
    }
  }

  setValue(){
    this.fromCategory.setValue( {
      name : this.category.name
    }) 
  }

  deleteCategory(){
    this.serviceCategory.deleteCategory(this.category.id+"")
      .subscribe(
         (valor : any ) =>{
           console.log(valor)
          if(valor.articles){
            this.sendMenssage(valor);
          }else{
            this.dialogRef.close({event:this.data.name,data:this.data.obj});
          }
           
          
        },
        (error) =>{
          console.log(error)
          this.errorMessage = this.serviceMessage.get();
          this.stateError = true;
        }
     )
  }

  sendMenssage(valor: any){
    if(valor.article == [] ){
      this.dialogRef.close({event:this.data.name,data:this.data.obj});
     }else{
       let nameAr = "";
      this.stateError = true;
      for(let article of valor.articles){
         nameAr = nameAr + article.name_article +" ,"
      }
      this.errorMessage = valor.message + " : " + nameAr

     }
  }


  saveCategory(name:string){
    var categoria  =  {} as Category;
    if(this.fromCategory.valid){
      categoria.name = name;
      this.serviceCategory.addCategory(categoria).subscribe(
      (valor : any) => {
          if ( this.serviceMessage.get() === "fetched"){
            console.log(categoria);
            this.dialogRef.close({event:this.data.name,data:this.data.obj})
            Swal.fire({
                position: 'center',
                icon: 'success',
                iconColor: '#85CE36',
                title: valor.message,
                showConfirmButton: false,
                 timer: 2000
            })
          }
      },
      (error : any) => {
        this.errorMessage = this.serviceMessage.get();
        this.stateError = true;
        console.log(this.errorMessage);
      }
      );
     }
     
     
  }
  

  editCategory(name:string){
    if(this.fromCategory.valid){
      this.category.name = name;
      this.serviceCategory.updateCategory(this.category.id+"",this.category)
      .subscribe(
         (valor:any) =>{
          console.log(valor)
            if ( this.serviceMessage.get() === "fetched update"){
            this.dialogRef.close({event:this.data.name,data:this.data.obj})
            }
          },
          (error:any)=>{
            this.errorMessage = this.serviceMessage.get();
            this.stateError = true;
          }
      )
    }
  }


  getCategories(){
    this.serviceCategory.getCategories().
    subscribe(
     (valor) =>{
      this.categories = valor;
      console.log(valor);
     }
    )
  }
  
  public inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9_ ]+$/;   
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z0-9_ ]/g,"");
      }
  }

  private buildForm() {
    this.fromCategory = this.formBuilder.group({
      name : ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30)
      ]],
      
    });
  }
}
