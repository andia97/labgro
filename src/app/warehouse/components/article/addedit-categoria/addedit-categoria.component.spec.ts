import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditCategoriaComponent } from './addedit-categoria.component';

describe('AddeditCategoriaComponent', () => {
  let component: AddeditCategoriaComponent;
  let fixture: ComponentFixture<AddeditCategoriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddeditCategoriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
