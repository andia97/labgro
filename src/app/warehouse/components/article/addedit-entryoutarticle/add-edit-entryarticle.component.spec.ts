import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEntryarticleComponent } from './add-edit-entryarticle.component';

describe('AddEditEntryarticleComponent', () => {
  let component: AddEditEntryarticleComponent;
  let fixture: ComponentFixture<AddEditEntryarticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditEntryarticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEntryarticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
