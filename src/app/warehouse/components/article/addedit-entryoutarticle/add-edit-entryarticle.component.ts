import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-edit-entryarticle',
  templateUrl: './add-edit-entryarticle.component.html',
  styleUrls: ['./add-edit-entryarticle.component.css']
})
export class AddEditEntryarticleComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddEditEntryarticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }
  
  onClose(status: string){
    this.dialogRef.close({event:this.data.name,data:this.data.obj})
  }
}
