import { AfterViewInit, Component, ElementRef, EventEmitter, Input, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { Observable } from 'rxjs';
import { AddEditEntryarticleComponent } from '../addedit-entryoutarticle/add-edit-entryarticle.component';




@Component({
  selector: 'app-tbl-entry-article',
  templateUrl: './tbl-entry-article.component.html',
  styleUrls: ['./tbl-entry-article.component.css']
})
export class TblEntryArticleComponent implements AfterViewInit {
  
  data!: any[];

  columnsEntry!: string[];

 
  idArticle!: string;

  //@ViewChild(MatPaginator) paginator!: MatPaginator;
 // @ViewChild(MatSort) sort!: MatSort;
 // @ViewChild(MatTable) table!: MatTable<any>;

  //@ViewChild('tbl') tbl!: ElementRef;

  dataSource = new MatTableDataSource<any>(this.data);
  dataState: boolean = false;
  DATA: any;
  currentPage: number = 0;
  mounthone: number = 0;
  mountTwo: number = 0; 
  year: number = 0;
  state: boolean =  true;


  private eventsSubscription: any;


  displayedColumnsEntry = ['Nº','Fecha','Comprobante','Origen' ,'Cantidad','Importe','Precio-Medio'];
  constructor(
    private dialog: MatDialog,
    private serviceIncomeArticle:  IncomesArticleService,
    private routeActive: ActivatedRoute,
  ) {
    console.log(this.dataSource.data)
  }
  
  
  ngOnInit(): void {
    this.idArticle =  this.routeActive.snapshot.params.id;
    this.columnsEntry = this.displayedColumnsEntry;
    this.getArticleIncome();
   // this.eventsSubscription = this.event.subscribe(() => doSomething())
    
  }


  ngAfterViewInit(): void {
   //this.dataSource.sort = this.sort;
   // this.dataSource.paginator = this.paginator;
    //this.table.dataSource = this.dataSource;
    console.log(this.columnsEntry)
  }

  openUAddEditComponentEntry(name: string,obj:any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name, obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddEditEntryarticleComponent, dialogConfig)
    console.log("open dialog");
    dialogRef.afterClosed().subscribe(
      (result) =>{
        console.log(result);
        if(result.event !== "Close"){
           console.log("hello");
        } 
      }
    )
      
  }
  
  getTbl(){
    this.DATA = document.getElementById('htmlData');
    console.log(this.DATA)
  }


  getArticleIncome(){
   this.serviceIncomeArticle.getIncomeArticle(this.idArticle, this.mounthone, this.mountTwo, this.year).
    subscribe( 
      (value : any) => {
        if(value ===  [] ){
          this.state = true ;
          this.dataState = false;
          console.log(value);
        }else{
          this.state = false;
          this.data = value.incomes;
          console.log(value.incomes)
          this.dataSource = new MatTableDataSource<any>(value.incomes);
          this.dataState = true;
        }
       }
    )
    
  }

  
  filterDate(event: number, option: string): void {
    if (option == 'month') {
      console.log(event);
      this.mounthone = event; 
    } else {
      this.year = + event; 
    }
    this.currentPage = 0;
   //   this.paginator.pageIndex = 0;
   //  console.log(this.month, this.year);
   // this.getOutNotes();

   switch(option){
     case 'month':
       this.mounthone= event;
       break;
     case 'monthTwo':
       this.mountTwo = event;
       break;
     case 'year': 
       this.year = + event;
       break;
   }
  }


  convertMonth(actualMonth: string): number {
    let month;
    switch (actualMonth) {
      case 'enero':
        month = 1;
        break;
      case 'febrero':
        month = 2;
        break;
      case 'marzo':
        month = 3;
        break;
      case 'abril':
        month = 4;
        break;
      case 'mayo':
        month = 5;
        break;
      case 'junio':
        month = 6;
        break;
      case 'julio':
        month = 7;
        break;
      case 'agosto':
        month = 8;
        break;
      case 'septiembre':
        month = 9;
        break;
      case 'octubre':
        month = 10;
        break;
      case 'noviembre':
        month = 11;
        break;
      case 'diciembre':
        month = 12;
        break;
        default:
        month = 0;
      break;
    }
    return month;
  }



}

