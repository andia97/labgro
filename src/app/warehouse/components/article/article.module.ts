import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleRoutingModule } from './article-routing.module';
import { MaterialModule } from '@material/material.module';
import { HomeComponent } from './home/home.component';
import { AddArticuloComponent } from './add-articulo/add-articulo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { AddeditCategoriaComponent } from './addedit-categoria/addedit-categoria.component';
import { ArticleComponent } from './article-details/article.component';
import { TblEntryArticleComponent } from './tbl-details-article/tbl-entry-article.component';
import { AddEditEntryarticleComponent } from './addedit-entryoutarticle/add-edit-entryarticle.component';
import { TblouputComponent } from './tblouput/tblouput.component';
import { TblsaldosComponent } from './tblsaldos/tblsaldos.component';

@NgModule({
  declarations: [
    HomeComponent,
    AddArticuloComponent,
    AddeditCategoriaComponent,
    ArticleComponent,
    TblEntryArticleComponent,
    AddEditEntryarticleComponent,
    TblouputComponent,
    TblsaldosComponent
  ],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule
  ]
})
export class ArticleModule { }
