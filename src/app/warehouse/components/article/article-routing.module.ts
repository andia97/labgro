import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './article-details/article.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
{ 
  path: '', 
  component: HomeComponent,
  
},
{
  path: ':id',
  component: ArticleComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
