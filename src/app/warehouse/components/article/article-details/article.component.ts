import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { AddEditEntryarticleComponent } from '../addedit-entryoutarticle/add-edit-entryarticle.component';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TblEntryArticleComponent } from '../tbl-details-article/tbl-entry-article.component';
import { TblouputComponent } from '../tblouput/tblouput.component';
import * as _moment from 'moment';
import { TblsaldosComponent } from '../tblsaldos/tblsaldos.component';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { OutputArticleService } from '@core/services/output-article/out-article.service';
import {MatTableModule} from '@angular/material/table';


const moment = _moment;
moment.locale('es');


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];


  @ViewChild('tblentry')
  componentblEntry!: TblEntryArticleComponent;

  @ViewChild('tblouput')
  componentblOuput!: TblouputComponent;

  @ViewChild('tblsaldo')
  componenSaldo!: TblsaldosComponent;
   
  @ViewChild('kardex')
  tableKardex!: MatTableModule;

  private eventsSubject: Subject<void> = new Subject<void>();

  tabStatus: number = 0; 
  dataEntry!: any[];
  dataOuput!: any[];

  dtaSaldo: any[]= [];
  stateDownload: boolean = false;

  dtSaldo: any[] = [];

  DATA: any;
  currentPage: number = 0;
  mounthone: number = 0 ;
  mountTwo: number = 0; 
  year: number = 0;
  
  actualDate = moment()

  clickStatus: number = 0;

  article!: Article;
  article$!: Observable<any>
  dataState: boolean = false;
  state: boolean = false;
  btnState: boolean = false;
  status: string = "";
  columnsEntry!: string[];
  idArticle!: string; 
  formSaldo!: FormGroup;
  selectedIndex = 0;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  constructor(
    private serviceArticle: ArticleService,
    private routeActive: ActivatedRoute,
    private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
    private formBuild : FormBuilder ,
    private serviceIncomeArticle:  IncomesArticleService,
    private serviceOutputArticle: OutputArticleService,
  ) {
    
    this.columnsEntry = [];
    const actualMonth = this.actualDate.format('MMMM');
    const actualYear = this.actualDate.format('YYYY');
     this.year = + actualYear;
     this.mounthone = this.convertMonth(actualMonth);
     this.mountTwo = this.convertMonth(actualMonth);
  }

  ngOnInit(): void {
    this.article$ = this.getArticle();
  }

  getArticle() {
     this.idArticle = this.routeActive.snapshot.params.id
     
    return this.serviceArticle.getArticle(+this.idArticle)
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    console.log(tabChangeEvent.index);

    switch (tabChangeEvent.index) {
      case 2:
        this.btnState = true;
        if(this.clickStatus == 0){
          this.fillBalanceOputs()
         this.fillInBalenceInputs();
          this.componenSaldo.crearun();
         this.clickStatus++;
         this.tabStatus = 2;
        }
        break
      case 1:
        this.btnState = true;
        if(this.clickStatus == 0){
          this.fillBalanceOputs()
         this.fillInBalenceInputs();
          this.componenSaldo.crearun();
         this.clickStatus++;
        }
        this.tabStatus = 1; 
        break
      case 0:
        this.btnState = true;
        if(this.clickStatus == 0){
          this.fillBalanceOputs()
         this.fillInBalenceInputs();
          this.componenSaldo.crearun();
         this.clickStatus++;
        }
        this.tabStatus = 0;
        break

    }
  }


  openUAddEditComponentEntry(name: string, obj: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = { name, obj };
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddEditEntryarticleComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result) => {
        console.log(result);
        if (result.event !== "Close") {
          console.log("hello");
        }
      }
    )

  }

  public downloadPDF(): void {

   //  this.selectedIndex = 1;

    this.btnState = true;
    if(this.clickStatus == 0){
      this.fillBalanceOputs()
     this.fillInBalenceInputs();
      this.componenSaldo.crearun();
     this.clickStatus++;
    }

    
    this.stateDownload = true;
    console.log(this.tableKardex);

   if(this.tabStatus === 0){
     console.log(this.tabStatus)
    this.DATA = document.getElementById('entrytbl');  
   }  
    if(this.tabStatus === 1){
   this.DATA = document.getElementById('outputtbl');
    }
    if(this.tabStatus === 2){
   this.DATA = document.getElementById('kardex');
    }
   const doc = new jsPDF('L', 'mm', 'A4');
   this. componentblEntry.getTbl()
    console.log(this.DATA);
  
    const options = {
      background: 'white',
      scale: 3
    }

    html2canvas(this.DATA, options).then(
      (canvas) => { 
      const img = canvas.toDataURL('image/PNG');
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      this.stateDownload = false; 
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }
  

  fillinSaldo(){
  }

  emitEventToChild() {
    this.eventsSubject.next()
  }

   
  filterDate(event: number, option: string): void {
   switch(option){
     case 'month':
      this.mounthone= event;
        this.componentblEntry.mounthone = this.mounthone;
        this.componentblOuput.mounthone = this.mounthone;
        this.componentblEntry.mountTwo = this.mountTwo;
        this.componentblOuput.mountTwo = this.mountTwo;
        this.componentblEntry.year = this.year;
        this.componentblOuput.year = this.year;
      //}
       break;
     case 'monthTwo':
       this.mountTwo = event;
       this.componentblEntry.mountTwo = this.mountTwo;
       this.componentblOuput.mountTwo = this.mountTwo;
       this.componentblEntry.mounthone = this.mounthone;
       this.componentblOuput.mounthone = this.mounthone;
       this.componentblEntry.year = this.year;
       this.componentblOuput.year = this.year;
       break;
     case 'year': 
       this.year = + event;
       this.componentblEntry.year = this.year;
       this.componentblOuput.year = this.year;
       this.componentblEntry.mountTwo = this.mountTwo;
       this.componentblOuput.mountTwo = this.mountTwo;
       this.componentblEntry.mounthone = this.mounthone;
       this.componentblOuput.mounthone = this.mounthone;
       break;
   }
   
    if(this.mounthone > 0 && this.mountTwo > 0 &&  this.year > 0 ){
       this.componentblEntry.getArticleIncome();
       this.componentblOuput.getArticleOutputs();
       this.dataOuput = [];
       this.fillBalanceOputs()
       this.fillInBalenceInputs();
        this.componenSaldo.crearun();

    }
  }


  convertMonth(actualMonth: string): number {
    let month;
    switch (actualMonth) {
      case 'enero':
        month = 1;
        break;
      case 'febrero':
        month = 2;
        break;
      case 'marzo':
        month = 3;
        break;
      case 'abril':
        month = 4;
        break;
      case 'mayo':
        month = 5;
        break;
      case 'junio':
        month = 6;
        break;
      case 'julio':
        month = 7;
        break;
      case 'agosto':
        month = 8;
        break;
      case 'septiembre':
        month = 9;
        break;
      case 'octubre':
        month = 10;
        break;
      case 'noviembre':
        month = 11;
        break;
      case 'diciembre':
        month = 12;
        break;
        default:
        month = 0;
      break;
    }
    return month;
  }



  fillBalanceOputs(){
    this.dataOuput = this.componentblOuput.data;
    for (var i in this.dataOuput) {
      let saldoOut: any = {
        id : this.dataOuput[i].receipt,
        date : this.dataOuput[i].delivery_date,
        origin :  this.dataOuput[i].name,
        quantity : this.dataOuput[i].quantity,
        amount:   this.dataOuput[i].receipt,
        halfPrice: this.dataOuput[i].unit_price,
        import: this.dataOuput[i].budget_output
    };
    this.dtSaldo.push(saldoOut)   
    }
  }

   
  fillInBalenceInputs(){
    this.dataEntry =  this.componentblEntry.data;
    for (var j in this.dataEntry) {
      let saldoEntry: any = {
        id : this.dataEntry[j].receipt,
        date : this.dataEntry[j].created_at,
        origin : "Almacen",
        quantity : this.dataEntry[j].quantity,
        amount:  this.dataEntry[j].receipt,
        halfPrice: this.dataEntry[j].unit_price,
        import   :  this.dataEntry[j].total_price
     };
    this.dtSaldo.push(saldoEntry)   
    }
  }
  
  
}
