import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TblsaldosComponent } from './tblsaldos.component';

describe('TblsaldosComponent', () => {
  let component: TblsaldosComponent;
  let fixture: ComponentFixture<TblsaldosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TblsaldosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TblsaldosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
