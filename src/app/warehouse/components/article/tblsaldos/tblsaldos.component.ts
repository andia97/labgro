import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { OutputArticleService } from '@core/services/output-article/out-article.service';

@Component({
  selector: 'app-tblsaldos',
  templateUrl: './tblsaldos.component.html',
  styleUrls: ['./tblsaldos.component.css']
})
export class TblsaldosComponent implements OnInit {
  

  @Input()
   dtSaldo: any[] = [];
  
   dtaSaldo: any[]= [];
  dataSource = new MatTableDataSource<any>(this.dtaSaldo);

  columnsEntry = ['Nº','Fecha', 'Estado','Comprobante','Origen/Destino','Cantidad','Importe' ,'Precio-Medio'];
  idArticle!: string;
  
  currentPage: number = 1;
  mounthone!: number;
  mountTwo!: number; 
  year!: number;

  data!: any[];
  dataTwo!: any[];

  constructor(
    private serviceIncomeArticle:  IncomesArticleService,
    private serviceOutputArticle: OutputArticleService,
    private routeActive: ActivatedRoute,

  ) { 

   // this.getArticleIncome()
    //this.getArticleOutputs()
  }

  
  ngAfterViewInit(): void{
  
  }


  ngOnInit(): void {
  
    this.idArticle = this.routeActive.snapshot.params.id;
    
   
    console.log(this.dtaSaldo);
    this.dataSource = new MatTableDataSource<any>(this.dtaSaldo);
  }


  getArticleOutputs(){
    /*
    this.serviceOutputArticle.getOuputArticle(this.idArticle,this.currentPage,this.mounthone,this.mountTwo,this.year)
    .subscribe(
      (value : any )=>{
        this.data = value.outputs.data; 
        console.log(value.outputs.data);
        console.log(this.data);
        this.fillBalanceOputs();
      }
    )
    */
  }

 
  getArticleIncome(){
    /*
    this.serviceIncomeArticle.getIncomeArticle(this.idArticle,this.mounthone,this.mountTwo,this.year).
    subscribe( 
      (value : any)=> {
        this.dataTwo = value.incomes;
        console.log(this.dataTwo);
        this.fillInBalenceInputs();
      }
    )
    */
  }

  

  fillInBalenceInputs(){
    console.log(this.dataTwo);
    for (var j in this.dataTwo) {
      let saldoEntry: any = {
        id : this.dataTwo[j].receipt,
        date : this.dataTwo[j].created_at,
        origin : "Almacen",
        quantity : this.dataTwo[j].quantity,
        amount:  this.dataTwo[j].receipt,
        halfPrice: this.dataTwo[j].unit_price,
        import   :  this.dataTwo[j].total_price
     };
    this.dtaSaldo.push(saldoEntry)   
    }
    console.log(this.dtaSaldo)
  }


  fillBalanceOputs(){
    console.log(this.data);
    for (var i in this.data) {
      let saldoOut: any = {
        id : this.data[i].receipt,
        date : this.data[i].delivery_date,
        origin : this.data[i].name,
        quantity : this.data[i].quantity,
        amount:  this.data[i].receipt,
        halfPrice: this.data[i].unit_price,
        import: this.data[i].budget_output
    };
    this.dtaSaldo.push(saldoOut)   
    }
  }
  

  crearun(){
    this.dtSaldo.sort(function(a,b){
        var dateA = new Date(a.date).getTime();
        var dateB = new Date(b.date).getTime();
        return dateA > dateB ? 1 : -1;
      });
    console.log(this.dtaSaldo)
    this.dataSource = new MatTableDataSource<any>(this.dtSaldo);
  }
  

}
