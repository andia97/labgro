import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditUnitmasureComponent } from './addedit-unitmasure.component';

describe('AddeditUnitmasureComponent', () => {
  let component: AddeditUnitmasureComponent;
  let fixture: ComponentFixture<AddeditUnitmasureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddeditUnitmasureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditUnitmasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
