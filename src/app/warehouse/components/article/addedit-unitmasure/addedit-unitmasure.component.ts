import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { UnitMeasure } from '@core/models/unit_measure';
import { MessageService } from '@core/services/menssage/message.service';
import { UnitMeasureService } from '@core/services/unit_measure/unit-measure.service';
import Swal from 'sweetalert2'

export interface tipo {
  id : number;
  name : string;
}

@Component({
  selector: 'app-addedit-unitmasure',
  templateUrl: './addedit-unitmasure.component.html',
  styleUrls: ['./addedit-unitmasure.component.css']
})

export class AddeditUnitmasureComponent implements OnInit {

  public  fromUnit!: FormGroup; 

  tipos: tipo[] =[
    {id:2,name:'Capacidad'},
    {id:3,name:'Peso'},
    {id:4,name:'Otro'},
  ];

  message:String = "";
  error:boolean = false;
  selected = '';
  constructor(
    public dialogRef: MatDialogRef<AddeditUnitmasureComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private serviceMessage: MessageService,
    private serviceUnitmasure : UnitMeasureService
  ) { }

  ngOnInit(): void {
    
    this.buildForm();
    if(this.data.name === "Modificar"){
      console.log(this.data);
      this.setValueForm();
    }
  }

  onSubmit(accion: string){
    
    switch (accion) { 
      case 'Crear':
        this.addUnit(); 
      break;
      case 'Modificar':
        this.editUnit();
      break;
      case 'Eliminar':
        this.deleteUnit();   
    }
  }
  
  onClose(accion : string) {
    this.dialogRef.close({event:accion,data:this.data.obj})
  }

  deleteUnit(){
    this.serviceUnitmasure.deleteUnitMeasure(this.data.obj.id)
    .subscribe(
      (valor) =>{
        console.log(valor);
        this.dialogRef.close({event:this.data.name,data:this.data.obj})
      },
      (error) =>{
        this.error = true;
      }
    )
  }

  editUnit(){
    const unitMeasu = {} as UnitMeasure;
    unitMeasu.id = this.data.obj.id;
    unitMeasu.unit_measure = this.fromUnit.get('unit_measure')?.value; 
    unitMeasu.kind = this.fromUnit.get('kind')?.value;
    this.serviceUnitmasure.updateUnitMeasure(unitMeasu)
    .subscribe(
      (valor) =>{
        console.log(valor);
        this.dialogRef.close({event:this.data.name,data:unitMeasu})
      }, 
      (error) =>{
        console.log(error);
      }

    )
  }

  addUnit(){
      const unitMeasu = {} as UnitMeasure;
      unitMeasu.unit_measure = this.fromUnit.get('unit_measure')?.value; 
      unitMeasu.kind = this.fromUnit.get('kind')?.value;
      this.serviceUnitmasure.addUnitMeasure(unitMeasu)
      .subscribe(
        (valor) =>{
          console.log(valor);
          this.dialogRef.close({event:this.data.name,data:unitMeasu})
          Swal.fire({
            position: 'center',
            icon: 'success',
            iconColor: '#85CE36',
            title: valor.message,
            showConfirmButton: false,
             timer: 2000
        })
        }, 
        (error) =>{
          this.error = true;
        }

      )
  }

  setValueForm(){
    this.selected = this.data.obj.kind
    this.fromUnit.patchValue( {
      unit_measure : this.data.obj.unit_measure,
      kind: this.data.obj.kind
    }) 
    console.log(this.fromUnit);
  }

  private buildForm() {
    this.fromUnit = this.formBuilder.group({
      unit_measure : ['', [
        Validators.required,
        Validators.maxLength(25)
      ]],
      kind : ['', [
        Validators.required,
      ]],
    });
  }
  
}
