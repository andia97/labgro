import { Component, Inject, OnInit } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder , FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

import { ArticleService } from '@core/services/article/article.service';
import { HomeComponent } from '../home/home.component';
import { Article } from '@core/models/article.model';
import { Category } from '@core/models/category.model';
import { CategoriaService } from '@core/services/categoria/categoria.service';
import { MessageService } from '@core/services/menssage/message.service';
import { UnitMeasure} from '@core/models/unit_measure';
import { UnitMeasureService } from '@core/services/unit_measure/unit-measure.service';

export interface tipo {
  id : number;
  name : string;
}

export interface unidad {
  id : number;
  name : string;
}

@Component({
  selector: 'app-add-articulo',
  templateUrl: './add-articulo.component.html',
  styleUrls: ['./add-articulo.component.css']
})
export class AddArticuloComponent implements OnInit {

   main!:HomeComponent;
   public  formArticle!: FormGroup;


  unidades!: UnitMeasure[];

  article! : Article;
  categori! : Category;
  categories!: Category[];
  error!: boolean;
  message! : String;
  statusBtn!: String;

  constructor(
    private serviceArticle: ArticleService,
    public dialogRef: MatDialogRef<AddArticuloComponent>,
    private formBuilder: FormBuilder,
    private serviceCategory: CategoriaService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageServie: MessageService,
    private unitMesureService: UnitMeasureService
  ) {

    this.main = this.dialogRef._containerInstance._config.data
    this.article = this.data.obj
    this.buildForm();
    this.getUnit();
    this.statusBtn = "";
    if(this.data.name === "Modificar"){
      this.setvalue();
    }
  }

  ngOnInit(): void {
    this.getCategories();
  }


  onClose(accion: String) {
    this.dialogRef.close({event:accion,data:this.data.obj});
  }

  onSubmit(accion: String,category:Category,unidad:unidad){
    switch (accion) {
      case 'Crear':
        this.saveArticle(category, unidad)
        this.statusBtn = "Agregar";
      break;
      case 'Modificar':
        this.editArticle(category);
        this.statusBtn = "Modificar";
      break;
      case 'Eliminar':
        this.deleteArticle(this.article.category_id+"")
    }

  }

  setvalue(){
    const category = {} as Category;
    const unitmasaure = {} as UnitMeasure;

    category.id = this.data.obj.category_id;
    category.name = this.data.obj.name;


    unitmasaure.id = this.data.obj.unit_id;
    unitmasaure.unit_measure = this.data.obj.unit_measure;

    this.categori = category;
    this.formArticle.patchValue(
      {
        name_article : this.data.obj.name_article,
        name : category,
        cod_article : this.data.obj.cod_article,
        stock : this.data.obj.stock,
       unit_price :this.data.obj.unit_price,
        unit_measure: unitmasaure,
        stock_min: this.data.obj.stock_min,
      }
    )
  }

  saveArticle(category:Category,unidad:unidad){
    if(this.formArticle.valid){
      let article =  {} as Article;
      article.cod_article = this.formArticle.get('cod_article')?.value;
      article.name_article =  this.formArticle.get('name_article')?.value;
      article.stock = this.formArticle.get('stock')?.value;
      article.unit_price = this.formArticle.get('unit_price')?.value;
      article.unit_id = unidad.id;
      article.name = category.name;
      article.unit_measure = unidad.name;
      article.category_id =  category.id;
      article.stock_min =this.formArticle.get('stock_min')?.value;
      this.serviceArticle.addArticle(article).subscribe(
        (valor : any) => {
           if(this.messageServie.get() == "fetched" ) {
             this.dialogRef.close({event:this.data.name,data:this.data.obj});
             Swal.fire({
              position: 'center',
              icon: 'success',
              iconColor: '#85CE36',
              title: valor.message,
              showConfirmButton: false,
               timer: 2000
          })
           }else{

           }
        },
        (error) =>{
          this.error = true;
          this.message = this.messageServie.get();
          console.log(error);
        }


      );
    }else{

    }

  }

  editArticle(category:Category){
    if(this.formArticle.valid){
        var article = {} as Article;
        article.category_id = this.article.category_id;
        article.cod_article = this.formArticle.get('cod_article')?.value;
        article.name_article =  this.formArticle.get('name_article')?.value;
        article.stock =  article.stock = this.formArticle.get('stock')?.value;;
        article.unit_id = this.formArticle.get('unit_measure')?.value.id
        article.name =  this.formArticle.get('name')?.value.name ;
        article.unit_price = this.formArticle.get('unit_price')?.value;
        article.unit_measure = this.formArticle.get('unit_measure')?.value.unit_measure
        article.category_id =  this.formArticle.get('name')?.value.id;
        article.stock_min =this.formArticle.get('stock_min')?.value;
        console.log(article);
        this.serviceArticle.updateArticle(this.article.id+ "",article).subscribe(
          (valor)=>{
            if(this.messageServie.get() == "fetched" ) {
              this.dialogRef.close({event:this.data.name,data:this.data.obj});
            }
          },
          (error) => {
            console.log(error)
          this.error = true;
          this.message = this.messageServie.get();
        }
        );
    }else{

    }

  }

  deleteArticle(accion: String){
    this.serviceArticle.deleteArticle(this.article.id+"")
      .subscribe(
       (valor) =>{
          if(this.messageServie.get() == "fetched" ) {
            this.dialogRef.close({event:this.data.name,data:this.data.obj});
          }
       },
       (error)=>{
         this.error = true;
          this.message = this.messageServie.get();
       }
     )
  }

  compareCategoryObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id == object2.id;
  }


  getCategories(){
    this.serviceCategory.getCategories().
    subscribe(
     (valor) =>{
      this.categories = valor;
     }
    )
  }

  getUnit(){
    this.unitMesureService.getUnitMeasures()
    .subscribe(
      valor => {
        this.unidades = valor
      }
    )
  }

  private buildForm() {
    this.formArticle = this.formBuilder.group({
      name_article : ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]],
      cod_article : ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]],
      stock : ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(7),
      ]],
      unit_price : ['', [
        Validators.required,
      ]],
      unit_measure : ['', [
        Validators.required,
        Validators.min(0)
      ]],
      name : ['', [
        Validators.required,
      ]],
      stock_min:['', [
        Validators.required,
        Validators.min(0)
      ]]

    });
  }

  public inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9 ]+$/;
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g,"");
      }
  }


}
