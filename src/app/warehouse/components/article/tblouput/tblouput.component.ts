import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { OutputArticleService } from '@core/services/output-article/out-article.service';

@Component({
  selector: 'app-tblouput',
  templateUrl: './tblouput.component.html',
  styleUrls: ['./tblouput.component.css']
})
export class TblouputComponent implements AfterViewInit {
  
  dataState: boolean = false;
  DATA: any;
  currentPage: number = 0;
  mounthone!: number;
  mountTwo!: number; 
  year!: number;
  state: boolean =  true;

  idArticle!: string;


  columnsEntry: string[] = ['Nº','Fecha', 'Comprobante', 'Destino', 'Cantidad', 'Importe', 'Precio-Medio'];
  data!: any[];
  dataSource = new MatTableDataSource<any>(this.data);
  
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<any>;



  constructor(
    private serviceOutputArticle: OutputArticleService,
    private routeActive: ActivatedRoute,
  ) { }


  ngAfterViewInit(): void {
   
  }

  ngOnInit(): void {
    this.idArticle = this.routeActive.snapshot.params.id;
    this.getArticleOutputs();
    this.getArticleOutput();
  
  }
  
  getArticleOutputs(){
    this.serviceOutputArticle.getOuputArticle(this.idArticle,this.currentPage,this.mounthone,this.mountTwo ,this.year)
    .subscribe(
      (value : any )=>{
        if(value ===  [] ){ 
          this.state = true ;
          this.dataState = false;
        }else{
          this.data = value.outputs.data; 
          this.dataSource = new MatTableDataSource<any>(value.outputs.data);
          this.state = false;
          this.dataState = true;
        }
      }
    )
  }


  getArticleOutput(){
    this.serviceOutputArticle.getOuputArticle(this.idArticle,this.currentPage,this.mounthone,this.mountTwo,this.year)
    .subscribe(
      (value : any )=>{
       // this.data = value.outputs.data; 
        console.log(value.outputs.data);
        
      }
    )
  }

  calculatingTotalPrice(precie:number,quantity:number){
    return precie*quantity;
  }




}
