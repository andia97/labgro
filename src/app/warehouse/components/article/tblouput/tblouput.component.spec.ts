import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TblouputComponent } from './tblouput.component';

describe('TblouputComponent', () => {
  let component: TblouputComponent;
  let fixture: ComponentFixture<TblouputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TblouputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TblouputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
