import { Component, OnInit,EventEmitter, } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable} from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { AddArticuloComponent } from '../add-articulo/add-articulo.component';
import { Category } from '@core/models/category.model';
import { CategoriaService } from '@core/services/categoria/categoria.service';
import { AddeditCategoriaComponent } from '../addedit-categoria/addedit-categoria.component';
import { UnitMeasure} from '@core/models/unit_measure';
import { EmptyData } from '@shared/components/empty-state/empty-state.component';
import { UnitMeasureService } from '@core/services/unit_measure/unit-measure.service';
import { AddeditUnitmasureComponent } from '../addedit-unitmasure/addedit-unitmasure.component';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit{

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  emptyData: EmptyData = {
    title: 'No tienes ningun articulo agregado',
    subtitle: 'Agrega uno dandole click en el botón de + Articulos',
    btn: 'Articulo',
    assets: 'assets/svg/empty-data.svg'
  }

  article = new FormControl('');

  emptyState: boolean = false;
  dataState: boolean = false;
  state: boolean = false

  categories! : Category[];
  unitMeasures!: UnitMeasure[];

  displayedColumns: string[] = ['buton','articulo','cantidad','notifications' ,'unidad','precio-uni', 'fecha-ing','categoria',"edit"];
  articles!: Article[];
  dataSource = new MatTableDataSource<Article>(this.articles);

  filterValues: any = {
    article: '',
  }

  searchValue: string = ''; 
  dataArticle!: Article[];
  category: string = '';
  constructor(
    private articleService :ArticleService,
    private dialog: MatDialog,
    private categoriaService :CategoriaService,
    private breakpointObserver: BreakpointObserver,
    private unitmasureService : UnitMeasureService,
  ) {
    this.getArticles();
    // this.dataSource.filterPredicate = this.createFilter();
  }


  ngOnInit(): void {
    this.dataSource.filterPredicate = (data: Article, filter: string) => {
      let searchTerms = JSON.parse(filter);

      return data.name == filter;
     };
    this.getArticles();
    this.getCategories();
    this.getUnitMasures();

    this.article.valueChanges.
    subscribe(
      value => {
        this.filterValues.article = value;
        this.dataSource.filter = JSON.stringify(this.filterValues);
        console.log(this.dataSource.filter);
      }
    )

 //   this.dataSource.filterPredicate = (data, filter)=>
  }

  openUAddEditComponentA(name: string,obj:any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name, obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddArticuloComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result) =>{
        if(result.event !== "Close"){
          this.getArticles();
        }
      }
    )

  }


  openUaddEditComponentC(name:string,obj:any){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name , obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '378px'
    const dialogRef = this.dialog.open(AddeditCategoriaComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
    (result) =>{
       if(result.event !== "Close"){
          this.getCategories();
        }
       if(result.event === "Modificar"){
         this.getArticles();
       }
      }
    )
  }

  openUaddEditComponentUni(name:string,obj:any){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name , obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '378px'
    const dialogRef = this.dialog.open(AddeditUnitmasureComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
    (result) =>{
       if(result.event !== "Close"){
          this.getUnitMasures()
        }
      }
    )
  }

  getCategories(){
    this.categoriaService.getCategories().subscribe(
      (valor) =>{
        this.categories = valor
      }
    )
  }

  getArticles(){
    this.state = true;
    this.articleService.getArticles().
    subscribe(
      (articles)=> {
        if (articles.length === 0) {
          this.emptyState = true;
          this.dataState = false;
          this.state = false;
        } else {
          this.state = false;
          this.dataState = true;
          this.emptyState = false;
          this.dataSource  = new MatTableDataSource<Article>(articles);
          this.dataArticle  = articles;
          this.dataSource.filterPredicate = function(data: any, filter: string): boolean {
            return data.name_article.toString().toLowerCase().includes(filter) 
            || data.cod_article.toLowerCase().includes(filter)
            || data.name.toLowerCase().includes(filter)
          }
        }
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  resetCategory(){
    this.getArticles();
  }

  filterCategory(categorie: Category){
    const filterValue = categorie.name;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getUnitMasures(){
    this.unitmasureService.getUnitMeasures()
    .subscribe(
      (valor:any)=>{
        this.unitMeasures = valor;
      }
    )
  }

  addNewArticle(event: any): void {

    if (event === true) {
       this.openUAddEditComponentA('Crear',{});
    }
  }

  setupFilter(column: string) {
    this.dataSource.filterPredicate = (d:Article, filter: string) => {
      const textToSearch = d[column] && d[column].toLowerCase() || '';
      return textToSearch.indexOf(filter) !== -1;
    };
  }

  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {

      let searchTerms = JSON.parse(filter);
      return data.name.toLowerCase().indexOf(searchTerms.articulo) !== -1;
    }
    return filterFunction;
  }



  statusNotification(status:boolean){

    if(status) {
      return "primary";
    }else{
      return "warn";
    }

  }

  retornarColor(status:boolean,stock:number,stock_min:number){

    //if(stock - 10 <= stock_min){
     // return '#FAE317';
   // }else {
      if(status) {
        return 'var(--error)';
      }else{
        return 'none';

     // }
    }
  }
}

