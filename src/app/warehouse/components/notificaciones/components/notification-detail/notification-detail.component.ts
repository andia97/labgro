import { ThisReceiver } from '@angular/compiler';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Area } from '@core/models/area.model';
import { Article } from '@core/models/article.model';
import { OrderService } from '@core/services/order/order.service';
import { EmptyData } from '@shared/components/empty-state/empty-state.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.css']
})
export class NotificationDetailComponent implements OnInit {


 
  statusbtn!: boolean;
  clickEvent = new EventEmitter();
  
  checked: any [] = [];

  order: any = {}
  notificationId!: number;
  date: Date = new Date();
  
  check = new FormControl('',[Validators.required])

  emptyData: EmptyData = {
    title: 'No tienes ningun articulo agregado',
    subtitle: 'Agrega uno dandole click en el botón de + Articulos',
    btn: 'Articulo',
    assets: 'assets/svg/empty-data.svg'
  }

  displayedColumns: string[] = ['check','quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
  displayedColumnsA: string[] = ['quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
 
  dataSource: any[] = [];

  orderForm!: FormGroup;

  isReceipt: boolean = false;
  isOrder: boolean = false;
  isDate: boolean = false;

  formGroup!: FormGroup;
  observationFrom!: FormGroup;

  state: boolean = true;
  dataState: boolean = false;


  stateDownload: boolean = false;


  orderId!: string;
 
  tabStatus: boolean = true; 

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private orderService: OrderService, 
  ) {
  
    this.buildForm();
    
  }

  ngOnInit(): void {

    this.route.params.subscribe(
      (params: Params) => {
        this.notificationId = params.id;
        this.getNotificationData();
        this.fillfrom();
        console.log(this.orderForm.value);
        this.checked =[];
        
         console.log(this.displayedColumns);

         this.buildForm();
        
      } 
    );

    this.fillfrom();
  }
  
  buildForm(): void {
    this.orderForm = this.formBuilder.group({
       details: this.formBuilder.array([],Validators.required),
    });

    this.observationFrom = this.formBuilder.group({
      observation : ['',
      [
        Validators.required,
       
      ]],
    })
  }
  
  fillObservation(){
    this.observationFrom.patchValue(
      {
        observation : this.order.observation
      }
    )
  }


 fillfrom(){

  this.orderForm.patchValue(
    {
      section_id : this.order.id,
      receipt :    this.order.receipt,
      order_number:   this.order.order_number,
      date_issue: this.order.order_date
    }
  )
 }



 addDetail(id:number){
   this.details.push(new FormControl(false,Validators.required))

  }


 get details(): FormArray {
  return  this.orderForm.get('details') as FormArray;
 }

 get requerid_quantity(): FormArray {
   return this.orderForm.get('required_quantity') as FormArray;
 }


 getNotificationData(): void {
     this.orderService.getOrder(this.notificationId).
    subscribe(
      (value:any) => {
        if(value === {}){
         

        }else{
          this.stateDownload = true;
          console.log(value.order);
          this.order = value.order;
          if(this.order.status === 'reprobate'){
            this.fillObservation();
          }
          this.dataSource = value.details;
          this.dataSource.forEach((val, idx, array) => {
            this.addDetail(idx);
         });  


          console.log(value.details);
          this.state = false;
          this.dataState = true;  
          if(this.order.status === 'pending'){
               this.displayedColumns = ['check','quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
          }else{
              this.displayedColumns = ['quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail']
          }
           this.stateDownload = false; 
        }
      }
    )
  }

  notArpobar(){
    console.log(this.observationFrom.get( 'observation')?.value)
    this.clickEvent.emit({value :{id: this.notificationId ,description: this.observationFrom.get('observation')?.value} ,status : 'reprobed'});
  }
  
  public inputValidator(event: any) {
    console.log(event.target.value);
    if (event.target.value  === " ") {
        this.statusbtn = false;
      }else{
        this.statusbtn = true;
      }
      console.log(this.statusbtn);
  }
  
  
  getCheckbox(){
    console.log(this.clickEvent.emit)
    this.clickEvent.emit({value : {details : this.checked , order: this.order} ,status : 'aprobed'})
     this.order.status  =  'approved';
    
  }
  
  onChange(date:any,event) {
  
    if(event) {
       this.checked.push(date);
    } else {
       this.checked = this.checked.filter((order) => order.name_article === date.name_article);
    }
    this.statusbtn = true; 
    this.check_false();
  }

   emitEvent(){
     this.clickEvent.emit()
   }



   check_false(){
    this.dataSource.forEach((val, idx, array) => {
       if (this.details.at(idx).value === false ){
            this.statusbtn = false;
        }
      });  
   }

  
}


