import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Params } from '@angular/router';
import { OrderService } from '@core/services/order/order.service';
import { OutputArticleService } from '@core/services/output-article/out-article.service';
import { Subscription } from 'rxjs';
import { NotificationDetailComponent } from './components/notification-detail/notification-detail.component';
import * as _moment from 'moment';
import Swal from 'sweetalert2';

const moment = _moment;
moment.locale('es');

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  ordens: any[] = [];
  ordernsPeding : any[] = [];
  ordernsDisapproved: any[] =  [];
  ordensApproved: any[] = [];
  order: any ;


  children!:  NotificationDetailComponent;
   
  pending: number =  0;

  emptyState: boolean = true;
  dataState: boolean = false;
  state: boolean = false

  subcription!: Subscription;
   
  tabstatus: number = 0;
  dataForm!: FormGroup;
  total : number =  0; 

  actualDate = moment()

  year!: number;
  month!: number;

  constructor(  
    private serviceorde: OrderService,
    private formBuilder: FormBuilder,
    private outputarticle: OutputArticleService,
  ) { }

  ngOnInit(): void {
   this.getOrdens(); 
    const mount =  this.actualDate.format('MMMM'); 
    const years = this.actualDate.format('YYYY'); 

    this.year = +years;
    this.month = this.convertMonth(mount);

   this.dataForm = this.formBuilder.group({
     order_id:[''],
    receipt: [''],
    section_id: [''],
    order_number: [''],
    order_date: [''],
    delivery_date: [''],
    total: [''],
    details: this.formBuilder.array([])
  });
 
  }

  get details(): FormArray {
    return  this.dataForm.get('details') as FormArray;
  }
  
  addArticleForm(data: any): void {
    const form = this.formBuilder.group({
      article_id: [data.article_id],
      quantity: [data.quantity_order],
      budget_output: [0],
      total: [data.quantity_order * +data.preci]
    });
    
    this.total =  (data.quantity_order * +data.preci) +this.total;
    this.details.push(form);
  }

  getOrdensDate(){
    this.serviceorde.getOrderss(11,2021).
    subscribe(
       (value:any)=>{
        console.log(value)

       }
    )
  } 

  getOrdens(){
    this.serviceorde.getOrders().
    subscribe(
      (value: any)=>{
        console.log(value);
        this.ordernsPeding = value.pendiente;
        this.pending = this.ordernsPeding.length;
        this.ordensApproved = value.aprobado;
        this.ordernsDisapproved = value.reprobado;
      },
      (error) =>{
        console.log(error)
      }
    )
  }

  getNotificationData(notificationId: string): void {
    this.serviceorde.getOrder(notificationId).
    subscribe(
      (value:any) => {
        console.log(value);
         this.order = value.order;
        console.log(value.order);
      }
    )
    console.log(this.order);
  }

  subcribeToEmiter(componentRef){
    console.log(componentRef);
    this.children = componentRef;
    console.log(this.tabstatus);
    this.children.clickEvent.subscribe(
      (value) =>{
        console.log(value);
        if(value.status === 'reprobed'){
           this.reprobate(value.value);
        }
        if(value.status === 'aprobed'){
          this.approved(value.value);
        }
       }
    )
      
  }
  
 
  unsubcribe(){
     if(this.subcription){
       this.subcription.unsubscribe();
     }
  }
  
  approved(details:any){
    const valu = Date.now();
    console.log(valu);
    const hoy = new Date(valu).toISOString().slice(0,10); ;
    console.log(this.order)
    console.log(details)
   

    this.dataForm.patchValue(
      {
        order_id: details.order.order_id ,
        receipt: details.order.receipt,
        section_id: details.order.section_id,
        order_number: details.order.order_number,
        order_date:  details.order.order_date,
        delivery_date: hoy,
        total: this.total,
      }
    )

    details.details.forEach(
       element => { 
         this.addArticleForm(element)
       }
    )

    console.log(details.order.order_id)

    this.outputarticle.addOutNoteOrder(this.dataForm.value,details.order.order_id)
    .subscribe(
      (value)=>{
        console.log(value);
      },
      (error) =>{
        console.log(error);
      }
    )
    this.children.getNotificationData()
    Swal.fire({
      position: 'center',
      icon: 'success',
      iconColor: '#85CE36',
      title: 'La orden a sido aprobada correctamente',
      showConfirmButton: false,
       timer: 2000
   })
  }
   
  reprobate(value: any){
    this.serviceorde.setOrderReprobed(value.id,value.description).subscribe(
      (value) =>{
        console.log(value);
      },
      (error)=>{
        console.log(error);
      }
    )
    this.getOrdens();
    this.children.getNotificationData()
    Swal.fire({
      position: 'center',
      icon: 'success',
      iconColor: '#85CE36',
      title: 'La orden a sido reprobada correctamente',
      showConfirmButton: false,
       timer: 2000
   })
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    switch (tabChangeEvent.index) {
      case 2:
        this.tabstatus = 2;
        this.children.tabStatus = false; 
      //  this.children.displayedColumns = ['quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
        console.log(this.tabstatus)
        break
      case 1:
        this.children.tabStatus = false;
        this.tabstatus = 1;
     //  this.children.displayedColumns =['quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
        console.log(this.tabstatus)
        break
      case 0:
      // this.children.displayedColumns = ['check','quantity', 'quantity_production', 'quantity_approved', 'unit', 'detail'];
        this.children.tabStatus = true;
        this.tabstatus = 0;
        console.log(this.tabstatus)
        break

    }
  }
  
  getArticle(id:number){
    let cantArticle = 0;
    this.serviceorde.getOrder(id).subscribe(
      (value : any )=>{
        cantArticle = value.details.length;
        console.log(cantArticle)
      },
      (err) =>{
        console.log(err)
      }
    ) 

    return cantArticle;
  }


  
  convertMonth(actualMonth: string): number {
    let month;
    switch (actualMonth) {
      case 'enero':
        month = 1;
        break;
      case 'febrero':
        month = 2;
        break;
      case 'marzo':
        month = 3;
        break;
      case 'abril':
        month = 4;
        break;
      case 'mayo':
        month = 5;
        break;
      case 'junio':
        month = 6;
        break;
      case 'julio':
        month = 7;
        break;
      case 'agosto':
        month = 8;
        break;
      case 'septiembre':
        month = 9;
        break;
      case 'octubre':
        month = 10;
        break;
      case 'noviembre':
        month = 11;
        break;
      case 'diciembre':
        month = 12;
        break;
        default:
        month = 0;
      break;
    }
    return month;
  }


  
  filterDate(event: number, option: string): void {
    switch(option){
      case 'month':
           console.log(event)
           this.getOrdensDate();
        break;
      case 'year':
          console.log(event);
          this.getOrdensDate();
        break;
    
    }
    
   }
  
}
