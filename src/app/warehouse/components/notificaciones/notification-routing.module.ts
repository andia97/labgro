import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotificationDetailComponent } from './components/notification-detail/notification-detail.component';
import { NotificationComponent } from './notification.component';

const routes: Routes = [
  { 
    path: '', 
    component: NotificationComponent,
    children: [
      { path: 'detalle/:id', component: NotificationDetailComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificacionesRoutingModule { }
