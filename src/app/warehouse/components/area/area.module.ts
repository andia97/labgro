import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AreaRoutingModule } from './area-routing.module';
import { AreaComponent } from './area-list/area.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticleEntryComponent } from './article-entry/article-entry.component';
import { AddeditAreaComponent } from './addedit-area/addedit-area.component';
import { FormsModule } from '@angular/forms';
import { DeliveryNoteComponent } from './delivery-note/delivery-note.component';


@NgModule({
  declarations: [
    AreaComponent,
    ArticleEntryComponent,
    AddeditAreaComponent,
    DeliveryNoteComponent,
  ],
  imports: [
    CommonModule,
    AreaRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule
  ]
})
export class AreaModule { }
