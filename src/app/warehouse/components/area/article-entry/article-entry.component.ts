import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ArticleArea } from '@core/models/article-area.model';
import { Category } from '@core/models/category.model';
import { AreaService } from '@core/services/area/area.service';
import { CategoriaService } from '@core/services/categoria/categoria.service';
import { EmptyData } from '@shared/components/empty-state/empty-state.component';


@Component({
  selector: 'app-article-entry',
  templateUrl: './article-entry.component.html',
  styleUrls: ['./article-entry.component.css']
})
export class ArticleEntryComponent implements OnInit {
   
  $area! : Observable<any>;
  displayedColumns: string[] = ['Fecha', 'Articulo', 'Cantidad', 'UnidadMedida','Categoria'];
  categories: Category[] = []; 
  id: string = "";
  article: ArticleArea[] = [];
  dataSource = new MatTableDataSource<ArticleArea>(this.article) ; 
  state: boolean  = true;
  dataState!: boolean;
  emptyState!: boolean;
  
  
  emptyData: EmptyData = {
    title: 'No tienes ningun Articulo agregado en la Area',
    subtitle: 'Agrega Entradas volviendo a la sección de Entradas',
    btn: 'Entradas',
    assets: ''
  }

  constructor(
    private routeActive: ActivatedRoute,
    private serviceAre: AreaService,
    private serviceCategory:  CategoriaService
  ) { }

  ngOnInit(): void {
    this.$area = this.getArea();
    this.getArticlesArea();
    this.getCategory();
  }

  
  getArea(){
     this.id = this.routeActive.snapshot.params.id
     return  this.serviceAre.getArea(+this.id)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getArticlesArea(){
    this.serviceAre.findArticle(+this.id)
    .subscribe(
      (value) => {
      if(value.length === 0) {
          this.state = true;
          this.dataState = false;
          this.emptyState = true;
      }else{
        this.article = value;
        this.dataSource = new MatTableDataSource<ArticleArea>(this.article);
        this.emptyState = false;
        this.state = false; 
        this.dataState = true;
      }
        
      }
    )
  }

  getCategory(){
    this.serviceCategory.getCategories().
    subscribe(
      (value) => {
        this.categories = value
      } 
    )
  }


  resetCategory(){
    this.getArticlesArea();
  }

  filterCategory(categorie: Category){
    categorie.name = categorie.name;
    this.dataSource.filter = categorie.name ;
    this.article = [];
    for (var index in this.dataSource.data) {
      if(this.dataSource.data[index]["name"] === categorie.name){
        console.log(this.dataSource.data[index]["name"]);
        this.article.push(this.dataSource.data[index]);
      } 
    }
  }

  addNewArticle(event: any): void {
    console.log(event);
    
    if (event === true) {
      console.log("redirecionar");
    }
  }
  
}

