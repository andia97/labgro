import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AreaComponent } from './area-list/area.component';
import { ArticleEntryComponent } from './article-entry/article-entry.component';
import { DeliveryNoteComponent } from './delivery-note/delivery-note.component';


const routes: Routes = [
    {
      path: '',
      component:AreaComponent
    },
    {
      path: 'nota',
      component: DeliveryNoteComponent
    },
    {
      path: ':id',
      component: ArticleEntryComponent
    }
    
    
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreaRoutingModule { }
