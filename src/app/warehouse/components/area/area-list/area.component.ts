import { Component, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatTableDataSource } from '@angular/material/table';

import { EmptyData } from '@shared/components/empty-state/empty-state.component';
import { AreaService } from '@core/services/area/area.service';
import { Area } from '@core/models/area.model';
import { MatSlideToggle, MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddeditAreaComponent } from '../addedit-area/addedit-area.component';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  @Input()
  status = "";
  areas: Area[] =[]
  displayedColumns: string[] = ['id', 'Areas', 'Activo', 'Edit'];
  emptyState: boolean = false;
  state: boolean = true;
  dataState: boolean = false;

  area = new FormControl(''); 

  emptyData: EmptyData = {
    title: 'No tienes ninguna Area agregado',
    subtitle: 'Agrega uno dandole click en el botón de + Area',
    btn: 'Area',
    assets: ''
  }

  dataSource = new MatTableDataSource<Area>(this.areas) ;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
   
  filterValues: any = {
    name: '',
  }

  constructor(
    private serviceArea : AreaService,
    private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
  ) {
    this.dataSource.filterPredicate = this.createFilter();
  }

  ngOnInit(): void {
   this.dataSource.filterPredicate 
   this.getAreas();
   this.area.valueChanges
   .subscribe(
     value => {
       this.filterValues.name = value;
      // this.dataSource.filter = JSON.stringify()
     }
   ) 
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAreas(){
    this.serviceArea.getAreas()
    .subscribe(
      (value) => {
        if(value.length === 0 ){
          this.state = true;
          this.emptyState = true;
          this.dataState = false;
        }else{
          this.state = false;
          this.dataState = true;
          this.emptyState = false;
          this.areas = value;
          this.dataSource = new MatTableDataSource<Area>(this.areas);
        }

      }
    )
  }

  editActiveArea(id: string,area : Area){
    this.serviceArea.updateArea(id,area)
    .subscribe(
      valor => console.log(valor)
    )
    {

    }
  }

  onChange(ob: MatSlideToggleChange, id : string , area : Area) {
    console.log(ob.checked);
    console.log(area);
    this.serviceArea.deleteArea(id)
    .subscribe(valor=> console.log(valor));
    //this.editActiveArea(id,area);
  }


  openUAddEditComponentArea(name: string,obj:any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name, obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddeditAreaComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result: any ) =>{
        console.log(result);
        if(result.event !== "Close"){
          this.getAreas();
        } else{
        }
      }
    )

  }

  addNewArticle(event: any): void {
    if (event === true) {
       this.openUAddEditComponentArea('Crear',{});
    }
  }
  
  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {

      let searchTerms = JSON.parse(filter);
      return data.name.toLowerCase().indexOf(searchTerms.articulo) !== -1;
    }
    return filterFunction;
  }
}
