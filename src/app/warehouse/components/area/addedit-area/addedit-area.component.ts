import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';

import { Area } from '@core/models/area.model';
import { AreaService } from '@core/services/area/area.service';
import { MessageService } from '@core/services/menssage/message.service';


@Component({
  selector: 'app-addedit-area',
  templateUrl: './addedit-area.component.html',
  styleUrls: ['./addedit-area.component.css']
})
export class AddeditAreaComponent implements OnInit {
  
  public  fromArea!: FormGroup;
  errorMessage= "";
  stateError=  false;
 
  constructor(
    public dialogRef: MatDialogRef<AddeditAreaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private serviceArea: AreaService,
    private serviceMessage: MessageService
  ) { }

  ngOnInit(): void {
    this.buildForm()
    if(this.data.name === 'Modificar'){
        this.setValue();
    }
  }
  
  onClose(status: string){
    this.dialogRef.close({event:this.data.name,data:this.data.obj})
  }

  onSubmit(accion : string){
   
   console.log(accion);
   switch (accion) { 
     case 'Crear':
       this.saveCategory();
     break;

     case 'Modificar':
       this.editCategory();
     break;

   }
 }

 setValue(){
   this.fromArea.setValue( {
     name : this.data.obj.name
   }) 
 }


 saveCategory(){
   var area  =  {} as  Area;
   if(this.fromArea.valid){
     area.name = this.fromArea.get('name')?.value;
     area.Active = true; 
     this.serviceArea.addArea(area).subscribe(
     (valor : any) => {
         if ( this.serviceMessage.get() === "fetched"){
           this.dialogRef.close({event:this.data.name,obj :area})
           Swal.fire({
               position: 'center',
               icon: 'success',
               iconColor: '#85CE36',
               title: 'Area creada Correctamente',
               showConfirmButton: false,
                timer: 2000
           })
         }
     },
     (error : any) => {
       this.errorMessage = this.serviceMessage.get();
       this.stateError = true;
       console.log(this.errorMessage);
       console.log(error)
     }
     );
    }
    
    
 }
 

 editCategory(){
   if(this.fromArea.valid){
     this.data.obj.name = this.fromArea.get('name')?.value;;
     this.serviceArea.updateArea(this.data.obj.id+"",this.data.obj)
     .subscribe(
        (valor:any) =>{
         console.log(valor)
           if ( this.serviceMessage.get() === "fetched update"){
           this.dialogRef.close({event:this.data.name,data:this.data.obj})
           }
         },
         (error:any)=>{
           this.errorMessage = this.serviceMessage.get();
           this.stateError = true;
         }
     )
   }
 }


 public inputValidator(event: any) {
   const pattern = /^[a-zA-Z ]+$/;   
   if (!pattern.test(event.target.value)) {
       event.target.value = event.target.value.replace(/[^a-zA-Z ]/g,"");
     }
 }

 private buildForm() {
   this.fromArea= this.formBuilder.group({
     name : ['', [
       Validators.required,
       Validators.minLength(3),
       Validators.maxLength(30)
     ]],
     
   });
 }

}
 
