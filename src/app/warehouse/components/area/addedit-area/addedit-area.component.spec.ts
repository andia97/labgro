import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditAreaComponent } from './addedit-area.component';

describe('AddeditAreaComponent', () => {
  let component: AddeditAreaComponent;
  let fixture: ComponentFixture<AddeditAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddeditAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
