import { AfterContentInit, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { SideNavService } from '@shared/services/side-nav.service';
import { StorageService } from '@core/services/storage/storage.service';
import { AuthConstants } from '@core/config/auth-constanst';

@Component({
  selector: 'app-layout-warehouse',
  templateUrl: './layout-warehouse.component.html',
  styleUrls: ['./layout-warehouse.component.css']
})
export class LayoutWarehouseComponent implements OnInit, AfterContentInit {

  title!: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  @ViewChild('sidenav') public sidenav!: MatSidenav;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private sideNavService: SideNavService,
    private storageService: StorageService,
  ) {
    this.storageService.get(AuthConstants.TITLE_NAV)
    .then(
      (title: string) => {
        this.title = title;
      }
    ).catch(
      (error: any) => {
        this.title = 'Articulos';
      }
    )
  }

  ngOnInit(): void {

  }
  ngAfterContentInit(): void {
    setTimeout(
      ()=> {
        this.sideNavService.sideNavToggleSubject.subscribe(()=> {
          this.sidenav.open();
        });
      }, 0);
  }

  changeTitle(event: string) {
    this.title = event;
    this.storageService.store(AuthConstants.TITLE_NAV, event);
  }
}
