import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

import * as _moment from 'moment';
const moment = _moment;
moment.locale('es');
import Swal from 'sweetalert2';
@Component({
  selector: 'app-peripheral-inventory',
  templateUrl: './peripheral-inventory.component.html',
  styleUrls: ['./peripheral-inventory.component.css']
})
export class PeripheralInventoryComponent implements OnInit {

  @ViewChild('toggleButton') toggleButton!: ElementRef;

  @ViewChild('trimestres') option! : ElementRef; 
  trimestre!: string;
  fechas!: string; 
  year!: number;
  stateDownload! : boolean;
  
  fecha:string = "31 DE MARZO";
  actualDate = moment();
  DATA! : any;
  isMenuOpen = false;
  dataSource:any;
  displayedColumns: string[]=['Kardex', 'Cantidad', 'Unidad', 'Detalle', 'P/U Bs.', 'P/Total Bs.', 'Salidas', 'Importe Bs.', 'Saldo', 'Importe Bs'];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  constructor(
    private breakpointObserver: BreakpointObserver,
    private renderer: Renderer2,
    private articleIncomeService: IncomesArticleService
  ) { 
    this.trimestre = "Tercer Trimestre";
    const actualMonth = this.actualDate.format('MMMM');
    const actualYear = this.actualDate.format('YYYY');
    this.year = + actualYear;
    this.trimestre = this.convertMonth(actualMonth);
  }
  
  convertMonth(actualMonth):string{
    const primerTrimestre=['enero', 'febrero', 'marzo'];
    const segundoTrimestre=['abril', 'mayo', 'junio'];
    const tercerTrimestre=['julio', 'agosto', 'septiembre'];
    const cuartoTrimestre=['octubre', 'noviembre', 'diciembre'];
    if(primerTrimestre.includes(actualMonth)) return "Primer Trimestre";
    if(segundoTrimestre.includes(actualMonth)) return "Segundo Trimestre";
    if(tercerTrimestre.includes(actualMonth)) return "Tercer Trimestre";
    if(cuartoTrimestre.includes(actualMonth)) return "Cuarto Trimestre";
    return "Primer Trimestre";
  }

  ngOnInit(): void {
    this.getIncomeArticles(this.trimestre,this.year);
  }

  getIncomeArticles(trimestre: string, year:number): void{
    this.articleIncomeService.peripheralReport(trimestre,year)
    .subscribe(
      (res: any)=>{
        this.mappingResult(res);
      },
      (err: any)=>{
        console.error(err)
        if(err === "No se encontraron resultados"){
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'No se encontraron registros con los datos brindados',
            showConfirmButton: true,
            confirmButtonColor: '#85CE36',
            timer: 2000
          });
        }
        this.dataSource= [];
      }
    )
  }
  
  mappingResult(initialArr: any): void{
    console.log(initialArr)
    const mappedArr: any=[];
    for(let run=0; initialArr.length>run;run){
        const arrFiltered=initialArr.filter((item)=>item.articleId===initialArr[run].articleId)
        if(arrFiltered.length>1){
          const obtList={...initialArr[run]}
          obtList["outputQuantity"]=arrFiltered.reduce(({outputQuantity}, currentValue)=>outputQuantity+currentValue.outputQuantity)
          mappedArr.push(obtList);
        }else{
          mappedArr.push(arrFiltered[0]);
        }
      initialArr = initialArr.filter(item => !arrFiltered.includes(item))
    }
    console.log(mappedArr)
    this.dataSource= mappedArr;
  }

  filterDate(event: number): void {
    this.year = + event; 
    this.getIncomeArticles(this.trimestre,this.year);
  }

  ngAfterViewInit(): void{
    
  }

  viewTrimestre(){
    this.isMenuOpen=true;
  }

  changeTrimenstre(trimestre:string){
    this.trimestre = trimestre;
     switch(this.trimestre){
      case  "Primer Trimeste":
        this.fecha = "31 DE MARZO";
        break; 
      case "Segundo Trimestre":
        this.fecha = "30 DE JUNIO";
        break; 
      case "Trecer Trimestre":
        this.fecha = "30 DE SEPTIEMBRE";
        break; 
      case "Cuarto Trimestre":
        this.fecha = "31 DE DICIEMBRE";
        break; 
    }
  
    this.isMenuOpen=false;
    this.getIncomeArticles(trimestre,this.year);
  }

  
  public downloadPDF(): void {
    this.stateDownload = true;
  
    const data: any = document.getElementById('peripal');

   const doc = new jsPDF('L', 'mm', 'A4');
   this.DATA = document.getElementById('peripal');
  // const DATA = this. componentblEntry.DATA; 
    console.log(this.DATA);
  
    const options = {
      background: 'white',
      scale: 3
    }

    html2canvas(this.DATA, options).then(
      (canvas) => { 
      const img = canvas.toDataURL('image/PNG');
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 1 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      this.stateDownload = false; 
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }
}
