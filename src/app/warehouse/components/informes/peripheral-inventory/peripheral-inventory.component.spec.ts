import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeripheralInventoryComponent } from './peripheral-inventory.component';

describe('PeripheralInventoryComponent', () => {
  let component: PeripheralInventoryComponent;
  let fixture: ComponentFixture<PeripheralInventoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeripheralInventoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PeripheralInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
