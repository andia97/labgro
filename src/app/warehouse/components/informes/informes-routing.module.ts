import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InformesComponent } from './informes.component';
import { PeripheralInventoryComponent } from './peripheral-inventory/peripheral-inventory.component';

const routes: Routes = [
  { path: '', 
    component: PeripheralInventoryComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformesRoutingModule { }
