import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformesRoutingModule } from './informes-routing.module';
import { InformesComponent } from './informes.component';
import { SharedModule } from '@shared/shared.module';
import { PeripheralInventoryComponent } from './peripheral-inventory/peripheral-inventory.component';
import { MaterialModule } from '@material/material.module';


@NgModule({
  declarations: [
    InformesComponent,
    PeripheralInventoryComponent
  ],
  imports: [
    CommonModule,
    InformesRoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class InformesModule { }
