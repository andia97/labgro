import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutNotesComponent } from './layout-notes.component';

describe('LayoutNotesComponent', () => {
  let component: LayoutNotesComponent;
  let fixture: ComponentFixture<LayoutNotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutNotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
