import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputNote } from '@core/models/input_note.model';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { Observable } from 'rxjs';
import { map, shareReplay, startWith } from 'rxjs/operators';

import * as _moment from 'moment';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
const moment = _moment;
moment.locale('es');

import Swal from 'sweetalert2'
import { EmptyData } from '@shared/components/empty-state/empty-state.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout-notes',
  templateUrl: './layout-notes.component.html',
  styleUrls: ['./layout-notes.component.css'],
})

export class LayoutNotesComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  searchControl: FormGroup = this.formBuilder.group(
    {
      search: ['',
      [Validators.min(1), Validators.max(999999999)]]
    }
  );

  get search(): any {
    return this.searchControl.get('search');
  }
  // Messages for validators
  public errorsMessages = {
    search: [
      { type: 'max', message: 'Debe introducir como máximo 9 digitos.'},
      { type: 'min', message: 'Debe introducir números mayores a 0'},
    ],
  }

  month: number;
  year: number;
  value: string;


  actualDate = moment();
  dataSource: any;
  noteList!: InputNote[];

  @ViewChild('paginator') paginator!: MatPaginator;
  pageEvent!: PageEvent;
  currentPage: number = 0;
  lastPage: any;

  emptyState: boolean = false;
  dataState: boolean = false;
  state: boolean = false;
  emptyData: EmptyData = {
    title: 'No tienes ninguna nota de recepción agregado con los filtros dados',
    subtitle: '¿Tienes una nueva entrada de articulos?',
    btn: 'Nota de recepción',
    assets: 'assets/svg/empty-note.svg'
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private incomeArticleService: IncomesArticleService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    const actualMonth = this.actualDate.format('MMMM');
    const actualYear = this.actualDate.format('YYYY');
    this.year = + actualYear;
    this.month = this.convertMonth(actualMonth);
    this.value = '';
  }

  ngOnInit(): void {
    this.getIncomeNotes();
    this.searchControl.controls.search.valueChanges.subscribe(
      (value: string) => {
        // console.log(value);

        if (value === null) {
          this.value = '';
          this.getIncomeNotes();
        }
      }
    )
  }


  getIncomeNotes(): void {
    this.state = true;
    this.incomeArticleService.getIncomesNotes(this.value, this.month, this.year,  this.currentPage + 1).subscribe(
      (res: any) => {
        console.log(res);
        this.state = false;
        this.dataSource = res;
        this.noteList = res.data;
        if (this.noteList.length === 0) {
          this.emptyState = true;
          this.dataState = false;
        } else {
          this.emptyState = false;
          this.dataState = true;
        }
        this.state = false;
        // console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Ocurrio un error inesperado, contactese con administración',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36',
        });
      }
    );
  }

  filterDate(event: number, option: string): void {
    if (option == 'month') {
      this.month = event;
    } else {
      this.year = + event;
    }
    this.currentPage = 0;
    this.paginator.pageIndex = 0;
    // console.log(this.month, this.year);
    this.getIncomeNotes();
  }

  convertMonth(actualMonth: string): number {
    let month;
    switch (actualMonth) {
      case 'enero':
        month = 1;
        break;
      case 'febrero':
        month = 2;
        break;
      case 'marzo':
        month = 3;
        break;
      case 'abril':
        month = 4;
        break;
      case 'mayo':
        month = 5;
        break;
      case 'junio':
        month = 6;
        break;
      case 'julio':
        month = 7;
        break;
      case 'agosto':
        month = 8;
        break;
      case 'septiembre':
        month = 9;
        break;
      case 'octubre':
        month = 10;
        break;
      case 'noviembre':
        month = 11;
        break;
      case 'diciembre':
        month = 12;
        break;
        default:
        month = 0;
      break;
    }
    return month;
  }

  onPaginatorChanges(event: PageEvent): void {
    // console.log(event);
    let page = event.pageIndex;
    this.currentPage = event.pageIndex;
    page = page + 1;
    // this.state = true;
    this.getIncomeNotes();

  }

  addNewArticle(event: true): void {
    this.router.navigate(['/warehouse/entradas/crear-nota']);
  }

  searchNote(): void {
    this.value = this.searchControl.controls.search.value;
    this.getIncomeNotes();
  }


}
