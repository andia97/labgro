import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import  Sweet from 'sweetalert2';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
}) 
export class EditNoteComponent implements OnInit {

  dataGeneralForm!: FormGroup;
  state: boolean = false;
  
  get receipt(): any {
    return this.dataGeneralForm.get('receipt');
  }

  get provider(): any {
    return this.dataGeneralForm.get('provider');
  }
  

  // Messages for validators
  public errorsMessages = {
    receipt: [
      { type: 'required', message: 'El nro de comprobante es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 digitos.'},
      { type: 'min', message: 'Debe introducir números mayores a 0'},
    ],
    provider: [
      { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'pattern', message: 'Solo se permite letras de la (a-z A-Z) y espacios ( )'},
    ],
  }

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditNoteComponent>,
    private incomeArticleService: IncomesArticleService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    const dataForm = this.data.data;
    console.log(dataForm);
    console.log("sadasd");
    
    this.dataGeneralForm.patchValue(dataForm);
    this.dataGeneralForm.controls.input_date.setValue(dataForm.created_at);
  }

  private buildForm(): void {
    this.dataGeneralForm = this.formBuilder.group({
      id: [''],
      receipt: ['', [Validators.required, Validators.min(1), Validators.max(9999999999), Validators.maxLength(9)]],
      provider: ['', [Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'), Validators.maxLength(50)]],
      created_at: [ , [Validators.required]],
      input_date: [
        {
          value: '',
          disabled: true
        },
      ]
    });
  }

  addEvent(event: MatDatepickerInputEvent<Date>) {
    const newDateInput = event.value;
    this.dataGeneralForm.controls.created_at.setValue(newDateInput);
  }

  saveGeneralData(): void {
    this.state = true;
    // console.log(this.dataGeneralForm.value);
    const data = this.dataGeneralForm.value;
    this.incomeArticleService.updateIncomeNote(data).subscribe(
      (res: any) => {
        this.state = false;
        console.log(res);
        Sweet.fire({
          icon: 'success',
          title: 'Modificado correctamente',
          text: 'Los datos se modificaron correctamente',
          showConfirmButton: false,
          timer: 3000
        });
        this.dialogRef.close(true);
      },
      (error: any) => {
        this.state = false;
        // console.log(error);
        Sweet.fire({
          icon: 'error',
          title: '¡Oops, ocurrio un error!',
          text: 'Los datos no se modificaron correctamente, contactese con administración',
          showConfirmButton: false,
          timer: 3000
        });
      },
    );
  }

}
