import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  templateUrl: './add-income-article.component.html',
  styleUrls: ['./add-income-article.component.css']
})
export class AddIncomeArticleComponent implements OnInit {

  // array of multi article from data base 
  options: Article[] = [];

  // variable for the change detection of autocomplete input
  filteredOptions!: Observable<Article[]>;
  
  // variable for the data form
  dataArticleForm!: FormGroup;

  // variable use only given the unit price last
  article!: Article;

  state: boolean = false;

  
  //Validators when user touchet type data on input
  get articleData():any {
    return this.dataArticleForm.get('article');
  }

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }

  get unitPrice():any {
    return this.dataArticleForm.get('unit_price');
  }


  // Messages for validators
  public errorsMessages = {
    articleData: [
      { type: 'required', message: 'Selecciona un articulo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
    unitPrice: [
      { type: 'required', message: 'El precio unitario es requerido' },
      { type: 'min', message: 'Debe introducir como mínimo 8 caracteres.'},
    ],
  }
  constructor(
    private articleService: ArticleService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddIncomeArticleComponent>,
    ) {
    this.buildForm();
  }
  
  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ , 
        [Validators.required]
      ],
      article_id: [ , 
        [
          Validators.required
        ]
      ],
      quantity: ['', 
        [
          Validators.required,
          Validators.min(0)
        ]
      ],
      unit_price: ['', 
        [
          Validators.required,
          Validators.min(0)
        ]
      ],
      total_price: ['']
    });
  }
  
  ngOnInit(): void {
    this.getArticles();

    //change detection by the unit price input
    this.dataArticleForm.controls.unit_price.valueChanges.subscribe(
      (unitPrice: number) => {
        const quantity = this.dataArticleForm.controls.quantity.value;
        let totalPrice = quantity * unitPrice;
        this.dataArticleForm.controls.total_price.setValue(totalPrice);
      }
    );

    this.dataArticleForm.controls.quantity.valueChanges.subscribe(
      (quantity: number) => {
        const unitPrice = this.dataArticleForm.controls.unit_price.value;
        let totalPrice = quantity * unitPrice;
        this.dataArticleForm.controls.total_price.setValue(totalPrice);
      }
    );
  }
  
  getArticles(): void {
    this.state = true;
    this.articleService.getArticles().subscribe(
      (res: Article[]) => {
        this.state = false;
        // console.log(res);
        this.options = res;
        this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name_article),
          map(name_article => name_article ? this._filter(name_article) : this.options.slice())
        );
      },
      (error: any) => {
        console.log(error);
        alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
      },
    )
  }

  saveArticle(): void {
    // console.log(this.dataArticleForm.value);
    this.dialogRef.close(this.dataArticleForm)
  }

  // Methods for the autocomplete
  displayFn(article: Article): string {
    return article && article.name_article ? article.name_article : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        // console.log(value);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }

  getArticle(option: any): void {
    this.article = option;
    this.dataArticleForm.controls.article_id.setValue(this.article.id);
    this.dataArticleForm.controls.unit_price.setValue(this.article.unit_price);
  }

}
