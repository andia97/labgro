import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIncomeArticleComponent } from './edit-income-article.component';

describe('EditIncomeArticleComponent', () => {
  let component: EditIncomeArticleComponent;
  let fixture: ComponentFixture<EditIncomeArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditIncomeArticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIncomeArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
