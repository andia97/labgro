import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIncomeArticleComponent } from './add-income-article.component';

describe('AddIncomeArticleComponent', () => {
  let component: AddIncomeArticleComponent;
  let fixture: ComponentFixture<AddIncomeArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddIncomeArticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIncomeArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
