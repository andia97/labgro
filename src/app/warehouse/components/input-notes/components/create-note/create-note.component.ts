import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { AuthConstants } from '@core/config/auth-constanst';
import { ArticleIncome } from '@core/models/article-incomes.model';

import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { StorageService } from '@core/services/storage/storage.service';

import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { MessageService } from '@core/services/menssage/message.service';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.css']
})
export class CreateNoteComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  dataForm!: FormGroup;
  state: boolean = false;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private formBuilder: FormBuilder,
    private incomesArticleService: IncomesArticleService,
    private storageService: StorageService,
    private router: Router,
    private messageServie: MessageService,
    ) {
      this.buildForm();
    }

  ngOnInit(): void {
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      receipt: [''],
      provider: [''],
      invoice_number: [''],
      order_number: [''],
      total: [''],
      created_at: [''],
      articles: this.formBuilder.array([])
    });
  }

  getFirstDataForm(event: FormGroup): void {
    // console.log(event);
    const firstData = event.value;
    this.dataForm.patchValue(firstData);
    // console.log(this.articles.value);
  }

  async getSecondDataForm(event: FormGroup) {
    const articles: ArticleIncome[] = event.value.articles;
    articles.forEach(element => {
        this.addArticleForm(element);
    });
    const secondData = event.value;
    this.dataForm.patchValue(secondData);

    this.saveAllDataNote();
  }

  saveAllDataNote(): void {
    this.state = true;
    const data = this.dataForm.value;
    data.created_at = data.created_at.toLocaleString("en-US", {timeZone: "America/La_Paz"});
    this.incomesArticleService.addIncomeNote(data).subscribe(
      (res: any) => {
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'success',
          // iconColor: '#85CE36',
          title: 'Guardado Correctamente',
          text: 'La nota de recepción se guardó correctamente',
          showConfirmButton: false,
          // confirmButtonColor: '#85CE36'
          timer: 2000
        })
        this.router.navigate(['/warehouse/entradas']);
        this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
        this.storageService.remove(AuthConstants.DATA_IN_NOTE);
      },
      (error: any) => {
        const message = this.messageServie.get();
        // console.log(message);
        const messageList = message.errors.receipt;
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'error',
          // iconColor: '#85CE36',
          title: 'Los datos no se guardaron correctamente',
          text: messageList.join('<br>'),
          showConfirmButton: true,
          confirmButtonColor: '#85CE36',
          // timer: 2000
        })
        // console.log(error);
      },
    );
  }

  goBack(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de volver atras?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'No, continuar',
      cancelButtonText: 'Si, volver',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire({
          title: 'Continua configurando',
          text: "No te preocupes, tu datos estan a salvo",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 1800
        });
      } else {

        if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          this.router.navigate(['/warehouse/entradas/']);
          this.storageService.remove(AuthConstants.DATA_IN_NOTE);
          this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
        }
      }
    })
  }


  // add article form from local storage
  addArticleForm(data: any): void {
    const form = this.formBuilder.group({
      article_id: [data.article_id],
      quantity: [data.quantity],
      unit_price: [data.unit_price],
      total_price: [data.total_price]
    });
    this.articles.push(form);
  }
  get articles(): FormArray {
    return this.dataForm.get('articles') as FormArray;
  }
}
