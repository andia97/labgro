import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AuthConstants } from '@core/config/auth-constanst';
import { StorageService } from '@core/services/storage/storage.service';

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.css']
})
export class FirstStepComponent implements OnInit{

  dataGeneralForm!: FormGroup;
  @Output() emitterDataForm: EventEmitter<FormGroup> = new EventEmitter();
  
  get receipt(): any {
    return this.dataGeneralForm.get('receipt');
  }

  get provider(): any {
    return this.dataGeneralForm.get('provider');
  }

  get invoice_number(): any {
    return this.dataGeneralForm.get('invoice_number');
  }
  
  get order_number(): any {
    return this.dataGeneralForm.get('order_number');
  }
  

  // Messages for validators
  public errorsMessages = {
    receipt: [
      { type: 'required', message: 'El nro de comprobante es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 digitos.'},
      { type: 'min', message: 'Debe introducir números mayores a 0'},
      { type: 'pattern', message: 'Solo se permite números'},
    ],
    provider: [
      { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'pattern', message: 'Solo se permite letras de la (a-z A-Z) y espacios ( )'},
    ],
    invoice_number: [
      { type: 'required', message: 'El nro de factura es requerido' },
      { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'pattern', message: 'Solo se permite números'},
    ],
    order_number: [
      { type: 'required', message: 'El nro de orden es requerido' },
      { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'pattern', message: 'Solo se permite números'},
    ],
  }

  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    if (localStorage.getItem('data_in_note')) {
      // console.log('existe');
      this.storageService.get(AuthConstants.DATA_IN_NOTE).then(
        (data: any) => {
          // console.log(data);
          this.dataGeneralForm.patchValue(data);
          this.dataGeneralForm.controls.input_date.setValue(data.created_at);
        }
      );
    }
  }

  private buildForm(): void {
    this.dataGeneralForm = this.formBuilder.group({
      receipt: ['', 
        [
          Validators.required, 
          Validators.min(1), 
          Validators.max(9999999999), 
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'), 
        ]],
      order_number: ['', 
        [
          Validators.required, 
          Validators.min(1), 
          Validators.max(9999999999), 
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'), 
        ]],
      invoice_number: ['', 
        [
          Validators.required, 
          Validators.min(1), 
          Validators.max(9999999999), 
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'), 
        ]],
      provider: ['', 
        [
          Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'), 
          Validators.maxLength(50)
        ]],
      created_at: [ new Date, [Validators.required]],
      input_date: [
        {
          value: new Date,
          disabled: true
        },
      ]
    });
  }

  addEvent(event: MatDatepickerInputEvent<Date>) {
    const newDateInput = event.value;
    this.dataGeneralForm.controls.created_at.setValue(newDateInput);
  }

  saveGeneralData(): void {
    // console.log(this.dataGeneralForm.value);
    this.emitterDataForm.emit(this.dataGeneralForm);
    const data = this.dataGeneralForm.value;
    this.storageService.store(AuthConstants.DATA_IN_NOTE, data);
  }

}
