import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputOutputRoutingModule } from './input-output-routing.module';
import { MaterialModule } from '@material/material.module';
import { LayoutNotesComponent } from './components/layout-notes/layout-notes.component';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { EditNoteComponent } from './components/edit-note/edit-note.component';
import { DataListNoteContainer } from './container/data-list-note/data-list-note.container';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FirstStepComponent } from './components/create-note/first-step/first-step.component';
import { SecondStepComponent } from './components/create-note/second-step/second-step.component';
import { AddIncomeArticleComponent } from './components/create-note/second-step/add-income-article/add-income-article.component';
import { EditIncomeArticleComponent } from './components/create-note/second-step/edit-income-article/edit-income-article.component';
import { SharedModule } from '@shared/shared.module';
import { IncomeDetailsContainer } from './container/income-details/income-details.container';


@NgModule({
  declarations: [
    LayoutNotesComponent,
    CreateNoteComponent,
    EditNoteComponent,
    DataListNoteContainer,
    FirstStepComponent,
    SecondStepComponent,
    AddIncomeArticleComponent,
    EditIncomeArticleComponent,
    IncomeDetailsContainer
  ],
  imports: [
    CommonModule,
    InputOutputRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class InputOutputModule { }
