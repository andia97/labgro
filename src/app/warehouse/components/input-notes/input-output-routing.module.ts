import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { LayoutNotesComponent } from './components/layout-notes/layout-notes.component';
import { IncomeDetailsContainer } from './container/income-details/income-details.container';

const routes: Routes = [
  { path: '', component: LayoutNotesComponent },
  { path: 'crear-nota', component: CreateNoteComponent },
  { path: 'detalle-nota/:id', component: IncomeDetailsContainer },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InputOutputRoutingModule { }
