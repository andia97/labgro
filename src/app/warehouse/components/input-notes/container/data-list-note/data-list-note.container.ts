import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InputNote } from '@core/models/input_note.model';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { EditNoteComponent } from '../../components/edit-note/edit-note.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-data-list-note',
  templateUrl: './data-list-note.container.html',
  styleUrls: ['./data-list-note.container.css']
})
export class DataListNoteContainer {

  @Input() incomeNote!: InputNote;
  @Output() emitNoteEventUpdate: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private dialog: MatDialog,
    private incomeArticleService: IncomesArticleService,
  ) { }

  editNote(): void {
    const dialogRef = this.dialog.open(EditNoteComponent, {
      width: '500px',
      height: '400px',
      panelClass: 'custom-dialog-container',
      data: {
        data: this.incomeNote
      }
    });
    dialogRef.afterClosed().subscribe(
      (res: boolean) => {
        if (res) {
         this.emitNoteEventUpdate.emit(res);
        }
      }
    )
  }

  deleteNote(): void {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la nota nro: ' + this.incomeNote.receipt + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {

        this.incomeArticleService.deleteIncomeNote(this.incomeNote.id).subscribe(
          (res: any) => {
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu nota de recepción se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
            this.emitNoteEventUpdate.emit(res);
        },
        (error: any) => {
          console.log(error);
          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });

        }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu nota de recepción esta a salvo",
          icon: 'error',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
      }
    })
  }
}
