import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { InputNote } from '@core/models/input_note.model';
import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-income-details',
  templateUrl: './income-details.container.html',
  styleUrls: ['./income-details.container.css']
})
export class IncomeDetailsContainer implements OnInit {
  
  displayedColumns: string[] = ['quantity', 'unit', 'desc', 'cod', 'price','total_price'];
  dataSource: any;
  noteId!: number;
  inputNote!: InputNote;
  state: boolean = false;
  constructor(
    private route: ActivatedRoute, 
    private serviceIncomes : IncomesArticleService  
    ) {
    this.route.params.subscribe(
      (params: Params) => {
        this.noteId = params.id;
      }
    );
  }

  ngOnInit(): void {
    this.getArticlesIncome();
  }

  getArticlesIncome(): void {
    this.state = true;
    this.serviceIncomes.getDataIncomeNote(this.noteId).
    subscribe(
      (res: any) => {
        this.state = false;
        this.inputNote = res.income[0];
        this.dataSource = res.details;
        // console.log(res);
        // console.log(this.inputNote)
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    )
  }

  public downloadPDF(): void {
   
    const doc = new jsPDF('L', 'mm', 'A4');
    const DATA : any = document.getElementById('htmlNote'); 
    console.log(DATA);
  
    const options = {
      background: 'white',
      scale: 3
    }

    html2canvas(DATA, options).then(
      (canvas) => {
        
      const img = canvas.toDataURL('image/PNG');

     
      const bufferX = 15;
      const bufferY = 1;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth + 4) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }

}
