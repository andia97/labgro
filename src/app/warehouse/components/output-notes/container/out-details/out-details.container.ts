import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { OutputArticleService } from '@core/services/output-article/out-article.service';
import { OutputNote } from '@core/models/output_note.model';



@Component({
  selector: 'app-income-details',
  templateUrl: './out-details.container.html',
  styleUrls: ['./out-details.container.css']
})
export class OutDetailsContainer implements OnInit {
  noteId!: number;
  displayedColumns: string[] = ['quatity', 'unit', 'description', 'code_article', 'budget', 'unit_price','total_price'];
  dataSource: any;
  outNote!: OutputNote;
  stateDownload!: boolean;
  constructor(
    private route: ActivatedRoute,
    private outArticleService: OutputArticleService
  ) {
    this.route.params.subscribe(
      (params: Params) => {
        this.noteId = params.id;
      }
    );
  }

  ngOnInit(): void {
    this.getDataOut();
  }
  
  getDataOut(): void {
    this.outArticleService.getDataOutNote(this.noteId)
    .subscribe(
      (res: any) => {
        // console.log(res);
        this.outNote = res.output[0];
        this.dataSource = res.details;
      }, 
      (error: any) => {
        console.log(error);
      }
    );
  }
  public downloadPDF(): void {
    this.stateDownload = true;
    const doc = new jsPDF('L', 'mm', 'A4');
    
    const DATA : any = document.getElementById('htmlNote'); 
     console.log(DATA);
   
     const options = {
       background: 'white',
       scale: 3
     }
 
     html2canvas(DATA, options).then(
       (canvas) => {
         
       const img = canvas.toDataURL('image/PNG');
       const bufferX = 15;
       const bufferY = 15;
       const imgProps = (doc as any).getImageProperties(img);
       const pdfWidth = doc.internal.pageSize.getWidth() - 1* bufferX;
       const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
       doc.addImage(img, 'PNG', bufferX, 10 , pdfWidth, 100, undefined, 'FAST');
       return doc;
     }).then((docResult) => {
      this.stateDownload = false;
       docResult.save(`${new Date().toISOString()}notasalida.pdf`);
     });
 
   }

}
