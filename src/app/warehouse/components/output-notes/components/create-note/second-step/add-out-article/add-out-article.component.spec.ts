import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOutArticleComponent } from './add-out-article.component';

describe('AddIncomeArticleComponent', () => {
  let component: AddOutArticleComponent;
  let fixture: ComponentFixture<AddOutArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOutArticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOutArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
