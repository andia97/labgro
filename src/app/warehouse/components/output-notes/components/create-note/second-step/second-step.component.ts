import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';


import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { AuthConstants } from '@core/config/auth-constanst';
import { ArticleIncome } from '@core/models/article-incomes.model';
import { StorageService } from '@core/services/storage/storage.service';

import Swal from 'sweetalert2'
import { EditOutArticleComponent } from './edit-out-article/edit-out-article.component';
import { EmptyData } from '@shared/components/empty-state/empty-state.component';
import { AddOutArticleComponent } from './add-out-article/add-out-article.component';
import { ArticleOut } from '@core/models/article-out.model';



@Component({
  selector: 'app-second-step',
  templateUrl: './second-step.component.html',
  styleUrls: ['./second-step.component.css']
})
export class SecondStepComponent implements OnInit{
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  
  displayedColumns = ['in_count', 'unit', 'description', 'budget_output', 'code_article', 'unit_price', 'total_price', 'actions'];
  articles!: ArticleOut[];



  dataForm!: FormGroup;
  @Output() emittDataForm: EventEmitter<FormGroup> = new EventEmitter();
  
  //variable to confirm save data
  clicked: number = 0;
  articlesAdded!: any[];

  emptyData: EmptyData = {
    title: 'Aun no hay lista de articulos salientes',
    subtitle: '¿Que articulos van a salir hoy?',
    btn: 'Articulos',
    assets: 'assets/svg/empty-income-article.svg'
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dialogRef: MatDialog,
    private formBuilder: FormBuilder,
    private storageService: StorageService,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    //verify if exist data article in the local storage and construc
    // data article from the local storage
    if (localStorage.getItem('data_in_article')) {
      this.storageService.get(AuthConstants.DATA_IN_ARTICLE).then(
        (data: any) => {
          // console.log(data);
          data.forEach((element: any) => {
            this.addArticleForm(element)
          });
          this.articles = this.articlesData.value;
          const total = this.getTotalCost();
          this.dataForm.controls.total.setValue(total);
          // console.log(this.articles);
        }
      );
    }
    
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      total: [''],
      articles: this.formBuilder.array([])
    });
  }

  // add article form from local storage
  addArticleForm(data: any): void {
    const form = this.formBuilder.group({
      article: [data.article],
      article_id: [data.article_id],
      quantity: [data.quantity],
      budget_output: [data.budget_output],
      unit_price: [data.unit_price],
      total_price: [data.total_price]
    });
    this.articlesData.push(form);
  }

  // open dialog for add new article income
  addArticle(): void {
    const dialog = this.dialogRef.open(AddOutArticleComponent, {
      width: '378px',
      minHeight: '352px',
      panelClass: 'custom-dialog-container',
      disableClose: true
    });

    dialog.afterClosed().subscribe(
      (res: FormGroup) => {
        if (res) {
          const articleId = res.value.article_id;

          const isExist = this.articles? this.articles.find(element => element.article_id === articleId) : false;
          if (isExist) {
            Swal.fire({
              position: 'center',
              icon: 'warning',
              // iconColor: '#85CE36',
              title: 'Este articulo ya existe en tu lista de entradas, selecciona otro articulo',
              showConfirmButton: true,
              confirmButtonColor: '#85CE36'
              // timer: 2000
            })
          } else {
            this.articlesData.push(res);
            this.articles = this.articlesData.value;
            const total = this.getTotalCost();
            this.dataForm.controls.total.setValue(total);
            this.storageService.store(AuthConstants.DATA_IN_ARTICLE, this.articles);
          }
        }
      }
    )
  }

  editArticle(article: any, index: number) {
    // console.log('Second' , articles);
    const articlesAdded = this.articlesData.value;
    const dialog = this.dialogRef.open(EditOutArticleComponent, {
      width: '378px',
      minHeight: '352px',
      panelClass: 'custom-dialog-container',
      disableClose: true,
      data: {
        articleData: article,
        articlesList: articlesAdded
      }
    });
    
    dialog.afterClosed().subscribe(
      (res: FormGroup) => {
        // this.articlesData.value;
        if (res) {
            this.articlesData.removeAt(index);
            this.articlesData.push(res);
            this.articles = this.articlesData.value;
            const total = this.getTotalCost();
            this.dataForm.controls.total.setValue(total);
            this.storageService.store(AuthConstants.DATA_IN_ARTICLE, this.articles);
        }
      }
    )
  }

  deleteArticle(article: ArticleIncome, index: number): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el artículo   : ' + article.article.name_article + ' ?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.articlesData.removeAt(index);
        this.articles = this.articlesData.value;
        const total = this.getTotalCost();
        this.dataForm.controls.total.setValue(total);
        this.storageService.store(AuthConstants.DATA_IN_ARTICLE, this.articles);
        swalWithBootstrapButtons.fire({
          title: '¡Eliminado!',
          text: "El artículo seleccionado ya no pertenece a tu lista de entradas",
          icon: 'success',
          showConfirmButton: false,
          timer: 1800
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu artículo esta a salvo",
          icon: 'error',
          showConfirmButton: false,
          timer: 1800
        });
      }
    })
  }

  saveData(): void {
    if (this.clicked === 1) {
      this.emittDataForm.emit(this.dataForm);
    } else {
      this.clicked = this.clicked + 1;
      // console.log(this.dataForm.value);
      Swal.fire({
        position: 'center',
        icon: 'warning',
        // iconColor: '#85CE36',
        title: 'Verifique que los datos sean correctos, una vez guardados no podra modificar por temas de auditoria',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
        // timer: 2000
      })
    }

  }

  // array of articles
  get articlesData(): FormArray {
    return this.dataForm.get('articles') as FormArray;
  }

  /** Gets the total cost of all transactions. */
  getTotalCost() {
    return this.articles.map(t => t.total_price).reduce((acc, value) => acc + value, 0);
  }
}
