import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  templateUrl: './edit-out-article.component.html',
  styleUrls: ['./edit-out-article.component.css']
})
export class EditOutArticleComponent implements OnInit {


  // array of multi article from data base 
  options: Article[] = [];

  // variable for the change detection of autocomplete input
  filteredOptions!: Observable<Article[]>;
  
  // variable for the data form
  dataArticleForm!: FormGroup;

  // variable use only given the unit price last
  article: Article = this.data.articleData.article;

  state: boolean = false;

  
  //Validators when user touchet type data on input
  get articleData():any {
    return this.dataArticleForm.get('article');
  }

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }

  get budget_output():any {
    return this.dataArticleForm.get('budget_output');
  }


  // Messages for validators
  public errorsMessages = {
    articleData: [
      { type: 'required', message: 'Selecciona un articulo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'max', message: 'La cantidad entregada supera el stock'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
    budget_output: [
      { type: 'required', message: 'La partida presupuesto es requerido' },
      { type: 'min', message: 'Debe introducir numeros mayores a 0.'},
      { type: 'max', message: 'Debe introducir como máximo 9 digitos.'},
      { type: 'pattern', message: 'Debe introducir solo números'},
    ],
  }
  numRegex = /^-?\d*[.,]?\d{0,2}$/;
  onlyNumber = '^[0-9]+$';

  constructor(
    private articleService: ArticleService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditOutArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.buildForm();
    this.dataArticleForm.patchValue(data.articleData);
  }
  
  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ , 
        [Validators.required]
      ],
      article_id: [ , 
        [
          Validators.required
        ]
      ],
      quantity: ['',      [
        Validators.max(this.article.stock), 
        Validators.required,
        Validators.pattern(
          this.numRegex
        ),
        Validators.min(1),
      ]],
      budget_output: ['', 
        [
          Validators.required,
          Validators.min(1),
          Validators.max(999999999),
          Validators.pattern(this.onlyNumber)
        ]
      ],
      total_price: ['']
    });
  }
  
  ngOnInit(): void {
    this.getArticles();
  }
  
  getArticles(): void {
    this.state = true;
    this.articleService.getArticles().subscribe(
      (res: Article[]) => {
        this.state = false;
        // console.log(res);
        this.verifyArticlesList(res);
        this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name_article),
          map(name_article => name_article ? this._filter(name_article) : this.options.slice())
        );
      },
      (error: any) => {
        console.log(error);
        alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
      },
    )
  }

  saveArticle(): void {
    // console.log(this.dataArticleForm.value);
    const quantity = this.dataArticleForm.controls.quantity.value;
    const unit_price = this.article.unit_price;
    let totalPrice = quantity * unit_price;
    console.log(unit_price);
    console.log(totalPrice);
    
    this.dataArticleForm.controls.total_price.setValue(totalPrice);
    this.dialogRef.close(this.dataArticleForm)
  }

  getArticle(article: Article): void {
    this.dataArticleForm.controls.article_id.setValue(article.id); 
    this.article = article;
    const stock = this.article.stock;
    this.dataArticleForm.controls.quantity.setValidators(
      [
        Validators.max(this.article.stock), 
        Validators.required,
        Validators.pattern(
          this.numRegex
        ),
        Validators.min(1),
      ])
  }

  verifyArticlesList(data: Article[]) {
    // console.log(this.data.articlesList);
    this.data.articlesList.forEach((element: any) => {
      const articleId = element.article_id;
      // console.log(articleId);
      data.forEach(element => {
        if (element.id === articleId && element.id !== this.data.articleData.article_id) {
          // console.log('eliminar');
          const i = data.indexOf( element );
          if ( i !== -1 ) {
            data.splice( i, 1 );
          }
        }
      });
      // return data;
    });
    this.options = data;
  }

  // Methods for the autocomplete
  displayFn(article: Article): string {
    return article && article.name_article ? article.name_article : '';
  }
  

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        // console.log(value);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }
}
