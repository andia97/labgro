import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOutArticleComponent } from './edit-out-article.component';

describe('EditOutArticleComponent', () => {
  let component: EditOutArticleComponent;
  let fixture: ComponentFixture<EditOutArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditOutArticleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOutArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
