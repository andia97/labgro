import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AuthConstants } from '@core/config/auth-constanst';
import { StorageService } from '@core/services/storage/storage.service';

import {default as _rollupMoment, Moment} from 'moment';
import * as _moment from 'moment';
import { OutputArticleService } from '@core/services/output-article/out-article.service';

const moment = _rollupMoment || _moment;



@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.css']
})
export class FirstStepComponent implements OnInit{

  dataGeneralForm!: FormGroup;
  @Output() emitterDataForm: EventEmitter<FormGroup> = new EventEmitter();
  areas!: any[];
  
  get receipt(): any {
    return this.dataGeneralForm.get('receipt');
  }

  get area(): any {
    return this.dataGeneralForm.get('name');
  }

  get order_number(): any {
    return this.dataGeneralForm.get('order_number');
  }
  

  // Messages for validators
  public errorsMessages = {
    receipt: [
      { type: 'required', message: 'El nro de comprobante es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 dígitos.'},
      { type: 'min', message: 'Debe introducir números mayores a 0'},
      { type: 'pattern', message: 'Solo se permite números'},
    ],
    areas: [
      { type: 'required', message: 'El area es requerido' },
    ],
    order_number: [
      { type: 'required', message: 'El nro de pedido es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 dígitos.'},
      { type: 'pattern', message: 'Solo se permite números'},
    ],
  }

  minDate!: Date;
  maxDate!: Date;
  currentYear = new Date().getFullYear();
  currentMonth = new Date().getMonth();
  myDateFilter = (d: Date | null): boolean => {
    const year = (d || new Date()).getFullYear();
    const month = (d || new Date()).getMonth();
    return year >= this.currentYear -1 && year <= this.currentYear + 1  &&
           month >= this.currentMonth && month <= this.currentMonth + 1;
  } 

  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private outputArticleService: OutputArticleService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.getAreas();
    if (localStorage.getItem('data_in_note')) {
      // console.log('existe');
      this.storageService.get(AuthConstants.DATA_IN_NOTE).then(
        (data: any) => {
          // console.log(data);
          this.dataGeneralForm.patchValue(data);
          this.dataGeneralForm.controls.output_date.setValue(data.order_date);
          this.dataGeneralForm.controls.delivery_output_date.setValue(data.delivery_date);
        }
      );
    }
  }

  getAreas(): void {
    this.outputArticleService.getAreas().subscribe(
      (res: any[]) => {
        // console.log(res);
        this.areas = res;
      }
    );
  }

  private buildForm(): void {
    this.dataGeneralForm = this.formBuilder.group({
      receipt: ['', 
        [
          Validators.required,
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'), 
        ]],
        order_number: ['', 
        [
          Validators.maxLength(9),
          Validators.required,
          Validators.pattern('^[0-9]*$'), 
        ]],
        section_id: ['', [Validators.required]],
        name: ['', [Validators.required]],
      order_date: [ new Date, [Validators.required]],
      delivery_date: [ new Date, [Validators.required]],
      output_date: [
        {
          value: new Date,
          disabled: true
        },
      ],
      delivery_output_date: [
        {
          value: new Date,
          disabled: true
        },
      ]
    });
  }

  addEvent(event: MatDatepickerInputEvent<Date>, option: string) {
    const newDateInput = event.value;
    if (option === 'order') {
      this.dataGeneralForm.controls.order_date.setValue(newDateInput);
    } else {
      this.dataGeneralForm.controls.delivery_date.setValue(newDateInput);
    }
  }

  saveGeneralData(): void {
    // console.log(this.dataGeneralForm.value);
    this.emitterDataForm.emit(this.dataGeneralForm);
    const data = this.dataGeneralForm.value;
    this.storageService.store(AuthConstants.DATA_IN_NOTE, data);
  }

}
