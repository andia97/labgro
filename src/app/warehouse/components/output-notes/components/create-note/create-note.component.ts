import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { AuthConstants } from '@core/config/auth-constanst';
import { ArticleIncome } from '@core/models/article-incomes.model';

import { IncomesArticleService } from '@core/services/incomes-article/incomes-article.service';
import { StorageService } from '@core/services/storage/storage.service';

import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { MessageService } from '@core/services/menssage/message.service';
import { OutputArticleService } from '@core/services/output-article/out-article.service';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.css']
})
export class CreateNoteComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  dataForm!: FormGroup;
  state: boolean = false;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private formBuilder: FormBuilder,
    private outputArticleService: OutputArticleService,
    private storageService: StorageService,
    private router: Router,
    private messageServie: MessageService,
    ) {
      this.buildForm();
    }

  ngOnInit(): void {
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      receipt: [''],
      section_id: [''],
      order_number: [''],
      order_date: [''],
      delivery_date: [''],
      total: [''],
      details: this.formBuilder.array([])
    });
  }

  getFirstDataForm(event: FormGroup): void {
    // console.log(event);
    const firstData = event.value;
    this.dataForm.patchValue(firstData);
    // console.log(this.articles.value);
  }

  async getSecondDataForm(event: FormGroup) {
    // console.log(event);
    const articles: ArticleIncome[] = event.value.articles;
    articles.forEach(element => {
        this.addArticleForm(element);
    });
    const secondData = event.value;
    this.dataForm.patchValue(secondData);

    this.saveAllDataNote();
  }

  saveAllDataNote(): void {
    this.state = true;
    const data = this.dataForm.value;
    data.order_date = `${data.order_date.getFullYear()}-${data.order_date.getMonth()+1}-${data.order_date.getDate()}`;
    data.delivery_date = `${data.delivery_date.getFullYear()}-${data.delivery_date.getMonth()+1}-${data.delivery_date.getDate()}`;

    this.outputArticleService.addOutputNote(data).subscribe(
      (res: any) => {
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'success',
          // iconColor: '#85CE36',
          title: 'Guardado Correctamente',
          text: 'La nota de entrega se guardó correctamente',
          showConfirmButton: false,
          // confirmButtonColor: '#85CE36'
          timer: 2000
        })
        this.router.navigate(['/warehouse/salidas']);
        this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
        this.storageService.remove(AuthConstants.DATA_IN_NOTE);
      },
      (error: any) => {
        const message = this.messageServie.get();
        const errors = message.errors;
        const messageList: string[]  = [];

        if (errors.delivery_date) {
          const errorList: string[] = errors.delivery_date;
          errorList.forEach(element => {
            messageList.push(element);
          });
        }

        if (errors.order_date) {
          const errorList: string[] = errors.order_date;
          errorList.forEach(element => {
            messageList.push(element);
          });
        }

        if (errors.order_number) {
          const errorList: string[] = errors.order_number;
          errorList.forEach(element => {
            messageList.push(element);
          });
        }

        if (errors.receipt) {
          const errorList: string[] = errors.receipt;
          errorList.forEach(element => {
            messageList.push(element);
          });
        }

        if (errors.section_id) {
          const errorList: string[] = errors.section_id;
          errorList.forEach(element => {
            messageList.push(element);
          });
        }

        this.state = false;
        Swal.fire({
          icon: 'error',
          title: 'Los datos no se guardaron correctamente',
          // html: `<p *ngFor="let error of ${messageList}">{{${error}}}</p>`,
          text: messageList.join('-'),
          // html: '<br>',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36',
          // timer: 2000
        })
        // console.log(error);
      },
      )
  }

  goBack(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de volver atras?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'No, continuar',
      cancelButtonText: 'Si, volver',
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire({
          title: 'Continua configurando',
          text: "No te preocupes, tu datos estan a salvo",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 1800
        });
      } else {

        if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          this.router.navigate(['/warehouse/salidas/']);
          this.storageService.remove(AuthConstants.DATA_IN_NOTE);
          this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
        }
      }
    })
  }


  // add article form from local storage
  addArticleForm(data: any): void {
    const form = this.formBuilder.group({
      article_id: [data.article_id],
      quantity: [data.quantity],
      budget_output: [data.budget_output],
      total: [data.total_price]
    });
    this.articles.push(form);
  }
  get articles(): FormArray {
    return this.dataForm.get('details') as FormArray;
  }
}
