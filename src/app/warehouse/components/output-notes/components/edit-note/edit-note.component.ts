import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OutputArticleService } from '@core/services/output-article/out-article.service';
import  Sweet from 'sweetalert2';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
})
export class EditNoteComponent implements OnInit {

  dataGeneralForm!: FormGroup;
  areas!: any[];
  state: boolean = false;
  
  get receipt(): any {
    return this.dataGeneralForm.get('receipt');
  }

  get area(): any {
    return this.dataGeneralForm.get('name');
  }

  get order_number(): any {
    return this.dataGeneralForm.get('order_number');
  }
  

  // Messages for validators
  public errorsMessages = {
    receipt: [
      { type: 'required', message: 'El nro de comprobante es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 dígitos.'},
      { type: 'min', message: 'Debe introducir números mayores a 0'},
    ],
    areas: [
      { type: 'required', message: 'El area es requerido' },
    ],
    order_number: [
      { type: 'required', message: 'El nro de pedido es requerido' },
      { type: 'max', message: 'Debe introducir como máximo 9 dígitos.'},
    ],
  }

  minDate!: Date;
  maxDate!: Date;
  currentYear = new Date().getFullYear();
  currentMonth = new Date().getMonth();
  myDateFilter = (d: Date | null): boolean => {
    const year = (d || new Date()).getFullYear();
    const month = (d || new Date()).getMonth();
    return year >= this.currentYear -1 && year <= this.currentYear + 1  &&
           month >= this.currentMonth && month <= this.currentMonth + 1;
  } 

  constructor(
    private formBuilder: FormBuilder,
    private outputArticleService: OutputArticleService,
    private dialogRef: MatDialogRef<EditNoteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.getAreas();
    const dataForm = this.data.data;
    console.log(dataForm);
    
    this.dataGeneralForm.patchValue(dataForm);
    this.dataGeneralForm.controls.output_date.setValue(dataForm.order_date);
    this.dataGeneralForm.controls.delivery_output_date.setValue(dataForm.delivery_date);
  }

  getAreas(): void {
    this.outputArticleService.getAreas().subscribe(
      (res: any[]) => {
        // console.log(res);
        this.areas = res;
      }
    );
  }

  private buildForm(): void {
    this.dataGeneralForm = this.formBuilder.group({
      receipt: ['', [Validators.required, Validators.min(1), Validators.max(9999999999), Validators.maxLength(9)]],
      section_id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      order_number: ['', [Validators.required, Validators.max(9999999999)]],
      order_date: [ new Date, [Validators.required]],
      delivery_date: [ new Date, [Validators.required]],
      output_date: [
        {
          value: new Date,
          disabled: true
        },
      ],
      delivery_output_date: [
        {
          value: new Date,
          disabled: true
        },
      ]
    });
  }

  addEvent(event: MatDatepickerInputEvent<Date>, option: string) {
    const newDateInput = event.value;
    if (option === 'order') {
      this.dataGeneralForm.controls.order_date.setValue(newDateInput);
    } else {
      this.dataGeneralForm.controls.delivery_date.setValue(newDateInput);
    }
  }

  saveGeneralData(): void {
    // console.log(this.dataGeneralForm.value);
    this.state = true;
    // console.log(this.dataGeneralForm.value);
    const data = this.dataGeneralForm.value;
    this.outputArticleService.updateOutputNote(data).subscribe(
      (res: any) => {
        this.state = false;
        console.log(res);
        Sweet.fire({
          icon: 'success',
          title: 'Modificado correctamente',
          text: 'Los datos se modificaron correctamente',
          showConfirmButton: false,
          timer: 3000
        });
        this.dialogRef.close(true);
      },
      (error: any) => {
        this.state = false;
        // console.log(error);
        Sweet.fire({
          icon: 'error',
          title: '¡Oops, ocurrio un error!',
          text: 'Los datos no se modificaron correctamente, contactese con administración',
          showConfirmButton: false,
          timer: 3000
        });
      },
    );

  }

}
