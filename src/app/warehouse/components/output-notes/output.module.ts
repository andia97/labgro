import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutputRoutingModule } from './output-routing.module';
import { MaterialModule } from '@material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';

import { LayoutNotesComponent } from './components/layout-notes/layout-notes.component';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { EditNoteComponent } from './components/edit-note/edit-note.component';
import { DataListNoteContainer } from './container/data-list-note/data-list-note.container';
import { FirstStepComponent } from './components/create-note/first-step/first-step.component';
import { SecondStepComponent } from './components/create-note/second-step/second-step.component';
import { EditOutArticleComponent } from './components/create-note/second-step/edit-out-article/edit-out-article.component';
import { AddOutArticleComponent } from './components/create-note/second-step/add-out-article/add-out-article.component';
import { OutDetailsContainer } from './container/out-details/out-details.container';



@NgModule({
  declarations: [
    LayoutNotesComponent,
    CreateNoteComponent,
    EditNoteComponent,
    DataListNoteContainer,
    FirstStepComponent,
    SecondStepComponent,
    AddOutArticleComponent,
    EditOutArticleComponent,
    OutDetailsContainer
  ],
  imports: [
    CommonModule,
    OutputRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class OutputModule { }
