import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { WarehouseRoutingModule } from './warehouse-routing.module';
import { MaterialModule } from '@shared/material/material.module';
import { SharedModule } from '@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutWarehouseComponent } from './components/layout-warehouse/layout-warehouse.component';
import { LayoutModule } from '@angular/cdk/layout';
import { SideNavService } from '@shared/services/side-nav.service';
import { AddeditUnitmasureComponent } from './components/article/addedit-unitmasure/addedit-unitmasure.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    LayoutWarehouseComponent,
    AddeditUnitmasureComponent,
  ],
  imports: [
    CommonModule,
    WarehouseRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    LayoutModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers:[
    SideNavService,
  ]
})
export class WarehouseModule { }
