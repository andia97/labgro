import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainNavComponent } from '@shared/components/main-nav/main-nav.component';

import { HomeComponent } from './components/article/home/home.component';
import { InformesComponent } from './components/informes/informes.component';
import { LayoutWarehouseComponent } from './components/layout-warehouse/layout-warehouse.component';
import { NotificationComponent } from './components/notificaciones/notification.component';
import { PerfilComponent } from './components/perfil/perfil.component';


const routes: Routes = [
 {
   path: '',
   component: LayoutWarehouseComponent,
   children: [
     {
       path: '',
       redirectTo: 'articulo',
       pathMatch: 'full'
     },
     {
       path: 'articulo', 
       loadChildren: () => import('./components/article/article.module').then(m => m.ArticleModule)
     },

     {
       path: 'area', 
       loadChildren: () => import('./components/area/area.module').then(m => m.AreaModule)
      },
      
      {
       path: 'informe',
       loadChildren: () => import('./components/informes/informes.module').then(m => m.InformesModule)
      },
      
      {
        path: 'notificaciones',
        loadChildren: () => import('./components/notificaciones/notification.module').then(m => m.NotificationModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('./components/perfil/perfil.module').then(m => m.PerfilModule)
      },
      {
        path: 'entradas',
        loadChildren: () => import('./components/input-notes/input-output.module').then(m => m.InputOutputModule)
      },
      {
        path: 'salidas',
        loadChildren: () => import('./components/output-notes/output.module').then(m => m.OutputModule)
      },
   ]
}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule { }
