import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutProducerComponent } from './components/layout-producer/layout-producer.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutProducerComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      {
        path: 'home', 
        loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'product', 
        loadChildren: () => import('./components/production/production.module').then(m => m.ProductionModule)
      },
      {
        path: 'production-detail', 
        loadChildren: () => import('./components/production-detail/production-detail.module').then(m => m.ProductionDetailModule)
      },
      {
        path: 'production-summary', 
        loadChildren: () => import('./components/production-summary/production-summary.module').then(m => m.ProductionSummaryModule)
      },
      {
        path: 'sales-detail', 
        loadChildren: () => import('./components/sales-detail/sales-detail.module').then(m => m.SalesDetailModule)
      },
      {
        path: 'sales-summary', 
        loadChildren: () => import('./components/sales-summary/sales-summary.module').then(m => m.SalesSummaryModule)
      },
      {
        path: 'consolidation', 
        loadChildren: () => import('./components/consolidation/consolidation.module').then(m => m.ConsolidationModule)
      },
      {
        path: 'warehouse', 
        loadChildren: () => import('./components/warehouses/warehouses.module').then(m => m.WarehousesModule)
      },
    ]
   }
  
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProducerRoutingModule { }
