import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProducerRoutingModule } from './producer-routing.module';
import { LayoutProducerComponent } from './components/layout-producer/layout-producer.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [
    LayoutProducerComponent
  ],
  imports: [
    CommonModule,
    ProducerRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ]
})
export class ProducerModule { }
