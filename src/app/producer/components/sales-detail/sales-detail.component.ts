import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { ProductionService } from '@core/services/production/production.service';
import { Moment } from 'moment';
import { AddSaleComponent } from './components/add-sale/add-sale.component';
import { EditSaleComponent } from './components/edit-sale/edit-sale.component';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-sales-detail',
  templateUrl: './sales-detail.component.html',
  styleUrls: ['./sales-detail.component.css']
})
export class SalesDetailComponent implements OnInit {

  date: Date = new Date();
  dateLabel: Date = new Date();
  dateSelected: FormControl = new FormControl(
    {
      value: this.date,
      disabled: true
    }
  );

  isBoxClicked: boolean = false;
  isCodeClicked: boolean = false;

  boxForm: FormControl = new FormControl('1');
  codeForm: FormControl = new FormControl('Agr-prod-tec-12-619');

  displayedColumns: string[] = [
    'nro',
    'code',
    'product',
    'date',
    'type',
    'destiny',
    'order_number',
    'unit',
    'sale',
    'quantity',
    'amount',
    'second_quantity',
    'second_amount',
    'third_quantity',
    'third_amount',
    'fourth_quantity',
    'fourth_amount',
    'action',
  ];
  dataSource: any[] = [1,1,1,1,1,1];

  constructor(
    private productionService: ProductionService,
    private dialog: MatDialog,
  ) {

  }

  ngOnInit(): void {
  }

  fetchConsolidationTable(year, month){
    this.productionService.getReportConsolidation(year, month)
      .subscribe(
        (res: any)=>{
          const arrAux: any[]=[];
          res.map((elem,i)=>{
            let newElement: any={...elem,'nro':100+i};
            arrAux.push(newElement)
          })
          this.dataSource=arrAux;
        },(err)=>{
          console.error(err)
        }
      )
  }

  chosenYearHandler(normalizedYear) {
    this.dateSelected.setValue(new Date(normalizedYear));
    this.dateLabel=new Date(normalizedYear);
  }

  chosenMonthHandler(normalizedMonth, datepicker: MatDatepicker<Moment>) {
    this.dateSelected.setValue(new Date(normalizedMonth));
    this.dateLabel=new Date(normalizedMonth);
    datepicker.close();
    this.fetchConsolidationTable(this.dateLabel.getFullYear(), this.dateLabel.getMonth()+1);
  }

  addSale(): void {
    const dialogRef = this.dialog.open(AddSaleComponent, {
      width: '500px',
      height: '750px',
      panelClass: 'custom-dialog-container',
    });
  }

  editSale(): void {
    const dialogRef = this.dialog.open(EditSaleComponent, {
      width: '500px',
      height: '750px',
      panelClass: 'custom-dialog-container',
    });
  }

  isClicked(option: string): void {
    switch (option) {
      case 'box':
        if (this.isBoxClicked) {
          this.isBoxClicked = false;
        } else {
          this.isBoxClicked = true;
        }
        break;
      case 'code':
        if (this.isCodeClicked) {
          this.isCodeClicked = false;
        } else {
          this.isCodeClicked = true;
        }
        break;
      default:
        console.log('Dont nothing');
        break;
    }

  }

  calculateBackground(i: number): string {
    if (i % 2 === 0) {
      return '#D7DDE4'
    } else {
      return '#ffffff'
    }
  }
  
  public downloadPDF(): void {
   
    const doc = new jsPDF('L', 'mm', 'A4');
    const DATA : any = document.getElementById('tbls-sumary'); 
    console.log(DATA);
  
    const options = {
      background: 'white',
      scale: 3
    }

    html2canvas(DATA, options).then(
      (canvas) => {
        
      const img = canvas.toDataURL('image/PNG');

     
      const bufferX = 15;
      const bufferY = 1;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth + 4) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }
}
