import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesDetailRoutingModule } from './sales-detail-routing.module';
import { SalesDetailComponent } from './sales-detail.component';

import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSaleComponent } from './components/add-sale/add-sale.component';
import { EditSaleComponent } from './components/edit-sale/edit-sale.component';


@NgModule({
  declarations: [
    SalesDetailComponent,
    AddSaleComponent,
    EditSaleComponent
  ],
  imports: [
    CommonModule,
    SalesDetailRoutingModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SalesDetailModule { }
