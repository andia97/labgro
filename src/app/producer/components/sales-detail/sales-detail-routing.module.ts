import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesDetailComponent } from './sales-detail.component';

const routes: Routes = [
  { path: '', component: SalesDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesDetailRoutingModule { }
