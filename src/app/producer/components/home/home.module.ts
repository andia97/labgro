import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { StatisticalDataComponent } from './container/statistical-data/statistical-data.component';
import { ProductionHistoryComponent } from './container/production-history/production-history.component';
import { ProductionSummaryComponent } from './container/production-summary/production-summary.component';
import { RawMaterialComponent } from './container/raw-material/raw-material.component';
import { SaleComponent } from './container/sale/sale.component';
import { SaleDetailComponent } from './container/sale-detail/sale-detail.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [
    HomeComponent,
    StatisticalDataComponent,
    ProductionHistoryComponent,
    ProductionSummaryComponent,
    RawMaterialComponent,
    SaleComponent,
    SaleDetailComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    SharedModule,
    NgxChartsModule
  ]
})
export class HomeModule { }
