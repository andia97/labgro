import { Component, OnInit } from '@angular/core';
import { presentation } from '../data';

@Component({
  selector: 'sale-data',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {
  presentation!: any[];
  view: any = [500, 270];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
    Object.assign(this, { presentation });
  }

  ngOnInit(): void {
      
  }

}
