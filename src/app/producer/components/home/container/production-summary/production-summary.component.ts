import { Component, OnInit } from '@angular/core';
import { single } from '../data';

@Component({
  selector: 'production-summary-data',
  templateUrl: './production-summary.component.html',
  styleUrls: ['./production-summary.component.css']
})
export class ProductionSummaryComponent implements OnInit {

  single!: any[];
  view: any = [530, 300];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: any = 'right';

  colorScheme = {
    domain: ['#3CC099', '#8CFC9D', '#C1F33D', '#A8385D', '#FFB800', '#2196F3']
  };

  constructor() {
    Object.assign(this, { single });
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }
  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
