export var multi = [
  {
    "name": "Nectar Frutilla",
    "series": [
      {
        "name": "Ene",
        "value": 894
      },
      {
        "name": "Feb",
        "value": 500
      },
      {
        "name": "Mar",
        "value": 720
      },
      {
        "name": "Abr",
        "value": 856
      },
      {
        "name": "May",
        "value": 235
      },
      {
        "name": "Jun",
        "value": 963
      },
      {
        "name": "Jul",
        "value": 752
      },
      {
        "name": "Ago",
        "value": 552
      },
      {
        "name": "Sep",
        "value": 321
      },
      {
        "name": "Oct",
        "value": 568
      },
      {
        "name": "Nov",
        "value": 0
      },
      {
        "name": "Dic",
        "value": 853
      },
    ]
  },

  {
    "name": "Nectar de Mango",
    "series": [
      {
        "name": "Ene",
        "value": 258
      },
      {
        "name": "Feb",
        "value": 852
      },
      {
        "name": "Mar",
        "value": 9
      },
      {
        "name": "Abr",
        "value": 15
      },
      {
        "name": "May",
        "value": 20
      },
      {
        "name": "Jun",
        "value": 96
      },
      {
        "name": "Jul",
        "value": 36
      },
      {
        "name": "Ago",
        "value": 23
      },
      {
        "name": "Sep",
        "value": 520
      },
      {
        "name": "Oct",
        "value": 635
      },
      {
        "name": "Nov",
        "value": 10
      },
      {
        "name": "Dic",
        "value": 83
      },
    ]
  },

  {
    "name": "Nectar de Narajana",
    "series": [
      {
        "name": "Ene",
        "value": 369
      },
      {
        "name": "Feb",
        "value": 963
      },
      {
        "name": "Mar",
        "value": 71
      },
      {
        "name": "Abr",
        "value": 17
      },
      {
        "name": "May",
        "value": 856
      },
      {
        "name": "Jun",
        "value": 658
      },
      {
        "name": "Jul",
        "value": 987
      },
      {
        "name": "Ago",
        "value": 789
      },
      {
        "name": "Sep",
        "value": 52
      },
      {
        "name": "Oct",
        "value": 25
      },
      {
        "name": "Nov",
        "value": 36
      },
      {
        "name": "Dic",
        "value": 63
      },
    ]
  },
  {
    "name": "Nectar de Coco",
    "series": [
      {
        "name": "Ene",
        "value": 201
      },
      {
        "name": "Feb",
        "value": 102
      },
      {
        "name": "Mar",
        "value": 520
      },
      {
        "name": "Abr",
        "value": 250
      },
      {
        "name": "May",
        "value": 850
      },
      {
        "name": "Jun",
        "value": 523
      },
      {
        "name": "Jul",
        "value": 69
      },
      {
        "name": "Ago",
        "value": 96
      },
      {
        "name": "Sep",
        "value": 75
      },
      {
        "name": "Oct",
        "value": 235
      },
      {
        "name": "Nov",
        "value": 78
      },
      {
        "name": "Dic",
        "value": 985
      },
    ]
  }
];

export var single = [
  {
    "name": "Nectar de Coco",
    "value": 8940000
  },
  {
    "name": "Nectar de Narajana",
    "value": 5000000
  },
  {
    "name": "Nectar de Mango",
    "value": 7200000
  },
    {
    "name": "Nectar Frutilla",
    "value": 6200000
  }
];

export var material = [
  {
    "name": "Carne de Res",
    "value": 8940000
  },
  {
    "name": "Azucar",
    "value": 5000000
  },
  {
    "name": "Leche",
    "value": 7200000
  },
  {
    "name": "Sal",
    "value": 6200000
  },
  {
    "name": "Gelatina",
    "value": 6200000
  },
  {
    "name": "Endulsante",
    "value": 6200000
  },
];


export var presentation = [
  {
    "name": "Bolsas de 1 lt - Yogurt",
    "value": 89
  },
  {
    "name": "Embases de 700cc - Nectar ",
    "value": 50
  },
  {
    "name": "Bolsas de 500cc - Pulpa",
    "value": 72
  },
  {
    "name": "Bolsas de 500cc - Queso",
    "value": 62
  },
];