import { Component, OnInit } from '@angular/core';
import { multi } from '../data';
@Component({
  selector: 'production-data',
  templateUrl: './production-history.component.html',
  styleUrls: ['./production-history.component.css']
})
export class ProductionHistoryComponent implements OnInit {
  multi!: any[];
  view: any = [700, 380];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Mes';
  yAxisLabel: string = 'Productos';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#3CC099', '#8CFC9D', '#C1F33D', '#A8385D', '#FFB800', '#2196F3']
  };

  constructor() {
    Object.assign(this, { multi });
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
