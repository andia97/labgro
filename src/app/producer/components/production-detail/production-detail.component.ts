import {Component, OnInit, ViewChild} from '@angular/core';
import { CalendarProductionContainer } from "@producer/components/production-detail/container/calendar-production/calendar-production.container";

@Component({
  selector: 'app-production-detail',
  templateUrl: './production-detail.component.html',
  styleUrls: ['./production-detail.component.css']
})
export class ProductionDetailComponent implements OnInit {

  @ViewChild(CalendarProductionContainer, {static:true}) child: any;
  titleDownloand: string = 'Kardex Diario';
  constructor() { }

  ngOnInit(): void {
  }

  tabChange(event: any): void {
    // console.log(event);
    if (event.index === 1 ) {
      this.titleDownloand = 'Kardex Detalle de Producción';
    } else {
      this.titleDownloand = 'Kardex Diario';

    }

  }

}
