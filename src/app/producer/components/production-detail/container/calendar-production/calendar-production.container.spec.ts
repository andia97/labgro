import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarProductionContainer } from './calendar-production.container';

describe('CalendarProductionComponent', () => {
  let component: CalendarProductionContainer;
  let fixture: ComponentFixture<CalendarProductionContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalendarProductionContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarProductionContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
