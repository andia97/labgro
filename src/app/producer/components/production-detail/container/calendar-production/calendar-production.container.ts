import {Component, OnInit, ViewEncapsulation, LOCALE_ID, Renderer2, ViewChild} from '@angular/core';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { AddUnitProductionComponent } from '../../components/add-unit-production/add-unit-production.component';
import { ProductService } from "@core/services/product/product.service";
import { FormBuilder } from '@angular/forms';
import { MatCheckboxChange } from "@angular/material/checkbox";
import { EditUnitProductionComponent } from "@producer/components/production-detail/components/edit-unit-production/edit-unit-production.component";
import { MaterialService } from "@core/services/material/material.service";
import Swal from "sweetalert2";

import { registerLocaleData } from '@angular/common'
import localeES from '@angular/common/locales/es'

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';

import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import 'moment/locale/es';

import {ProductionService} from "@core/services/production/production.service";
import {ShoppingCartService} from "@core/services/shopping-cart/shopping-cart.service";

registerLocaleData(localeES, 'es');
@Component({
  selector: 'app-calendar-production',
  templateUrl: './calendar-production.container.html',
  styleUrls: ['./calendar-production.container.css'],
  providers: [
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'es-ES'
    },
    {
      provide: LOCALE_ID,
      useValue: 'es'
    },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarProductionContainer implements OnInit {
  @ViewChild('calendar') calendar!: MatCalendarCellClassFunction<Date>;
  selected: Date | null= new Date;
  formatDate: any= new Date;
  dateProductions!: any;

  loading=false;

  color: string = '#F85D5D';
  checked: boolean = false;
  displayedColumns: string[] = ['unit', 'quantity','price_production', 'price_sale', 'action'];
  dataSource!: any[];

  allProductsData: any[]=[];
  productsSelectedData:any[]=[];

  productPresentationsData!: any[];
  canRender=false;
  productSelected: any={};

  production!: any;

  productMaterials!: any[];

  isToday: boolean=true;

  daySelectedWithProductions=false;

  ArrowBtns:any=NodeList;


  breackPointCalendarCols: number = 3;
  breackPointCalendarRows: number = 3;
  breackPointMainCols: number = 9;
  breackPointMainRows: number = 6;
  breackPointMaterialCols: number = 3;
  breackPointMaterialRows: number = 3;
  constructor(
    private dialog: MatDialog,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private materialService: MaterialService,
    private productionService: ProductionService,
    private cartService: ShoppingCartService,
    private renderer: Renderer2,
    private _adapter: DateAdapter<any>
  ) {

  }

  ngOnInit(): void {
    const date=new Date();
    this.buildCalendarMarkdown(date.getFullYear(),date.getMonth()+1);
    this.productionService.getProductionsByDate(`${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`)
      .subscribe(
        (res: any)=>{
          if(res.sucess){
            this.fillProducts(res);
            this.fetchProduct();
            this.production = res.production;
          }else{
            this.fetchProduct();
          }
        },(err: any)=>{
          console.error(err);
        }
      )
  }

  onResize(event) {
    this.breackPointCalendarCols = (event.target.innerWidth <= 725) ? 12 : 3;
    this.breackPointCalendarRows = (event.target.innerWidth <= 725) ? 8 : 3;
    this.breackPointMainCols = (event.target.innerWidth <= 725) ? 12 : 9;
    this.breackPointMainRows = (event.target.innerWidth <= 725) ? 8 : 6;
    this.breackPointMaterialCols = (event.target.innerWidth <= 725) ? 12 : 3;
    this.breackPointMaterialRows = (event.target.innerWidth <= 725) ? 8 : 3;
  }


  dateClass() {
    this.ArrowBtns.length===0 && this.onCalendarCharged();
    return (cellDate, view): MatCalendarCellClassFunction<Date> => {
      let classCss: any = '';
      if (view === 'month') {
        // console.log(cellDate);
        const cellDay = cellDate._d.toDateString();
        const today: any = this.selected?.toDateString();
        if (cellDay === today) {
          classCss = 'custom-date-class';
        } else {
          if (this.dateProductions?.includes(cellDay)) {
            classCss = 'second-date-class';
          }
        }
        return classCss;
      }
      return classCss;
    }
  }

  buildCalendarMarkdown(year: number,month: number){
    this.productionService.getProductionsDates(year,month)
      .subscribe(
        (res: any)=>{
          let arrDates: any[] = [];
          res.productions.map((item)=>{
            const thatDay=new Date(item.date_production);
            thatDay.setDate(thatDay.getDate()+1);
            arrDates.push(thatDay.toDateString());
          })
          this.dateProductions = arrDates;
        },(err)=>{
          console.error(err)
        }
      )
  }

  onCalendarCharged() {
    this.ArrowBtns = document
      .querySelectorAll('.mat-calendar-previous-button, .mat-calendar-next-button');
      if (this.ArrowBtns) {
        Array.from(this.ArrowBtns).forEach(button => {
          this.renderer.listen(button, 'click', () => {
            const span=document.getElementById('mat-calendar-button-0');
            const dateLabel = new Date(span!.innerHTML)
            this.buildCalendarMarkdown(dateLabel.getFullYear(), dateLabel.getMonth()+1);

          });
        });
    }
  }

  fetchGetProductionByDay(){
    this.loading=true;
    this.productionService.getProductionsByDate(this.formatDate)
      .subscribe(
        (res: any)=>{
          // console.log(res);
          if(res.sucess){
            this.fillProducts(res);
          }else{
            this.daySelectedWithProductions=false;
            this.productsSelectedData=[];
            const today = new Date();
            if( `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}`=== this.formatDate ){
              this.fetchProduct();
            }
          }
          this.loading=false;
        },(err: any)=>{
          console.error(err);
        }
      )
  }

  fillProducts(res){
    const dateProduction = new Date(res.production.date_production);
    dateProduction.setDate(dateProduction.getDate()+1)
    const today = new Date();
    const newArr:any[]=[];
    res.products.map((item)=>{
      const newPresentations:any[]=[];
      item.presentations.map((present)=>{
        newPresentations.push({
          quantity:present.quantity,
          unit_price_sale: present.unit_price_sale,
          unit_cost_production: present.unit_cost_production,
          unit:{
            id: present.id,
            name: present.name,
            unit_cost_production: present.unit_cost_production,
            unit_price_sale: present.unit_price_sale
          },
        });
      })
      const newMaterials: any[]=[];
      item.materials.map((mate)=>{
        newMaterials.push({
          nombre: mate.name_article,
          id: mate.id,
          unit_measure: mate.unit_measure,
          stock_requerid: mate.quantity_required
        });
      });
      newArr.push({
        "product": {
          id:item.id,
          name: item.name,
          code: item.code,
          image: item.image
        },
        "checked":true,
        "presentations":newPresentations,
        "materials":newMaterials,
        "canProduce":false,
        "quantity":item.quantity,
        "focus":false,
        "produced":true,
        "isToday": dateProduction.toDateString()===today.toDateString()
      })
    })
    this.production = res.production;
    this.productsSelectedData=newArr;
    this.daySelectedWithProductions=true;
    this.canRender=true;
  }



  fetchProduct(): void{
    this.loading=true;
    this.productService.getProducts("","","","")
      .subscribe(
        (res: any)=>{
          // console.log(res);
          // console.log(this.productsSelectedData);

          const newArr:any[]=[];
          res.map((item)=>{
            if(!this.productsSelectedData.find((element,index)=>{
              return element.product.id===item.id
            })){
              newArr.push({
                "product":item,
                "checked":false,
                "presentations":[],
                "materials":[],
                "canProduce":false,
                "quantity":0,
                "focus":false,
                "produced":false,
                "isToday":true
              })
            }

          })
          this.allProductsData=newArr;
          this.canRender=true;
          this.loading=false;
        },(err)=>{
          console.error(err);
          this.loading=false;

          if(err==="No se encontraron resultados"){
            Swal.fire({
              position: 'center',
              icon: 'warning',
              title: 'No existen productos creados',
              showConfirmButton: true,
              confirmButtonColor: '#85CE36',
            });
          }
        }
      )
  };

  changeProductSelected(item): void{
    this.productSelected.focus=false;
    this.productSelected=item;
    item.focus=true;
    this.dataSource=item.presentations;
    if(item.materials){
      this.productMaterials=item.materials
    }
    // console.log(this.productsSelectedData);
  }



  showOptions(event:MatCheckboxChange, item: any, pos): void {
    if(event.checked){
      this.productsSelectedData=[...this.productsSelectedData,item]
      item.checked=!item.checked;
    }else{
      const index=this.productsSelectedData.indexOf(item);
      this.productsSelectedData.splice(index,1);
      item.checked=!item.checked;
    }
  }



  deleteProduct(item: any, pos): void {
    const index:any=this.productsSelectedData.indexOf(item);
    this.productsSelectedData.splice(index,1);
    item.checked=!item.checked;
    this.dataSource=[];
    this.productMaterials=[];
    this.productSelected={};
  }

  deleteUnit(unit: any): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    });

    this.productionService.deleteProductionProductPresentations(this.production.id, this.productSelected.product.id, unit.unit.id)
      .subscribe(
        (res: any)=>{
          swalWithBootstrapButtons.fire({
            title: 'Eliminada correctamente!',
            text: "La presentacion se elimino",
            icon: 'success',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 4000
          });
          const arr=[...this.dataSource];
          this.dataSource.map((element,index)=>{
            if (element.unit.id===unit.unit.id){
              arr.splice(index,1);
            }
          })
          this.dataSource=arr;
          this.productSelected.presentations=arr;
        },(err)=>console.error(err)
      )
  }

  creatProduction(element: any):void {
    this.loading=true;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })
    if(!this.production) {
      swalWithBootstrapButtons.fire({
        title: '¿Esta seguro que quiere producir?',
        text: ` ${element.quantity} ${element.product.name}(s)`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, es correcto'
      }).then((result) => {
          if (result.isConfirmed) {
            this.productionService.createProduction()
              .subscribe(
                (res: any) => {
                  // console.log(res)
                  this.production = res.production;
                  this.productionService.createProductionProducts(res.production.id, element.product.id, element.quantity)
                    .subscribe(
                      (res: any) => {
                        element.produced = true;
                        swalWithBootstrapButtons.fire({
                          title: '¡Agregado Correctamente!',
                          text: "Se agrego su producto a la produccion del dia",
                          icon: 'success',
                          // iconColor: '#FE821D',
                          showConfirmButton: false,
                          timer: 4000
                        });
                        this.loading = false;
                      }, (error) => console.error(error)
                    )

                },(err)=>{
                  console.error(err)
                }
                )
          } else {
            this.loading = false;
          }
        }
        )
    }else{
      swalWithBootstrapButtons.fire({
        title: '¿Desea realizar la produccion?',
        text: `De ${element.product.name} con una cantidad de ${element.quantity}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, es correcto'
      }).then((result) => {
        if (result.isConfirmed) {
          this.productionService.createProductionProducts(this.production.id,element.product.id,element.quantity)
            .subscribe(
              (res: any)=>{
                // console.log(res)

                element.produced=true;
                swalWithBootstrapButtons.fire({
                  title: '¡Agregado Correctamente!',
                  text: "Se agrego su producto a la produccion del dia",
                  icon: 'success',
                  // iconColor: '#FE821D',
                  showConfirmButton: false,
                  timer: 4000
                });
                this.loading=false;

              },(error)=> console.error(error)
            )
        }else{
          this.loading=false;
        }
      })

    }
  }

  updateProduction(item){
    this.loading=true;

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: '¿Desea actualizar la cantidad producida?',
      text: `De ${item.product.name} con una cantidad de ${item.quantity}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, es correcto'
    }).then((result) => {
      if (result.isConfirmed) {
        this.productionService.updateProduction(this.production.id, item.product.id, item.quantity)
          .subscribe(
            (res: any)=>{
              item.produced=true;
              swalWithBootstrapButtons.fire({
                title: '¡Modificado Correctamente!',
                text: "Se modifico la cantidad producida",
                icon: 'success',
                // iconColor: '#FE821D',
                showConfirmButton: false,
                timer: 4000
              });
              this.loading=false;
            },(error => {
              this.loading=false;
              console.error(error);
            })
          )
      }else{
        this.loading=false;
      }
    })
  }

  onEnter(value: string) {
    this.loading=true;

    if(value!==""){
      this.materialService.verifyStockMaterials(this.productSelected.product.id, parseInt(value))
        .subscribe(
          (res: any)=>{
            this.productSelected.materials=res;
            this.productMaterials=res;
            let isPermit=true;
            res.map((item)=>{
              if(!item.is_permit) isPermit=false;
            })
            if(!isPermit){
              Swal.fire({
                title: 'Oops!',
                text: "La cantidad que requieres producir no es valida porque te falta materias primas",
                icon: 'warning',
                // iconColor: '#FE821D',
                showConfirmButton: false,
                timer: 4000
              });
            }else{
              Swal.fire({
                title: 'Usted puede producir!',
                text: "Cuenta con la cantidad suficiente de materias primas e insumos",
                icon: 'success',
                // iconColor: '#FE821D',
                showConfirmButton: false,
                timer: 4000
              });
            }
            this.productSelected.canProduce=isPermit;
            this.productSelected.quantity=value;
            this.loading=false;

          },(err)=>{
            console.error(err);
            Swal.fire({
              title: 'Oops!',
              text: "Este producto no tiene asignados materias primas o insumos",
              icon: 'warning',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 4000
            });
          }
        );
    }
  }

  dateChange(event: any): void{
    this.productSelected = {};
    this.allProductsData=[];
    this.production={};
    const fecha: Date=new Date(event);
    const today = new Date();
    this.isToday = today.toDateString()==fecha.toDateString();
    this.dataSource=[];
    this.productMaterials=[];
    this.formatDate = `${fecha.getFullYear()}-${fecha.getMonth()+1}-${fecha.getDate()}`;
    this.fetchGetProductionByDay();
  }

  searchArticle(): void {
  };

  calculateColor(numberRequired: string, numberTotal: string): string {
    if (parseInt(numberRequired) > parseInt(numberTotal)) {
      return '#A8385D'
    } else {
      return '#85CE36'
    }
  }

  addUnit(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false,
    })

    if(this.productSelected.produced){
      this.loading=true;
      this.productService.getProductPresentations( this.productSelected.product.id)
        .subscribe(
          (res: any)=>{
            this.productPresentationsData=res;
            const dialogRef = this.dialog.open( AddUnitProductionComponent , {
              width: '450px',
              minHeight: '360px',
              data: {
                unitList: res,
              },
              panelClass: 'custom-dialog-container'
            })
            dialogRef.afterClosed().subscribe(
              data =>{
                if(data){
                  if(!this.productSelected.presentations.some((element)=>element.unit.id===data.unit.id)){
                  this.saveProductPresentations(data);

                  }else{
                    swalWithBootstrapButtons.fire({
                      title: 'Oops!',
                      text: "La presentacion q desea agregar ya existe",
                      icon: 'warning',
                      // iconColor: '#FE821D',
                      showConfirmButton: false,
                      timer: 4000
                    });
                  }
                }
              }
            );
            this.loading=false;
          },(err)=>{
            swalWithBootstrapButtons.fire({
              title: 'Oops!',
              text: "Este producto no tiene presentaciones asignadas\n-Dirigase a detalle producto y asignelos",
              icon: 'warning',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 4000
            });
            console.error(err)
          }
        )
    }else{
      swalWithBootstrapButtons.fire({
        title: 'Primeramente realize una produccion',
        icon: 'warning',
        // iconColor: '#FE821D',
        showConfirmButton: false,
        timer: 4000
      });
    }
  }

  editUnit(unit: any): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    });

    const dialogRef = this.dialog.open( EditUnitProductionComponent , {
      width: '450px',
      minHeight: '360px',
      data: {
        productId:this.productSelected.product.id,
        unit: unit,
        production: this.production
      },
      panelClass: 'custom-dialog-container'
    })
    dialogRef.afterClosed().subscribe(
      data =>{
        if(data) {
          unit.quantity=data.quantity;
          unit.unit_cost_production = parseInt(data.unit_cost_production);
          unit.unit_price_sale = parseInt(data.unit_price_sale);
          // console.log(unit);
        }
      })

  }

  saveProductPresentations(presentation){
    this.loading=true;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    this.productionService.createProductionProductPresentations(this.production.id,this.productSelected.product.id, presentation)
      .subscribe(
      (res: any)=>{
        // this.productSelected.isToday=true;
        swalWithBootstrapButtons.fire({
          title: 'Agregado excitosamente!',
          text: "Las presentaciones se agregaron de manera exitosa",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 4000
        });
        this.loading=false;
        this.productsSelectedData.map((item,index)=>{
          if(item.product.id===this.productSelected.product.id){
            this.productsSelectedData[index].presentations=[... this.productsSelectedData[index].presentations,presentation]
            this.dataSource=this.productsSelectedData[index].presentations;
          }
        })
      },(err: any)=> {
        console.error(err);
        this.loading=false;
      }
    )
  }

  addToCart(item: any): void {
    const material={
      id:item.id,
      name_article: item.nombre,
      unit_measure: item.unit_measure,
      cod_article: "cod",
      stock: 0,
      unit_id: 0,
      category_id: 0,
      created_at: "string",
      name: "came",
      unit_price: 0,
      kind: "asd",
      stock_min: 0,
      is_low: false,
    };
    this.cartService.addCart(material);
  }
}
