import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common'
import localeES from '@angular/common/locales/es'
import { FormControl } from '@angular/forms';
import moment, {Moment} from 'moment';
import {ProductionService} from "@core/services/production/production.service";
import {MatDatepicker} from "@angular/material/datepicker";
import Swal from "sweetalert2";

registerLocaleData(localeES, 'es');

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-kardex-production',
  templateUrl: './kardex-production.container.html',
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ],
  styleUrls: ['./kardex-production.container.css']
})
export class KardexProductionContainer implements OnInit {
  date: Date = new Date;
  dateLabel: Date = new Date();
  dateSelected: FormControl = new FormControl(
    {
      value: this.date,
      disabled: true
    }
  );
  firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
  startDate: FormControl = new FormControl(
    {
      value: this.firstDay,
      disabled: true
    }
  );
  endDate: FormControl = new FormControl(
    {
      value: this.lastDay,
      disabled: true
    }
  );

  isBoxClicked: boolean = false;
  isCodeClicked: boolean = false;

  boxForm: FormControl = new FormControl('1');
  codeForm: FormControl = new FormControl('Agr-prod-tec-12-619');

  displayedColumns: string[] = [
    'nro',
    'date',
    // 'milk',
    'code',
    'production',
    'unit',
    'cost',
    'quantity',
    'amount',
    'second_quantity',
    'second_amount',
    'third_quantity',
    'third_amount',
  ];
  dataSource!: any;
  constructor(
    private productionService: ProductionService,

  ) {
  }

  ngOnInit(): void {
    const firstDate = `${this.startDate.value.getFullYear()}-${this.startDate.value.getMonth()+1}-${this.startDate.value.getDate()}`;
    const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
    this.fetchProductionDetails(firstDate,endDate);
  }

  fetchProductionDetails(firstDate, endDate): void {
    this.productionService.getProductionDetails(firstDate, endDate)
      .subscribe(
        (res: any) => {
          if(res.presentation.length>0){
            this.dataSource=new Array(res[0]);
          }else{
            this.dataSource=res;
          }
        }, (err) => {
          console.error(err)
        }
      )
  }

  dateChanged(typeOfDate,date: any): void{
    if(typeOfDate===1){
      this.startDate.setValue(date.value);
      if(Date.parse(date.value)<Date.parse(this.endDate.value)){
        const firstDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        this.fetchProductionDetails(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha menor a la FECHA FIN',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }

    }
    if(typeOfDate===2){
      this.endDate.setValue(date.value);
      if(Date.parse(this.startDate.value)<Date.parse(date.value)){
        const firstDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        const endDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        this.fetchProductionDetails(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha mayor a la FECHA INICIO',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }
    }
  }
  isClicked(option: string): void {
    switch (option) {
      case 'box':
        if (this.isBoxClicked) {
          this.isBoxClicked = false;
        } else {
          this.isBoxClicked = true;
        }
        break;
      case 'code':
        if (this.isCodeClicked) {
          this.isCodeClicked = false;
        } else {
          this.isCodeClicked = true;
        }
        break;
      default:
        console.log('Dont nothing');
        break;
    }

  }

  calculateBackground(i: number): string {
    if (i % 2 === 0) {
      return '#D7DDE4'
    } else {
      return '#ffffff'
    }
  }

}
