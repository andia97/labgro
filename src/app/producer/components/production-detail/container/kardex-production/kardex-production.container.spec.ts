import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KardexProductionContainer } from './kardex-production.container';

describe('KardexProductionComponent', () => {
  let component: KardexProductionContainer;
  let fixture: ComponentFixture<KardexProductionContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KardexProductionContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KardexProductionContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
