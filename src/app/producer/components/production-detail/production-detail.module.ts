import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';

import { ProductionDetailRoutingModule } from './production-detail-routing.module';
import { ProductionDetailComponent } from './production-detail.component';

import { MaterialModule } from '@material/material.module';
import { CalendarProductionContainer } from './container/calendar-production/calendar-production.container';
import { KardexProductionContainer } from './container/kardex-production/kardex-production.container';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddUnitProductionComponent } from './components/add-unit-production/add-unit-production.component';
import { EditUnitProductionComponent } from './components/edit-unit-production/edit-unit-production.component';


@NgModule({
  declarations: [
    ProductionDetailComponent,
    CalendarProductionContainer,
    KardexProductionContainer,
    AddUnitProductionComponent,
    EditUnitProductionComponent
  ],
  imports: [
    CommonModule,
    ProductionDetailRoutingModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProductionDetailModule { }
