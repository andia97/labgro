import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ProductService} from "@core/services/product/product.service";
import {ProductionService} from "@core/services/production/production.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-edit-unit-production',
  templateUrl: './edit-unit-production.component.html',
  styleUrls: ['./edit-unit-production.component.css']
})
export class EditUnitProductionComponent implements OnInit {
  state: boolean = false;
  dataUnitForm!: FormGroup;
  unitList: any = {};
  numRegex = /^-?\d*[.,]?\d{0,2}$/;
  productId!: number;
  production!: any;

  public errorsMessages = {
    unit: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
    unit_cost_production: [
      { type: 'required', message: 'El costo de produccion es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
    unit_price_sale: [
      { type: 'required', message: 'El costo de venta es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ]
  }

  get unitData():any {
    return this.dataUnitForm.get('unit');
  }

  get quantity():any {
    return this.dataUnitForm.get('quantity');
  }

  get unitCostProduction():any {
    return this.dataUnitForm.get('unit_cost_production');
  }

  get unitPriceSale():any {
    return this.dataUnitForm.get('unit_price_sale');
  }

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<EditUnitProductionComponent>,
    private productService: ProductService,
    private productionService: ProductionService
  ) {
    this.buildForm();
    this.unitList = data.unit;
    this.productId = data.productId;
    this.production = data.production;
  }

  ngOnInit(): void {
    this.dataUnitForm.controls["quantity"].setValue(this.unitList.quantity);
    this.dataUnitForm.controls["unit_cost_production"].setValue(this.unitList.unit_cost_production);
    this.dataUnitForm.controls["unit_price_sale"].setValue(this.unitList.unit_price_sale);
    this.dataUnitForm.controls["unit"].setValue(this.unitList.unit.name);
  }

  buildForm(): void {
    this.dataUnitForm = this.formBuilder.group({
      unit: ['',[
        Validators.required,
      ]],
      quantity: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_cost_production: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_price_sale: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ]
    });
  }

  saveData() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    });

    this.productService.updateProductPresentation(this.productId,this.unitList.unit.id,this.dataUnitForm.value.unit_cost_production,this.dataUnitForm.value.unit_price_sale)
      .subscribe(
        (res: any)=>{
          this.productionService.updateProductionProductPresentations(this.production.id,this.productId, this.unitList.unit.id,this.dataUnitForm.value)
            .subscribe(
              (res: any)=>{
                swalWithBootstrapButtons.fire({
                  title: 'Modificado correctamente!',
                  text: "La presentacion se modifico",
                  icon: 'success',
                  // iconColor: '#FE821D',
                  showConfirmButton: false,
                  timer: 4000
                });
                this.save();
              },(err)=> {
                 console.error(err)
              }
            )
        },(err: any)=>{
          console.error(err);
        }
      )
  }

  save() {
    this.dialogRef.close(this.dataUnitForm.value);
  }
}
