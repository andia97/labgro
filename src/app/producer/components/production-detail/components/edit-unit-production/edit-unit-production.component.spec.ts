import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUnitProductionComponent } from './edit-unit-production.component';

describe('EditUnitProductionComponent', () => {
  let component: EditUnitProductionComponent;
  let fixture: ComponentFixture<EditUnitProductionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUnitProductionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUnitProductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
