import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  templateUrl: './add-unit-production.component.html',
  styleUrls: ['./add-unit-production.component.css']
})
export class AddUnitProductionComponent implements OnInit {
  state: boolean = false;
  dataUnitForm!: FormGroup;
  unitList: any = {};
  numRegex = /^-?\d*[.,]?\d{0,2}$/;

  public errorsMessages = {
    unit: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
    unit_cost_production: [
      { type: 'required', message: 'El costo de produccion es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
    unit_price_sale: [
      { type: 'required', message: 'El costo de venta es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ]
  }

  get unitData():any {
    return this.dataUnitForm.get('unit');
  }

  get quantity():any {
    return this.dataUnitForm.get('quantity');
  }

  get unitCostProduction():any {
    return this.dataUnitForm.get('unit_cost_production');
  }

  get unitPriceSale():any {
    return this.dataUnitForm.get('unit_price_sale');
  }
  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddUnitProductionComponent>,
  ) {
    this.buildForm();
    this.unitList=data.unitList;
  }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.dataUnitForm = this.formBuilder.group({
      unit: [ ,
        [Validators.required],
      ],
      quantity: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_cost_production: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_price_sale: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ]
    });
  }

  getPresentation(presentation: any){
    this.dataUnitForm.controls["unit_cost_production"].setValue(presentation.unit_cost_production);
    this.dataUnitForm.controls["unit_price_sale"].setValue(presentation.unit_price_sale);
  }

  saveData() {
    this.save();
  }

  save() {
    this.dialogRef.close(this.dataUnitForm.value);
  }

}
