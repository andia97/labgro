import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUnitProductionComponent } from './add-unit-production.component';

describe('AddUnitProductionComponent', () => {
  let component: AddUnitProductionComponent;
  let fixture: ComponentFixture<AddUnitProductionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUnitProductionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUnitProductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
