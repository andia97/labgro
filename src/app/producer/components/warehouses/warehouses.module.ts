import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarehousesRoutingModule } from './warehouses-routing.module';
import { ArticleLayoutComponent } from './components/article-layout/article-layout.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
import { ArticleContainer } from './container/article/article.container';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SuppliesContainer } from './container/supplies/supplies.container';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddArticleComponent } from './components/article-layout/add-article/add-article.component';
import { PresentationContainer } from './container/presentation/presentation.container';
import { AddPresentationComponent } from './components/article-layout/presentation/add-presentation/add-presentation.component';
import { EditPresentationComponent } from './components/article-layout/presentation/edit-presentation/edit-presentation.component';
import { EditArticleWarehouseComponent } from './container/article/edit-article-warehouse/edit-article-warehouse.component';
import { EditArticleCartComponent } from './components/shopping-cart/edit-article-cart/edit-article-cart.component';
import { ArticleIncomeContainer } from './container/article-income/article-income.container';
import { ArticleOutputContainer } from './container/article-output/article-output.container';
import { ArticleMonthDataComponent } from './components/article-detail/article-month-data/article-month-data.component';


@NgModule({
  declarations: [
    ArticleContainer,
    SuppliesContainer,
    PresentationContainer,
    ArticleLayoutComponent,
    ArticleDetailComponent,
    ShoppingCartComponent,
    AddArticleComponent,
    AddPresentationComponent,
    EditPresentationComponent,
    EditArticleWarehouseComponent,
    EditArticleCartComponent,
    ArticleIncomeContainer,
    ArticleOutputContainer,
    ArticleMonthDataComponent
  ],
  imports: [
    CommonModule,
    WarehousesRoutingModule,
    MaterialModule,
    SharedModule,
    NgxChartsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class WarehousesModule { }
