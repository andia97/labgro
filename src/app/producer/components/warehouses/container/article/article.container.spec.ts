import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleContainer } from './article.container';

describe('ArticleContainer', () => {
  let component: ArticleContainer;
  let fixture: ComponentFixture<ArticleContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
