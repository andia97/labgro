import { MatSnackBar } from '@angular/material/snack-bar';
import { ShoppingCartService } from '@core/services/shopping-cart/shopping-cart.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MaterialProducer } from '@core/models/material-producer';
import { ArticleService } from '@core/services/article/article.service';
import { MaterialService } from '@core/services/material/material.service';
import Swal from 'sweetalert2';
import { EditArticleWarehouseComponent } from './edit-article-warehouse/edit-article-warehouse.component';

@Component({
  selector: 'app-article',
  templateUrl: './article.container.html',
  styleUrls: ['./article.container.css']
})
export class ArticleContainer implements OnInit {
  @Input() rawMaterial!: MaterialProducer | any;
  @Output() emitMaterialEventUpdate: EventEmitter<boolean> = new EventEmitter();
  color = '#8CFC9D';
  constructor(
    private cartService: ShoppingCartService,
    private snackBar: MatSnackBar,
    private materialService: MaterialService,
    private dialog: MatDialog,
    private articleService: ArticleService,
  ) {
  }

  ngOnInit(): void {

  }

  editArticle(): void {
    this.articleService.getArticles()
    .subscribe(
      (res: any)=>{
        const dialogRef = this.dialog.open( EditArticleWarehouseComponent, {
          width: '480px',
          height: '600px',
          panelClass: 'custom-dialog-container',
          data: {
            articles: res,
            material: this.rawMaterial
          }
        });

        dialogRef.afterClosed().subscribe(
          (result: any ) =>{
            this.emitMaterialEventUpdate.emit(res);
          }
        )
      },
      (err: any)=>{
        console.error("something went wrong: "+ err)
      }
    )

  }

  deleteArticle(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el material: ' + this.rawMaterial.name_article + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.materialService.deleteMaterial(this.rawMaterial.id)
        .subscribe(
          (res: any)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu producto se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
            this.emitMaterialEventUpdate.emit(res);
        },(err)=>{
          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });
        })
      }
    })
  }

  addToCart(rawMaterial: MaterialProducer | any): void {
    this.cartService.addCart(rawMaterial);
  }
}
