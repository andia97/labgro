import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditArticleWarehouseComponent } from './edit-article-warehouse.component';

describe('EditArticleWarehouseComponent', () => {
  let component: EditArticleWarehouseComponent;
  let fixture: ComponentFixture<EditArticleWarehouseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditArticleWarehouseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditArticleWarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
