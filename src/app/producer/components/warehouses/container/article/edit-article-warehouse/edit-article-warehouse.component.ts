import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { MaterialProducer } from '@core/models/material-producer';
import { MaterialService } from '@core/services/material/material.service';
import { MessageService } from '@core/services/menssage/message.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-article-warehouse',
  templateUrl: './edit-article-warehouse.component.html',
  styleUrls: ['./edit-article-warehouse.component.css']
})
export class EditArticleWarehouseComponent implements OnInit {

  options!: Article[];

  filteredOptions!: Observable<Article[]>;
  articleSelected!: any;

  selectedColor='#85CE36';

  dataMaterialForm!: FormGroup;
  
  canRender: boolean = false;
  state: boolean=false;

  materialId!: number;

  get codeArticle():any {
    return this.dataMaterialForm.get('code');
  }

  get quantityInitialStock():any {
    return this.dataMaterialForm.get('stock_start');
  }

  get quantityMinimalStock():any {
    return this.dataMaterialForm.get('stock_min');
  }
  
  public errorsMessages = {
    article: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    code: [
      { type: 'required', message: 'El codigo es requerido' },
    ],
    stock_start: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
    stock_min: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
  };

  constructor(
    private materialService: MaterialService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditArticleWarehouseComponent>,
    private messageService: MessageService
  ) {
    this.buildForm();
    this.options = data.articles;
    this.materialId = data.material.id;
    this.changeColor(data.material.color);
   }

  ngOnInit(): void {
    const dataForm = this.data.material;
    this.dataMaterialForm.patchValue(dataForm);
    this.getArticles();
  }

  getArticle(article: Article): void {
    this.dataMaterialForm.controls.article_id.setValue(article.id); 
    this.articleSelected = article;
    
  }

  getArticles(): void{
    this.dataMaterialForm.controls.article.setValue(this.data.material); 
    this.articleSelected = this.data.material;
    this.filteredOptions = this.dataMaterialForm.controls.article.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? "" : value.name_article),
      map(name_article => name_article ?  this._filter(name_article): this.options.slice())
    );
    this.canRender=true;
  }


  buildForm(): void {
    this.dataMaterialForm = this.formBuilder.group({
      article: [ , 
        [Validators.required]
      ],
      article_id: [""], 
      code: ["",Validators.required],
      stock_start: ['', 
        [
          Validators.required,
          Validators.min(1),
        ]
      ],
      stock_min: ['', 
        [
          Validators.required,
          Validators.min(1),
        ]
      ],
    });
  };

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataMaterialForm.controls.article_id.reset();
        }
        return value;
      }
    );
  };


  displayFn(article: any): string {
    return article && article.name_article ? article.name_article : '';
  };
  
  changeColor(event: any): void {
    this.selectedColor = event;
  };

  saveData(): void {
    this.state=true;
    const material={
      code: this.dataMaterialForm.value.code,
      stock_start: this.dataMaterialForm.value.stock_start,
      stock_min: this.dataMaterialForm.value.stock_min,
      color: this.selectedColor,
      article_id: this.dataMaterialForm.value.article_id
    };
    this.materialService.updateMaterial(material,this.materialId)
    .subscribe((res: any)=>{
    this.state=false;
      Swal.fire({
        title: 'Guardados correctamente!',
        text: 'Los datos de la materia prima/insumo se guardaron correctamente',
        icon: 'success',
        timer: 2000,
        showConfirmButton: false,
      });
      this.dialogRef.close();
    },(err: any)=>{
      const errors = this.messageService.get();
      this.state=false;
      if(errors.message){
        Swal.fire({
          title: '¡Oops!',
          text: errors.errors.code[0],
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }else{
        Swal.fire({
          title: '¡Error!',
          text: "Ocurrio un problema inseperado, contactese con administración",
          icon: 'error',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }
    })
  };
}
