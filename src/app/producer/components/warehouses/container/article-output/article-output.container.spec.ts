import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleOutputContainer } from './article-output.container';

describe('ArticleOutputComponent', () => {
  let component: ArticleOutputContainer;
  let fixture: ComponentFixture<ArticleOutputContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleOutputContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleOutputContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
