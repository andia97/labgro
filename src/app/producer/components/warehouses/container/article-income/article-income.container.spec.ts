import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleIncomeContainer } from './article-income.container';

describe('ArticleIncomeComponent', () => {
  let component: ArticleIncomeContainer;
  let fixture: ComponentFixture<ArticleIncomeContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleIncomeContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleIncomeContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
