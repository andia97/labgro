import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MaterialProducer } from '@core/models/material-producer';
import { ArticleService } from '@core/services/article/article.service';
import { ShoppingCartService } from '@core/services/shopping-cart/shopping-cart.service';
import { MaterialService } from '@core/services/material/material.service';
import Swal from 'sweetalert2';
import { EditArticleWarehouseComponent } from '../article/edit-article-warehouse/edit-article-warehouse.component';

@Component({
  selector: 'app-supplies',
  templateUrl: './supplies.container.html',
  styleUrls: ['./supplies.container.css']
})
export class SuppliesContainer implements OnInit {
  @Input() dataSource: MaterialProducer[] | any;
  @Output() emitMaterialEventUpdate: EventEmitter<boolean> = new EventEmitter();

  displayedColumns: string[] = ['article', 'unit', 'quantity' ,'action'];
   
  constructor(
    private materialService: MaterialService,
    private dialog: MatDialog,
    private articleService: ArticleService,
    private cartService: ShoppingCartService,
  ) { }

  ngOnInit(): void {
  };

  editArticle(material: MaterialProducer): void {
    this.articleService.getArticles()
    .subscribe(
      (res: any)=>{
        const dialogRef = this.dialog.open( EditArticleWarehouseComponent, {
          width: '480px',
          height: '600px',
          panelClass: 'custom-dialog-container',
          data: {
            articles: res,
            material: material
          }
        });
        
        dialogRef.afterClosed().subscribe(
          (result: any ) =>{
            this.emitMaterialEventUpdate.emit(res);
          }
        )
      },
      (err: any)=>{
        console.error("something went wrong: "+ err)
      }
    )
    
  };

  deleteArticle(element: MaterialProducer): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el material: ' + element.name_article + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.materialService.deleteMaterial(element.id)
        .subscribe(
          (res: any)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu producto se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
            this.emitMaterialEventUpdate.emit(res);
        },(err)=>{
          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });
        })
      }
    })
  };
 
  addToCart(rawMaterial: MaterialProducer | any): void {
    this.cartService.addCart(rawMaterial);
  }
}
