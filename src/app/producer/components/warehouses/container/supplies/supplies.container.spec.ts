import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppliesContainer } from './supplies.container';

describe('SuppliesContainer', () => {
  let component: SuppliesContainer;
  let fixture: ComponentFixture<SuppliesContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuppliesContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppliesContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
