import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Presentation } from '@core/models/presentation.model';
import { PresentationService } from '@core/services/presentation/presentation.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { AddPresentationComponent } from '../../components/article-layout/presentation/add-presentation/add-presentation.component';
import { EditPresentationComponent } from '../../components/article-layout/presentation/edit-presentation/edit-presentation.component';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.container.html',
  styleUrls: ['./presentation.container.css']
})
export class PresentationContainer implements OnInit {
  @Input() presentationList!: Observable<Presentation[]>;
  @Output() eventEmmitPresentation: EventEmitter<any> = new EventEmitter();
  @Output() emitEventUpdatePresentationList: EventEmitter<any> = new EventEmitter();

  displayedColumns: string[] = ['name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;

  constructor(
    private dialog: MatDialog,
    private presentationService: PresentationService,
    
  ) { 
  };
  
  ngOnInit(): void {
  };

  closeSidenav(): void {
    this.eventEmmitPresentation.emit(true);
  };

  addPresentation(): void {
    const dialogRef = this.dialog.open(AddPresentationComponent, {
      width: '480px',
      maxHeight: '500px',
      panelClass: 'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(
      (result: any ) =>{
        this.emitEventUpdatePresentationList.emit();
      }
    )
  };
  
  editPresentation(presentation: Presentation): void {
    const dialogRef = this.dialog.open(EditPresentationComponent, {
      width: '480px',
      maxHeight: '500px',
      panelClass: 'custom-dialog-container',
      data: {
        presentation
      }
    });

    dialogRef.afterClosed().subscribe(
      (result: any ) =>{
        this.emitEventUpdatePresentationList.emit();
      }
    )
  };

  deletePresentation(element): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la presentacion: ' + element.name + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.presentationService.deletePresentation(element.id)
        .subscribe(
          (res: any)=>{
            if(res.success){
              swalWithBootstrapButtons.fire({
                title: '¡Eliminado!',
                text: "Tu producto se eliminó correctamente",
                icon: 'success',
                // iconColor: '#FE821D',
                showConfirmButton: false,
                timer: 2000
              });
              this.emitEventUpdatePresentationList.emit();
            }else{
              let products="";
              res.products.map((item)=>{
                products=products+" - "+item.name;
              })
              swalWithBootstrapButtons.fire({
                title: '¡Oops!',
                text: res.message + ": " + products,
                icon: 'error',
                // iconColor: '#FE821D',
                showConfirmButton: false,
              });
            }
          },(err)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Oops!',
              text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
              icon: 'error',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 1800
            });
        })
      }
    })
  };
}
