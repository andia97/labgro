export var multi = [
  {
    "name": "Leche",
    "series": [
      {
        "name": "Ene",
        "value": 0
      },
      {
        "name": "Feb",
        "value": 0
      },
      {
        "name": "Mar",
        "value": 0
      },
      {
        "name": "Abr",
        "value": 154
      },
      {
        "name": "May",
        "value": 984
      },
      {
        "name": "Jun",
        "value": 354
      },
      {
        "name": "Jul",
        "value": 204
      },
      {
        "name": "Ago",
        "value": 594
      },
      {
        "name": "Sep",
        "value": 864
      },
      {
        "name": "Oct",
        "value": 8920
      },
      {
        "name": "Nov",
        "value": 3620
      },
      {
        "name": "Dic",
        "value": 2620
      },
    ]
  },

  {
    "name": "Azucar",
    "series": [
      {
        "name": "Ene",
        "value": 600
      },
      {
        "name": "Feb",
        "value": 730
      },
      {
        "name": "Mar",
        "value": 8
      },
      {
        "name": "Abr",
        "value": 80
      },
      {
        "name": "May",
        "value": 265
      },
      {
        "name": "Jun",
        "value": 96
      },
      {
        "name": "Jul",
        "value": 301
      },
      {
        "name": "Ago",
        "value": 520
      },
      {
        "name": "Sep",
        "value": 962
      },
      {
        "name": "Oct",
        "value": 123
      },
      {
        "name": "Nov",
        "value": 205
      },
      {
        "name": "Dic",
        "value": 963
      },
    ]
  },

  {
    "name": "Naranja",
    "series": [
      {
        "name": "Ene",
        "value": 96
      },
      {
        "name": "Feb",
        "value": 1
      },
      {
        "name": "Mar",
        "value": 20
      },
      {
        "name": "Abr",
        "value": 85
      },
      {
        "name": "May",
        "value": 63
      },
      {
        "name": "Jun",
        "value": 305
      },
      {
        "name": "Jul",
        "value": 87
      },
      {
        "name": "Ago",
        "value": 85
      },
      {
        "name": "Sep",
        "value": 52
      },
      {
        "name": "Oct",
        "value": 20
      },
      {
        "name": "Nov",
        "value": 0
      },
      {
        "name": "Dic",
        "value": 0
      },
    ]
  },
  {
    "name": "Manzana",
    "series": [
      {
        "name": "Ene",
        "value": 82
      },
      {
        "name": "Feb",
        "value": 730
      },
      {
        "name": "Mar",
        "value": 302
      },
      {
        "name": "Abr",
        "value": 963
      },
      {
        "name": "May",
        "value": 71
      },
      {
        "name": "Jun",
        "value": 23
      },
      {
        "name": "Jul",
        "value": 78
      },
      {
        "name": "Ago",
        "value": 852
      },
      {
        "name": "Sep",
        "value": 20
      },
      {
        "name": "Oct",
        "value": 63
      },
      {
        "name": "Nov",
        "value": 96
      },
      {
        "name": "Dic",
        "value": 320
      },
    ]
  }
];

export var single = [
  {
    "name": "Materias Primas",
    "value": 89
  },
  {
    "name": "Insumos",
    "value": 50
  },
];
