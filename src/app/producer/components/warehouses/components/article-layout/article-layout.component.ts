import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MaterialProducer } from '@core/models/material-producer';
import { AddArticleComponent } from '@producer/components/warehouses/components/article-layout/add-article/add-article.component';
import { multi, single } from '../../data-chart-example';
import { ArticleService } from '@core/services/article/article.service';
import { filter, map, toArray } from 'rxjs/operators';
import { zIndex } from 'html2canvas/dist/types/css/property-descriptors/z-index';
import { Article } from '@core/models/article.model';
import { MaterialService } from '@core/services/material/material.service';
import { ShoppingCartService } from '@core/services/shopping-cart/shopping-cart.service';
import { Observable } from 'rxjs';
import { Presentation } from '@core/models/presentation.model';
import { PresentationService } from '@core/services/presentation/presentation.service';
import {MatTableDataSource} from "@angular/material/table";
import {Area} from "@core/models/area.model";
import * as events from "events";
@Component({
  selector: 'app-article-layout',
  templateUrl: './article-layout.component.html',
  styleUrls: ['./article-layout.component.css']
})
export class ArticleLayoutComponent implements OnInit {
  multi!: any[];
  view: any = [700, 300];
  viewPie: any = [470, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = false;
  xAxisLabel: string = 'Year';
  yAxisLabel: string = 'Cantidad';
  legendTitle: string = 'Materia Prima';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  single!: any;
  gradient: boolean = true;
  showLegend: boolean = true;
  isDoughnut: boolean = false;
  legendTitlePie: string = 'Artículos';
  legendPosition: any = 'right';

  searchControl: FormGroup = this.formBuilder.group(
    {
      search: ['',
      [Validators.minLength(1), Validators.maxLength(50)]]
    }
  );
  total$: Observable<number>;
  get search(): any {
    return this.searchControl.get('search');
  }
  // Messages for validators
  public errorsMessages = {
    search: [
      { type: 'maxength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'minlength', message: 'Debe introducir como minimos 1 caractere'},
    ],
  }
  materialList: MaterialProducer[] | any;
  supplieList: MaterialProducer[]=[];
  DataSource= new MatTableDataSource<MaterialProducer>(this.supplieList);
  presentationList!: Observable<Presentation[]> ;
  articles!: Article[];
  canRender:boolean = false;
  state: boolean =false;
  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private articleService: ArticleService,
    private materialService: MaterialService,
    private shoppingCartService: ShoppingCartService,
    private presentationService: PresentationService
  ) {
    Object.assign(this, { single });
    this.total$ = this.shoppingCartService.cart$
    .pipe(
      map(
        (
          (articles: Article[] | any[]) => articles.length
        )
      )
    );
  }

  ngOnInit(): void {
    this.fetchMaterials();
    this.fetchPresentations();
    this.fetchArticleHistoric();
  }

  fetchArticleHistoric(): void{
    this.articleService.getIncomesArticlesHistoric(2021)
      .subscribe(
        (res: any)=>{
          this.multi=res;
        },(err)=>{
          console.error(err);
        }
      )
  }

  fetchMaterials(): void{
    this.state=true;
    this.materialService.getMaterials()
    .subscribe(
      (res: any)=>{
        this.state=false;
        this.materialList = res.raw_material;
        this.supplieList = res.supplies;
        this.DataSource= new MatTableDataSource<MaterialProducer>(this.supplieList);
        this.canRender = true;
      },
      (err: any)=>{
        console.error("something went wrong feching articles");
      }
    )
  };

  fetchPresentations(): void{
    this.presentationService.getPresentations()
    .subscribe(
      (res: any)=>{
        this.presentationList=res.presentations;
      },
      (err: any)=>{
        console.error(err)
      }
    )
  }

  searchArticle(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.DataSource.filter = filterValue.trim().toLowerCase();
  };

  addArticle(): void {
    this.articleService.getArticles()
    .pipe(map(data => data.filter(article => article.name == "Materia Prima" || article.name =="Insumos") ))
    .subscribe((res: any)=>{
      const dialogRef = this.dialog.open( AddArticleComponent, {
        width: '480px',
        height: '600px',
        panelClass: 'custom-dialog-container',
        data: {
          articles: res,
        }
      });

      dialogRef.afterClosed().subscribe(
        (result: any ) =>{
          this.fetchMaterials();

        }
      )
    },(err: any)=>{
      console.error(err);
    })
  };
}
