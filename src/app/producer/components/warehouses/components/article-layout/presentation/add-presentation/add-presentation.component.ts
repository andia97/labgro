import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from '@core/services/menssage/message.service';
import { PresentationService } from '@core/services/presentation/presentation.service';
import Swal from 'sweetalert2';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  templateUrl: './add-presentation.component.html',
  styleUrls: ['./add-presentation.component.css']
})
export class AddPresentationComponent implements OnInit {

  dataPresentationForm!: FormGroup;

  state: boolean = false;
  numRegex = /^-?\d*[.,]?\d{0,2}$/;


  get namePresentation():any {
    return this.dataPresentationForm.get('name');
  }

  get quantityPresentation():any {
    return this.dataPresentationForm.get('quantity');
  }

  get stock_minPresentation():any {
    return this.dataPresentationForm.get('stock_min');
  }

  public errorsMessages = {
    name: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
    stock_min: [
      { type: 'required', message: 'El stock es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
  };

  constructor(
    private presentationService: PresentationService,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private dialogRef: MatDialogRef<AddPresentationComponent>,

) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.dataPresentationForm = this.formBuilder.group({
      name: [ "",
        [Validators.required]
      ],
      quantity: ['',
        [
          Validators.required,
          Validators.min(1),
          Validators.pattern(this.numRegex)

        ]
      ],
      stock_min: ['',
        [
          Validators.required,
          Validators.min(1),
          Validators.pattern(this.numRegex)

        ]
      ],
    });
  };

  saveData(): void {
    this.state = true;
    const presentation={
      name: this.dataPresentationForm.value.name,
      quantity: this.dataPresentationForm.value.quantity,
      stock_min: this.dataPresentationForm.value.stock_min,
    };
    this.presentationService.createPresentation(presentation)
    .subscribe(
      (res: any)=>{
        this.state=false;
        Swal.fire({
          title: '¡Agregado correctamente!',
          text: 'Los datos de la presentacion se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        }).then(_=>{
        this.dialogRef.close();
        })

      },
      (err: any)=>{
        console.log(this.messageService.get());
        console.error(err);
        this.state = false;
        Swal.fire({
          title: '¡Error!',
          text: this.messageService.get(),
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }
    );
  }

}
