import { Component, Inject, inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { Presentation } from '@core/models/presentation.model';
import { MessageService } from '@core/services/menssage/message.service';
import { PresentationService } from '@core/services/presentation/presentation.service';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './edit-presentation.component.html',
  styleUrls: ['./edit-presentation.component.css']
})
export class EditPresentationComponent implements OnInit {

  dataPresentationForm!: FormGroup;

  state=false;

  numRegex = /^-?\d*[.,]?\d{0,2}$/;

  presentation!: Presentation;

  get namePresentation():any {
    return this.dataPresentationForm.get('name');
  }

  get quantityPresentation():any {
    return this.dataPresentationForm.get('quantity');
  }

  get stock_minPresentation():any {
    return this.dataPresentationForm.get('stock_min');
  }

  public errorsMessages = {
    name: [
      { type: 'required', message: 'La presentacion es requerida' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
    stock_min: [
      { type: 'required', message: 'El stock es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
  };

  constructor(
    private presentationService: PresentationService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private dialogRef: MatDialogRef<EditPresentationComponent>,

  ) {
    this.buildForm();
    this.presentation = data.presentation;
  };

  ngOnInit(): void {
    const dataForm = {
      name: this.data.presentation.name,
      quantity: this.data.presentation.quantity,
      stock_min: this.data.presentation.stock_min
    };
    this.dataPresentationForm.patchValue(dataForm);
  };

  buildForm(): void {
    this.dataPresentationForm = this.formBuilder.group({
      name: [ "",
        [Validators.required]
      ],
      quantity: ['',
        [
          Validators.required,
          Validators.min(1),
          Validators.pattern(this.numRegex)

        ]
      ],
      stock_min: ['',
        [
          Validators.required,
          Validators.min(1),
          Validators.pattern(this.numRegex)

        ]
      ],
    });
  };

  saveData(): void {
    this.state=true;
    const newPresentation={
      name: this.dataPresentationForm.value.name,
      quantity: this.dataPresentationForm.value.quantity,
      stock_min: this.dataPresentationForm.value.stock_min,
    };
    this.presentationService.editPresentation(this.presentation.id, newPresentation)
    .subscribe(
      (res: any)=>{
        this.state=false;
        Swal.fire({
          title: 'Modificando correctamente!',
          text: 'Los datos de la presentacion se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        }).then(_=>{
          this.dialogRef.close();
        });
      },
      (err: any)=>{

        console.error(err);
        this.state=false;
        Swal.fire({
          title: '¡Error!',
          text: this.messageService.get(),
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }
    );
  }
}
