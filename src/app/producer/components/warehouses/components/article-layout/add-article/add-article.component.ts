import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { MaterialService } from '@core/services/material/material.service';
import { MessageService } from '@core/services/menssage/message.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  options!: Article[];
  filteredOptions!: Observable<Article[]>;
  articleSelected!: Article;

  selectedColor='#85CE36';

  onlyNumber = '^[0-9]+$';
  numRegex = /^-?\d*[.,]?\d{0,2}$/;

  dataArticleForm!: FormGroup;

  canRender: boolean = false;
  state: boolean=false;

  get articleData(): any{
    return this.dataArticleForm.get('article');
  }

  get codeArticle():any {
    return this.dataArticleForm.get('code_article');
  }

  get quantityInitialStock():any {
    return this.dataArticleForm.get('quantity_initial_stock');
  }

  get quantityMinimalStock():any {
    return this.dataArticleForm.get('quantity_minimal_stock');
  }

  public errorsMessages = {
    article: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    code_article: [
      { type: 'required', message: 'El codigo es requerido' },
    ],
    quantity_initial_stock: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
    quantity_minimal_stock: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddArticleComponent>,
    private materialService: MaterialService,
    private messageService: MessageService
  ) {
    this.options = data.articles;
    this.buildForm();
  };

  ngOnInit(): void {
    this.getArticles();
  };

  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ ,
        [Validators.required]
      ],
      article_id: [""],
      code_article: ["",Validators.required],
      quantity_initial_stock: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)

        ]
      ],
      quantity_minimal_stock: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)

        ]
      ],
    });
  };

  saveData(): void {
    this.state=true;
    const material={
      code: this.dataArticleForm.value.code_article,
      stock_start: this.dataArticleForm.value.quantity_initial_stock,
      stock_min: this.dataArticleForm.value.quantity_minimal_stock,
      color: this.selectedColor,
      article_id: this.articleSelected.id,
    };
    this.materialService.postMaterial(material)
    .subscribe((res: any)=>{
      this.state=false;
      Swal.fire({
        title: '¡Agregado correctamente!',
        text: 'Los datos de la materia prima / insumo se guardaron correctamente',
        icon: 'success',
        timer: 2000,
        showConfirmButton: false,
      });
      this.dialogRef.close();
    },(err: any)=>{
      const errors = this.messageService.get();
      console.log(errors);
      const validators = errors.errors;
      this.state=false;
      if (validators.article_id) {
        Swal.fire({
          title: '¡Oops!',
          text: 'La materia prima/insumo ya se encuentra registrado',
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }
      if(errors.message){
        Swal.fire({
          title: '¡Oops!',
          text: errors.errors.code[0],
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }else{
        Swal.fire({
          title: '¡Error!',
          text: "Ocurrio un problema inseperado, contactese con administración",
          icon: 'error',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        })
      }
    })
  };

  getArticles(): void{
    // this.state = true;
    // this.presentationsService.getPresentations().subscribe(
    //   (res: any) => {
    //     this.state = false;
    //     this.canRender=true;
    //     this.options = res.presentations;
        this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? "" : value.name_article),
          map(name_article => name_article ?  this._filter(name_article): this.options.slice())
        );
    //   },
    //   (error: any) => {
    //     alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
    //   },
    // )
    this.canRender=true;
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }

  public getArticle(article: any): void{
    this.dataArticleForm.controls.article_id.setValue(article.id);
    this.articleSelected = article;
  }

  displayFn(article: any): string {
    return article && article.name_article ? article.name_article : '';
  }

  changeColor(event: any): void {
    this.selectedColor = event;
  }
}
