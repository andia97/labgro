import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '@core/services/shopping-cart/shopping-cart.service';
import { Article } from '@core/models/article.model';
import { Observable, throwError } from 'rxjs';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AreaService } from '@core/services/area/area.service';
import { Area } from '@core/models/area.model';
import { ArticleService } from '@core/services/article/article.service';
import { catchError, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { EditArticleCartComponent } from './edit-article-cart/edit-article-cart.component';
import Swal from 'sweetalert2';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { StorageService } from '@core/services/storage/storage.service';
import { AuthConstants } from '@core/config/auth-constanst';
import { MessageService } from '@core/services/menssage/message.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource!: Article[] | any[];

  articles$!: Observable<Article[] | any[]>;

  orderForm!: FormGroup;

  isReceipt: boolean = false;
  isOrder: boolean = false;
  isDate: boolean = false;
  areas!: Area[];
  articles!: Article[];
  state: boolean = false;
  stateDownload: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private shoppingCartService: ShoppingCartService,
    private areaService: AreaService,
    private articlesService: ArticleService,
    private dialog: MatDialog,
    private storageService: StorageService,
    private messageService: MessageService,
  ) {
    this.articles$ = this.shoppingCartService.cart$;
    this.buildForm();
  }

  ngOnInit(): void {
    this.getArticles();
    this.getAreas();
    this.buildDataSource();
  }
  
  buildForm(): void {
    this.orderForm = this.formBuilder.group({
      section_id: ['', [Validators.required]],
      receipt: [96586, [Validators.max(99999999), Validators.min(0)]],
      order_number: [6523, [Validators.max(99999999), Validators.min(0)]],
      date_issue: [new Date],
      details: this.formBuilder.array([])
    });
  }

  getAreas(): void {
    this.areaService.getAreas().subscribe(
      (areas: Area[]) => {
        this.areas = areas;
      },
      (error: any) => {
        console.log(error);
      },
    )
  }
  getArticles(): void {
    this.articlesService.getArticles()
    .subscribe(
      (articles: Article[]) => {
        this.articles = articles;
        // console.log(articles);
      },
      (error: any) => {
        console.log(error);
      },
    )
  }

  buildDataSource(): void {
    this.articles$.subscribe(
      (articles: any[]) => {
        console.log(articles);
        articles.forEach((article: Article | any) => {
          this.addArticleToForm(article);
        });
        // console.log(this.detailsForm.value);
        this.dataSource = this.detailsForm.value;
      }
    ).unsubscribe();
  }

  editArticleCart(article: Article | any, index: number): void {
    const dialogRef = this.dialog.open(EditArticleCartComponent, {
      width: '380px',
      maxHeight: '400px',
      data: {
        articles: this.articles,
        articlesAdded: this.dataSource,
        article: article
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(
      (res: FormGroup) => {
        // console.log(res);
        // this.articlesData.value;
        if (res) {
            this.detailsForm.removeAt(index);
            this.detailsForm.push(res);
            this.dataSource = this.detailsForm.value;
            this.storageService.store(AuthConstants.SHOPPING_CART, this.dataSource);

            // this.buildDataSource();
            // this.shoppingCartService.editCart(res.value);
        }
      }
    )
  }


  isExist(value: Article | any): boolean {
    let res = false;
    this.dataSource.forEach(element => {
      if (element.material_id === value.material_id) {
        res = true;
      }
    });
    return res;
  }
  addArticleToForm(article: Article | any): void {
    const articleForm = this.formBuilder.group({
      material_id: [''],
      quantity: [''],
      article_name: [''],
      unit: [''],
    });
    articleForm.patchValue(article);
    this.detailsForm.push(articleForm);
  }

  // array of articles
  get detailsForm(): FormArray {
    return this.orderForm.get('details') as FormArray;
  }

  deleteArticleCart(article: Article | any): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la materia prima/insumo: ' + article.article_name + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.shoppingCartService.deleteCart(article);
        this.detailsForm.clear();
        this.buildDataSource();
        swalWithBootstrapButtons.fire({
          title: '¡Eliminado!',
          text: "Tu materia prima/insumo se eliminó correctamente",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
      }
    })
  }

  public downloadPDF(): void {
    this.stateDownload = true;
    const doc = new jsPDF('L', 'mm', 'letter');
    
    const DATA : any = document.getElementById('htmlNote'); 
     console.log(doc);
   
     const options = {
       background: 'white',
       scale: 3
     }
 
     html2canvas(DATA, options).then(
       (canvas) => {
         
       const img = canvas.toDataURL('image/PNG');
       const bufferX = 3;
       const bufferY = 1;
       const imgProps = (doc as any).getImageProperties(img);
       const pdfWidth = 0;
       const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
       doc.addImage(img, 'PNG', bufferX, 10 , pdfWidth, 100, undefined, 'FAST');
       return doc;
     }).then((docResult) => {
       docResult.save(`${new Date().toISOString()}notasolicitud.pdf`);
       this.stateDownload = false;
     });
 
   }

   sendData(): void {
     this.state = true;
    //  console.log(this.orderForm.value);
     this.shoppingCartService.sendData(this.orderForm.value).subscribe(
       (res: any) => {
         this.state = false;
         Swal.fire({
          title: '¡Enviado correctamente!',
          text: "Tu solicitud de materias primas/insumos fueron enviados correctamente",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
        this.storageService.remove(AuthConstants.SHOPPING_CART);
        this.orderForm.controls.receipt.reset();
        this.orderForm.controls.order_number.reset();
        this.orderForm.controls.details.reset();
        this.dataSource = [];
      },
      (error: any) => {
        this.state = false;
        const errorResponse = this.messageService.get();
        if (errorResponse.errors.receipt) {
          // console.log('Error del backend', errorResponse);
          Swal.fire({
            title: '¡Oops...!',
            text: "El numero de comprobante ya existe, intenta cambiar por otro",
            icon: 'warning',
            showConfirmButton: true,
            confirmButtonColor: '#85CE36',
            // timer: 2000
          });
        }
        if (!errorResponse) {
          Swal.fire({
            title: '¡Error!',
            text: "Ocurrio un problema inseperado, contactese con administración",
            icon: 'error',
            showConfirmButton: true,
            confirmButtonColor: '#85CE36',
            // timer: 2000
          });
        }

      },
     )
   }

   cancelOrder(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Quiéres cancelar la solicitud?',
      text: "Se perderan los datos que se configuro anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Cancelar!',
      cancelButtonText: 'Continuar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.detailsForm.clear();
        this.dataSource = this.detailsForm.value;
        this.shoppingCartService.clearData();
        swalWithBootstrapButtons.fire({
          title: 'Cancelado!',
          text: "Tu solicitud se canceló",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
      }
    })
   }

}
