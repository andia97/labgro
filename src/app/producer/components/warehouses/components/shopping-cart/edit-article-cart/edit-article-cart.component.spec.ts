import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditArticleCartComponent } from './edit-article-cart.component';

describe('EditArticleCartComponent', () => {
  let component: EditArticleCartComponent;
  let fixture: ComponentFixture<EditArticleCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditArticleCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditArticleCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
