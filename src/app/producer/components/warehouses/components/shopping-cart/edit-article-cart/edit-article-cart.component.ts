import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { Observable } from 'rxjs';
import { map, startWith, filter } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './edit-article-cart.component.html',
  styleUrls: ['./edit-article-cart.component.css']
})
export class EditArticleCartComponent implements OnInit, AfterViewInit {
  dataArticleForm!: FormGroup;
  articlesAdded!: Article[] | any[];

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }

  get article_name():any {
    return this.dataArticleForm.get('article_name');
  }

  public errorsMessages = {
    article_name: [
      { type: 'required', message: 'Selecciona una materia prima/insumo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
    ],
  };

  options!: Article[];
  filteredOptions!: Observable<Article[]>;
  // variable use only given the unit price last
  article!: Article;

  constructor(
    private dialogRef: MatDialogRef<EditArticleCartComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder
  ) {
    // console.log(this.data);
    this.buildForm();
    this.article = this.data.article;
    this.dataArticleForm.patchValue(this.article);
    // this.dataArticleForm.controls.unit.setValue(this.article.unit_measure);
    // console.log(this.dataArticleForm.value);
  }
  
  ngOnInit(): void {
    // this.displayFn(this.data.article);
    this.options = this.data.articles;
    setTimeout(() => {
      this.filteredOptions = this.dataArticleForm.controls.article_name.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name_article),
        map(name_article => name_article ? this._filter(name_article) : this.options.slice())
        );
    }, 1200);
    this.buildNewArray();
  }
  
  ngAfterViewInit(): void {
  }

  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article_name: ['', [Validators.required]],
      material_id: ['', [Validators.required]],
      unit: [''],
      quantity: ['', [Validators.required, Validators.min(0)]]
    });
  }

  saveData(): void {
    if (this.isExist()) {
      Swal.fire({
        title: '¡Oops...!',
        text: "La materia prima/insumo seleccionado ya se encuentra en la lista, elija otro porfavor",
        icon: 'warning',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
      })
    } else {
      this.dialogRef.close(this.dataArticleForm);
    }
  }

  isExist(): boolean {
    let res = false;
    const id = this.dataArticleForm.controls.material_id.value;
    this.articlesAdded.forEach(element => {
      if (element.material_id === id) {
        res = true;
      }
    });
    return res;
  }

  buildNewArray() {
    const articles: Article[] | any[] = this.data.articlesAdded;
    // console.log(articles);
    const id = this.dataArticleForm.controls.material_id.value;
    // console.log(id);
    const articlesAux: any[] = [];
    articles.forEach(element => {
      if ( element.material_id !== id ) {
        articlesAux.push(element);
      }
    });
    this.articlesAdded = articlesAux;
  }

  // Methods for the autocomplete
  displayFn(article: Article | any): string {
    let res: string = '';
    if (article && article.name_article) {
      res = article.name_article;
      console.log(res);
      
    } else {
      // console.log(article);
      res = article;
      // console.log(res);
    }
    // console.log(res);

    return res;
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        // console.log(value);
        if (!value) {
          this.dataArticleForm.controls.material_id.reset();
        }
        return value;
      }
    );
  }

  changeArticle(option: Article): void {
    this.dataArticleForm.controls.article_name.setValue(option.name_article);
    this.dataArticleForm.controls.unit.setValue(option.unit_measure);
    this.dataArticleForm.controls.material_id.setValue(option.id);
    // console.log(this.dataArticleForm.value);
    // console.log(option);
  }
  
}
