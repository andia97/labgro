import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleMonthDataComponent } from './article-month-data.component';

describe('ArticleMonthDataComponent', () => {
  let component: ArticleMonthDataComponent;
  let fixture: ComponentFixture<ArticleMonthDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleMonthDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleMonthDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
