import { Component, OnInit } from '@angular/core';
import { single } from './data';

@Component({
  templateUrl: './article-month-data.component.html',
  styleUrls: ['./article-month-data.component.css']
})
export class ArticleMonthDataComponent implements OnInit {
  single!: any[];
  view: any = [665, 450];

  colorScheme = {
    domain: [
      '#F0F3F6',
      '#eae2b780',
      '#2ECC71',
      '#67B2A5',
      '#8CFC9D',
      '#C1F33D',
      '#A8385D',
      '#FFB800',
      '#2196F3',
      '#E3F2FD',
      '#99C664',
      '#257933',
    ]
  };
  cardColor: string = '#3A4651';
  constructor() {
    Object.assign(this, { single });
  }

  ngOnInit(): void {
  }

}
