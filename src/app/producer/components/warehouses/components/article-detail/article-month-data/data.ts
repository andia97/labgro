export var single = [
  {
    "name": "Enero",
    "value": 98
  },
  {
    "name": "Febrero",
    "value": 15
  },
  {
    "name": "Marzo",
    "value": 70
  },
  {
    "name": "Abril",
    "value": 0
  },
  {
    "name": "Mayo",
    "value": 12
  },
  {
    "name": "Junio",
    "value": 21
  },
  {
    "name": "Julio",
    "value": 63
  },
  {
    "name": "Agosto",
    "value": 58
  },
  {
    "name": "Septiembre",
    "value": 39
  },
  {
    "name": "Octubre",
    "value": 52
  },
  {
    "name": "Noviembre",
    "value": 36
  },
  {
    "name": "Diciembre",
    "value": 96
  },
];