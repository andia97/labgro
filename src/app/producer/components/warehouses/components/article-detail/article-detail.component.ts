import { ArticleMonthDataComponent } from './article-month-data/article-month-data.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MaterialService } from '@core/services/material/material.service';
import { single } from '../../../production/components/data-chart-example';
import Swal from 'sweetalert2';
import { EditArticleWarehouseComponent } from '../../container/article/edit-article-warehouse/edit-article-warehouse.component';
import { ArticleService } from '@core/services/article/article.service';
import { ShoppingCartService } from '@core/services/shopping-cart/shopping-cart.service';
import { MaterialProducer } from '@core/models/material-producer';
import * as _moment from 'moment';
const moment = _moment;
moment.locale('es');

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  single!: any[];
  multi!: any[];
  id!: string;


  months=[
    {num:1,name:'Ene'},
    {num:2,name:'Feb'},
    {num:3,name:'Mar'},
    {num:4,name:'Abr'},
    {num:5,name:'May'},
    {num:6,name:'Jun'},
    {num:7,name:'Jul'},
    {num:8,name:'Ago'},
    {num:9,name:'Sep'},
    {num:10,name:'Oct'},
    {num:11,name:'Nov'},
    {num:12,name:'Dic'},
  ];

  year!: number;
  actualDate = moment();

  view: any = [750, 250];

  articleIncomes!: any[];
  articleOutputs!: any[];

  materialId!: number;
  material!: any;
  canRender=false;
  state: boolean=false;
  color!: string;

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Mes';
  showYAxisLabel = true;
  yAxisLabel = 'Cantidad';
  colorScheme = {
    domain: this.arrayColorShema()
  };
  constructor(
    private routerParam: ActivatedRoute,
    private materialService: MaterialService,
    private dialog: MatDialog,
    private articleService: ArticleService,
    private router: Router,
    private cartService: ShoppingCartService,
    private serviceMaterial:  MaterialService,
    private route: ActivatedRoute
  ) {
    this.routerParam.params.subscribe(
      (params: Params) => {
        this.materialId = params.id;
      }
    );
    const actualYear = this.actualDate.format('YYYY');
    this.year = + actualYear;
  }

  formatSingle(): void{
    const res:any[]=[];
    this.months.map((val,index)=>{
      const arr=this.articleIncomes.filter((item,i)=>{
        const addDay=new Date(item.date_of_admission)
        addDay.setDate(addDay.getDate()+1)
        return addDay.getMonth()+1===val.num
      })
      if(arr.length>1){
        let quantityTotal=0;
        arr.map((item)=>{
          quantityTotal = quantityTotal+item.quantity;
        })
        res.push({
          "name":val.name,
          "value":quantityTotal
        });
      }else{
        if(arr.length===0){
          res.push({
            "name":val.name,
            "value":0
          });

        }else{
          res.push({
            "name":val.name,
            "value":arr[0].quantity
          });
        }
      }
    })
    this.single=res;
  };

  ngOnInit(): void {
    this.fetchMaterial();
    this.fetchMaterialIncomes();
    this.fetchMateriaOutputs();

  }

  fetchMaterial(): void{
    this.state = true;
    this.materialService.getMaterial(this.materialId)
    .subscribe((res: any)=>{
      this.state=false;
      this.material = res.material[0];
      this.color = res.material[0].color;
      this.canRender = true;
    },(err)=>{
      console.error(err)
    })
  }

  fetchMaterialIncomes(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    this.materialService.getMaterialIncomes(this.materialId, this.year)
    .subscribe(
      (res: any)=>{
        this.articleIncomes = res;
        this.formatSingle();
      },
      (err)=>{
        console.error(err)
        swalWithBootstrapButtons.fire({
          title: 'Oops!',
          text: "No se encontraron Entradas del material",
          icon: 'warning',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 3000
        });
      }
    )
  };

  filterDate(event: number): void {
    this.year = + event;
    this.fetchMaterialIncomes();
    this.fetchMateriaOutputs();
  }


  fetchMateriaOutputs(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })
    this.materialService.getMaterialOutputs(this.materialId, this.year)
    .subscribe(
      (res: any)=>{
        this.articleOutputs = res;
      },
      (err)=>{
        console.error(err)
        swalWithBootstrapButtons.fire({
          title: 'Oops!',
          text: "No se encontraron Salidas del material",
          icon: 'warning',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 3000
        });
      }
    )
  };

  arrayColorShema(): string[] {
    const colorSchema: string[] = [];
    for (let index = 0; index < 12; index++) {
      colorSchema.push('#67B2A5');
    }
    return colorSchema;
  }

  openCalendar(): void {
    const dialogRef = this.dialog.open(ArticleMonthDataComponent, {
      width: '650px',
      minHeight: '590px',
      maxHeight: '590px',
      panelClass: 'custom-dialog-container'
    });
  }

  editArticle(): void {
    this.articleService.getArticles()
    .subscribe(
      (res: any)=>{
        const dialogRef = this.dialog.open( EditArticleWarehouseComponent, {
          width: '480px',
          height: '600px',
          panelClass: 'custom-dialog-container',
          data: {
            articles: res,
            material: this.material
          }
        });

        dialogRef.afterClosed().subscribe(
          (result: any ) =>{
            this.fetchMaterial();
          }
        )
      },
      (err: any)=>{
        console.error("something went wrong: "+ err)
      }
    )
  }


  deleteArticle(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el material: ' + this.material.name_article + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.materialService.deleteMaterial(this.material.id)
        .subscribe(
          (res: any)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu producto se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
            this.router.navigate(['/producer/warehouse'])
          },(err)=>{
            console.log(err );

          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });
        })
      }
    })
  }

  addToCart(rawMaterial: MaterialProducer | any): void {
    this.cartService.addCart(rawMaterial);
  }



  getMaterialArticle(){
    this.serviceMaterial.getMaterialIncomes(+this.id , this.year )
    .subscribe(
      (data: any)=>{
        console.log(data)
        this.articleIncomes = data;
      }
    )
  }


  getid(){
    this.route.params.subscribe(params => {
       this.id = params['id'];
    })
  }
}
