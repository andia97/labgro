import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionRoutingModule } from './production-routing.module';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ProductContainer } from './container/product/product.container';

import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductLayoutComponent } from './components/product-layout/product-layout.component';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { GeneralDataComponent } from './components/create-product/general-data/general-data.component';
import { UnitsPresentationComponent } from './components/create-product/units-presentation/units-presentation.component';
import { ArticlesSuppliesComponent } from './components/create-product/articles-supplies/articles-supplies.component';
import { DataPresentationComponent } from './components/product-detail/data-presentation/data-presentation.component';
import { DataArticlesComponent } from './components/product-detail/data-articles/data-articles.component';
import { EditArticleComponent } from './components/product-detail/data-articles/edit-article/edit-article.component';
import { EditPresentationComponent } from './components/product-detail/data-presentation/edit-presentation/edit-presentation.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { AddArticleComponent } from './components/product-detail/data-articles/add-article/add-article.component';
import { AddPresentationComponent } from './components/product-detail/data-presentation/add-presentation/add-presentation.component';

import { AddeditUnitComponent } from './components/create-product/addedit-unit/addedit-unit.component';
import { AddEditArticleComponent } from './components/create-product/add-edit-article/add-edit-article.component';
import { HistoryDataComponent } from './container/history-data/history-data.component';


@NgModule({
  declarations: [
    ProductLayoutComponent,
    ProductDetailComponent,
    ProductContainer,
    CreateProductComponent,
    GeneralDataComponent,
    UnitsPresentationComponent,
    ArticlesSuppliesComponent,
    DataPresentationComponent,
    DataArticlesComponent,
    EditArticleComponent,
    EditPresentationComponent,
    EditProductComponent,
    AddArticleComponent,
    AddPresentationComponent,
    AddeditUnitComponent,
    AddEditArticleComponent,
    HistoryDataComponent,
  ],
  imports: [
    CommonModule,
    ProductionRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxChartsModule,
    
  ]
})
export class ProductionModule { }
