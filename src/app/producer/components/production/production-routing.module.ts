import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductLayoutComponent } from './components/product-layout/product-layout.component';

const routes: Routes = [

  { path: '', component: ProductLayoutComponent },
  { path: 'product-detail/:id', component: ProductDetailComponent },
  { path: 'create-product', component: CreateProductComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
