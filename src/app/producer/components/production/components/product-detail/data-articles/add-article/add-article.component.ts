import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ArticleService } from '@core/services/article/article.service';
import { Article } from '@core/models/article.model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { ProductService } from '@core/services/product/product.service';
import { Product } from '@core/models/product.model';
@Component({
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  options: Article[] = [];
  canRender = false;

  articleSelected!:Article;

  // variable for the data form
  dataArticleForm!: FormGroup;
  onlyNumber = '^[0-9]+$';
  numRegex = /^-?\d*[.,]?\d{0,2}$/;

  articleMeasure!: string;


  state!: boolean;
  filteredOptions!: Observable<Article[]>;
  product!: Product;

  public errorsMessages = {
    article: [
      { type: 'required', message: 'Selecciona una materia/insumo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'max', message: 'La cantidad supera el stock'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  }

  get articleData():any {
    return this.dataArticleForm.get('article');
  }

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }

  constructor(
    private dialogRef: MatDialogRef<AddArticleComponent>,
    private articleService: ArticleService,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.buildForm();
    this.product = data.product;
    this.options = data.articles;
    this.formatOptions();
  }

  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ ,
        [Validators.required]
      ],
      article_id: [''],
      quantity: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.max(999999999),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_measure: ['']
    });
  }

  ngOnInit(): void {

  }

  formatOptions(): void{
      this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name_article),
          map(name_article => name_article ? this._filter(name_article) : this.options.slice())
        );
  }
  // getArticles(): void {
  //   this.state = true;
  //   this.articleService.getArticles().subscribe(
  //     (res: Article[]) => {
  //       this.state = false;
  //       this.canRender=true;
  //       this.options = res;
  //       this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
  //       .pipe(
  //         startWith(''),
  //         map(value => typeof value === 'string' ? value : value.name_article),
  //         map(name_article => name_article ? this._filter(name_article) : this.options.slice())
  //       );
  //     },
  //     (error: any) => {
  //       alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
  //     },
  //   )
  // }


  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }

  getArticle(article: Article): void {
    this.dataArticleForm.controls.article_id.setValue(article.id);
    this.articleSelected = article;
    this.articleMeasure = article.unit_measure;
    // this.dataArticleForm.controls.quantity.setValidators(
    //   [
    //     Validators.max(this.articleSelected.stock),
    //     Validators.required,
    //     Validators.pattern(
    //       this.numRegex
    //     ),
    //     Validators.min(1),
    //   ])
  }

  saveData(): void {
    this.productService.createProductArticle(this.product.id,this.articleSelected.id,this.dataArticleForm.value.quantity)
    .subscribe((res: any)=>{
      if(res.sucess){
        Swal.fire({
          title: '¡Agregado correctamente!',
          text: 'Los datos de la materia prima / insumo se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        });
        this.dialogRef.close();
      }else{
        Swal.fire({
          title: '¡Oops!',
          text: res.error+" en tus ingredientes",
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        });
        this.dialogRef.close();
      }
    },
    (err)=>{
      Swal.fire({
        title: '¡Error!',
        text: 'Ocurrio un problema inseperado, contactese con administración',
        icon: 'error',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
      })
    })
  }

  displayFn(article: Article): string {
    return article && article.name_article ? article.name_article : '';
  }

  getFormulaBase(event: any){
    const pattern = /^[0-9-+./*()]+$/;   
    console.log(event.target.value);
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^0-9-+/.*()]/g,"");
      }
  }
}
