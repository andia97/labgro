import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { Product } from '@core/models/product.model';
import { ArticleService } from '@core/services/article/article.service';
import { ProductService } from '@core/services/product/product.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {

  options!: Article[];
  filteredOptions!: Observable<Article[]>;
  canRender=false;
  dataArticleForm!: FormGroup;

  articleSelected!: Article;
  articleMeasure!: string;

  onlyNumber = '^[0-9]+$';
  numRegex = /^-?\d*[.,]?\d{0,2}$/;
  state = false;

  product!: Product;
  stock!: number;
  unit_measure!: string;

  public errorsMessages = {
    article: [
      { type: 'required', message: 'Selecciona un articulo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'max', message: 'La cantidad supera el stock'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  }

  get articleData():any {
    return this.dataArticleForm.get('article');
  }

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }

  constructor(
    private dialogRef: MatDialogRef<EditArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private articleService: ArticleService,
    private productService: ProductService,
    ) {
      this.buildForm();
      this.product = data.product;
      this.stock= data.articleCategory.stock_total;
      this.unit_measure = data.article.unit_measure;
    }

  private buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ ,
        [Validators.required]
      ],
      article_id: [ ,
        [
          Validators.required
        ]
      ],
      quantity: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.max(999999999),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_measure: ['']
    });
  }

  ngOnInit(): void {
    const dataForm = this.data.article;
    this.dataArticleForm.patchValue(dataForm);
    // this.getArticles();
    this.getArticle();
  }

  getArticle(): void {
    this.dataArticleForm.controls.article.setValue(this.data.article);
    this.dataArticleForm.controls.article_id.setValue(this.data.article.id);
    this.articleSelected = this.data.article;
    this.articleMeasure = this.data.article.unit_measure;

    // this.dataArticleForm.controls.quantity.setValidators(
    //   [
    //     Validators.max(this.stock),
    //     Validators.required,
    //     Validators.pattern(
    //       this.numRegex
    //     ),
    //     Validators.min(1),
    //   ])
  }

  // getArticles(): void {
  //   this.state = true;
  //   this.articleService.getArticles().subscribe(
  //     (res: Article[]) => {
  //       this.state = false;
  //       this.options = res;
  //       this.canRender = true;
  //       this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
  //       .pipe(
  //         startWith(''),
  //         map(value => typeof value === 'string' ? value : value.name_article),
  //         map(name_article => name_article ? this._filter(name_article) : this.options.slice())
  //         );
  //     },
  //     (error: any) => {
  //       alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
  //     },
  //   )
  // }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }

  displayFn(article: Article): string {
    return article && article.name_article ? article.name_article : '';
  }

  compareCategoryObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id == object2.id;
  }

  saveData(): void {
    this.productService.updateProductArticle(this.product.id,this.articleSelected.id,this.dataArticleForm.value.quantity)
    .subscribe((res: any)=>{
      Swal.fire({
        title: '¡Agregado correctamente!',
        text: 'Los datos de la materia prima/insumo se guardaron correctamente',
        icon: 'success',
        timer: 2000,
        showConfirmButton: false,
      });
      this.dialogRef.close();
    },
    (err)=>{
      Swal.fire({
        title: '¡Error!',
        text: 'Ocurrio un problema inseperado, contactese con administración',
        icon: 'error',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
      })
    })
  }
}
