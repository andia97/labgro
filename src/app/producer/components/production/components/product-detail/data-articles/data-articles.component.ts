import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddArticleComponent } from './add-article/add-article.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { ProductService } from '@core/services/product/product.service';
import Swal from 'sweetalert2';
import { ArticleProducer } from '@core/models/article-producer';
import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { Product } from '@core/models/product.model';
import {MaterialService} from "@core/services/material/material.service";

@Component({
  selector: 'app-data-articles',
  templateUrl: './data-articles.component.html',
  styleUrls: ['./data-articles.component.css']
})
export class DataArticlesComponent implements OnInit {
  displayedColumns: string[] = ['article', 'quantity', 'unit', 'action'];
  @Input() dataSource!: ArticleProducer[];
  @Input() product!: Product;

  emptyState = true;
  dataState = false;

  idProduct!: number;
  constructor(
    private dialog: MatDialog,
    private productService: ProductService,
    private articleService: ArticleService,
    private materialService: MaterialService,
  ) { }

  ngOnInit(): void {
  }

  addArticle(): void {
    this.materialService.getMaterials()
    .subscribe((res: any)=>{
      console.log(res)
      const dialogRef = this.dialog.open(AddArticleComponent, {
        width: '580px',
        height: '450px',
        panelClass: 'custom-dialog-container',
        data:{
          articles: res.raw_material,
          product: this.product
        }
      });
      dialogRef.afterClosed().subscribe(
        (result: any ) =>{
          this.getProductMaterials();
        }
      )
    }),
    (err)=>{
      console.error(err);
    }
  }

  getProductMaterials(): void {
    this.productService.getProductMaterials(this.product.id).
    subscribe(
      (res: any) => {
        this.dataSource = res;
      },
      (error: any) => {
        if(error=="No se encontro resultados") this.dataSource = [];
        console.error(error);
      },
    )
  }

  editArticle(article: Article): void {
    this.articleService.getArticle(article.id)
    .subscribe(
    ({category}:any)=>{
      const dialogRef = this.dialog.open(EditArticleComponent, {
        width: '580px',
        height: '450px',
        panelClass: 'custom-dialog-container',
        data:{
          article,
          articleCategory:category,
          product: this.product,
        }
      });

      dialogRef.afterClosed().subscribe(
        (result: any ) =>{
            this.getProductMaterials();
        }
      )
    },
    (err)=>{

    })

  }

  deleteArticle(article: Article): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el artículo?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.productService.deleteProductMaterial(this.product.id,article.id)
        .subscribe(
          (res: any)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "El artículo seleccionado ya no pertenece a tu lista de entradas",
              icon: 'success',
              showConfirmButton: false,
              timer: 1800
            });
            this.getProductMaterials();
        },(err: any)=>{
          console.error(err)
        })
        // this.articlesData.removeAt(index);
        // this.articles = this.articlesData.value;
        // this.storageService.store(AuthConstants.DATA_IN_ARTICLE, this.articles);

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu artículo esta a salvo",
          icon: 'error',
          showConfirmButton: false,
          timer: 1800
        });
      }
    })
  }
}
