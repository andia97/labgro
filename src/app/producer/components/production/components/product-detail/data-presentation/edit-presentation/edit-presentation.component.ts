import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '@core/models/product.model';
import { PresentationService } from '@core/services/presentation/presentation.service';
import { ProductService } from '@core/services/product/product.service';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './edit-presentation.component.html',
  styleUrls: ['./edit-presentation.component.css']
})
export class EditPresentationComponent implements OnInit {

  options!: string[];
  dataPresentationForm!: FormGroup;
  product!: Product;

  canRender=false;
  numRegex = /^-?\d*[.,]?\d{0,2}$/;


  get unitCostProduction():any {
    return this.dataPresentationForm.get('unit_cost_production');
  }
  get unitPrice():any {
    return this.dataPresentationForm.get('unit_price_sale');
  }

  public errorsMessages = {
    presentation: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    unit_cost_production: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
    unit_price_sale: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  }

  constructor(
    private dialogRef: MatDialogRef<EditPresentationComponent>,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.buildForm();
    this.product=data.product;
   }

   buildForm(): void {
    this.dataPresentationForm = this.formBuilder.group({
      presentation: [ ,
        [Validators.required]
      ],
      presentation_id: [""],

      unit_cost_production: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)

        ]
      ],
      unit_price_sale: ['',
      [
        Validators.required,
        Validators.min(0),
        Validators.pattern(this.numRegex)

      ]
    ]
    });
  }

  ngOnInit(): void {
    const dataForm = this.data.presentation;
    this.dataPresentationForm.patchValue(dataForm);
    this.fillPresentationField();
  }
  fillPresentationField(): void {
    this.canRender = true;
    this.dataPresentationForm.controls.presentation.setValue(this.data.presentation.name);
    this.dataPresentationForm.controls.presentation_id.setValue(this.data.presentation.id);
  }

  saveData(): void {
    this.productService.updateProductPresentation(this.product.id,this.data.presentation.id,this.dataPresentationForm.value.unit_cost_production,this.dataPresentationForm.value.unit_price_sale)
    .subscribe((res: any)=>{
      if(res.sucess){
        Swal.fire({
          title: 'Modificado correctamente!',
          text: 'Los datos de la presentacion se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        });
        this.dialogRef.close();
      }else{
        Swal.fire({
          title: '¡Error!',
          text: res.error,
          icon: 'error',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        });
        this.dialogRef.close();
      }
    },
    (err)=>{
      Swal.fire({
        title: '¡Error!',
        text: 'Ocurrio un problema inseperado, contactese con administración',
        icon: 'error',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
      })
    })
  }
}
