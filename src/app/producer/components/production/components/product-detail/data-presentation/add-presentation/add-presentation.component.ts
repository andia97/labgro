import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PresentationProducer } from '@core/models/presentation-producer';
import { Product } from '@core/models/product.model';
import { PresentationService } from '@core/services/presentation/presentation.service';
import { ProductService } from '@core/services/product/product.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './add-presentation.component.html',
  styleUrls: ['./add-presentation.component.css']
})
export class AddPresentationComponent implements OnInit {
  options!: PresentationProducer[];
  filteredOptions!: Observable<PresentationProducer[]>;

  product!: Product;

  state=false;
  canRender=false;
  dataPresentationForm!: FormGroup;

  presentationSelected!: PresentationProducer;

  onlyNumber = '^[0-9]+$';
  numRegex = /^-?\d*[.,]?\d{0,2}$/;

  get presentationData(): any{
    return this.dataPresentationForm.get('presentation');
  }

  get unitCostProduction():any {
    return this.dataPresentationForm.get('unit_cost_production');
  }
  get unitPrice():any {
    return this.dataPresentationForm.get('unit_price_sale');
  }

  public errorsMessages = {
    presentation: [
      { type: 'required', message: 'Selecciona una presentacion de la lista' },
    ],
    unit_cost_production: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},

    ],
    unit_price_sale: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  }

  constructor(
    private dialogRef: MatDialogRef<AddPresentationComponent>,
    private presentationsService: PresentationService,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.product = data.product;
    this.options = data.presentations;
    this.buildForm();
  }

  buildForm(): void {
    this.dataPresentationForm = this.formBuilder.group({
      presentation: [ ,
        [Validators.required]
      ],
      presentation_id: [""],

      unit_cost_production: ['',
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.numRegex)
        ]
      ],
      unit_price_sale: ['',
      [
        Validators.required,
        Validators.min(0),
        Validators.pattern(this.numRegex)
      ]
    ]
    });
  }

  ngOnInit(): void {
    this.getPresentations();
  }

  getPresentations(): void{
    // this.state = true;
    // this.presentationsService.getPresentations().subscribe(
    //   (res: any) => {
    //     this.state = false;
    //     this.canRender=true;
    //     this.options = res.presentations;
        this.filteredOptions = this.dataPresentationForm.controls.presentation.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? "" : value.name),
          map(name => name ?  this._filter(name): this.options.slice())
        );
    //   },
    //   (error: any) => {
    //     alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
    //   },
    // )
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(
      (option) => {
        const value = option.name.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataPresentationForm.controls.presentation_id.reset();
        }
        return value;
      }
    );
  }

  public getPresentation(presentation: PresentationProducer): void{
    this.dataPresentationForm.controls.presentation_id.setValue(presentation.id);
    this.presentationSelected = presentation;
  }

  saveData(): void {
    this.productService.createProductPresentation(this.product.id,this.dataPresentationForm.value.presentation.id,this.dataPresentationForm.value.unit_cost_production,this.dataPresentationForm.value.unit_price_sale)
    .subscribe((res: any)=>{
      if(res.sucess){
        Swal.fire({
          title: '¡Agregado correctamente!',
          text: 'Los datos de la presentacion se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        });
        this.dialogRef.close();
      }else{
        Swal.fire({
          title: '¡Oops!',
          text: res.error,
          icon: 'warning',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        });
        this.dialogRef.close();
      }
    },
    (err)=>{
      Swal.fire({
        title: '¡Error!',
        text: 'Ocurrio un problema inseperado, contactese con administración',
        icon: 'error',
        showConfirmButton: true,
        confirmButtonColor: '#85CE36'
      })
    })
  }
  displayFn(presentation: PresentationProducer): string {
    return presentation && presentation.name ? presentation.name : '';
  }

}
