import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditPresentationComponent } from './edit-presentation/edit-presentation.component';

import Swal from 'sweetalert2';
import { AddPresentationComponent } from './add-presentation/add-presentation.component';
import { PresentationProducer } from '@core/models/presentation-producer';
import { ProductService } from '@core/services/product/product.service';
import { PresentationService } from '@core/services/presentation/presentation.service';
import { Product } from '@core/models/product.model';

@Component({
  selector: 'app-data-presentation',
  templateUrl: './data-presentation.component.html',
  styleUrls: ['./data-presentation.component.css']
})
export class DataPresentationComponent implements OnInit {
  displayedColumns: string[] = ['presentation', 'unit_cost', 'unit_sale', 'action'];
  @Input() dataSource!: PresentationProducer[];
  @Input() product!: Product;
  
  constructor(
    private dialog: MatDialog,
    private productService: ProductService,
    private presentationService: PresentationService
  ) { }

  ngOnInit(): void {
  }

  getProductPresentations(): void {
    this.productService.getProductPresentations(this.product.id).
    subscribe(
      (res: any) => {
        this.dataSource = res;
      },
      (error: any) => {
        if(error=="No se encontraron resultados") this.dataSource = [];
        console.error(error);
      },
    )
  }

  addPresentation(): void {
    this.presentationService.getPresentations()
    .subscribe(
      (res: any)=>{
        const dialogRef = this.dialog.open(AddPresentationComponent, {
          width: '580px',
          height: '450px',
          panelClass: 'custom-dialog-container',
          data:{
            product: this.product,
            presentations: res.presentations
          }
        });
        dialogRef.afterClosed().subscribe(
          (result: any ) =>{
              this.getProductPresentations();
          }
        )
      },
      (err)=>{

      })
    
  }

  editPresentation(presentation: PresentationProducer): void {
    const dialogRef = this.dialog.open(EditPresentationComponent, {
      width: '580px',
      height: '450px',
      panelClass: 'custom-dialog-container',
      data:{
        presentation,
        product: this.product,
      }
    });
    dialogRef.afterClosed().subscribe(
      (result: any ) =>{
          this.getProductPresentations();
      }
    )
  }

  deletePresentation(presentation: PresentationProducer): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la presentación: ' + name + ' ?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.productService.deleteProductPresentation(this.product.id,presentation.id)
        .subscribe(
          (res: any)=>{
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "El artículo seleccionado ya no pertenece a tu lista de entradas",
              icon: 'success',
              showConfirmButton: false,
              timer: 1800
            });
            this.getProductPresentations();
        },(err: any)=>{  
          console.error(err)
        })
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu presentación esta a salvo",
          icon: 'error',
          showConfirmButton: false,
          timer: 1800
        });
      }
    })
  }
}
