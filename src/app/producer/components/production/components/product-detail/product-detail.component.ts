import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditProductComponent } from '../edit-product/edit-product.component';
import Swal from 'sweetalert2';

import { Router, ActivatedRoute, Params  } from '@angular/router';
import { ArticleProducer } from '@core/models/article-producer';
import { PresentationProducer } from '@core/models/presentation-producer';
import { ProductService } from '@core/services/product/product.service';
import { Product } from '@core/models/product.model';
// import 'rxjs/add/operator/filter';
import * as _moment from 'moment';
import {ProductionService} from "@core/services/production/production.service";
const moment = _moment;
moment.locale('es');

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  single!: any[];
  multi!: any[];

  view: any = [750, 250];

  months=[
    {num:1,name:'Ene'},
    {num:2,name:'Feb'},
    {num:3,name:'Mar'},
    {num:4,name:'Abr'},
    {num:5,name:'May'},
    {num:6,name:'Jun'},
    {num:7,name:'Jul'},
    {num:8,name:'Ago'},
    {num:9,name:'Sep'},
    {num:10,name:'Oct'},
    {num:11,name:'Nov'},
    {num:12,name:'Dic'},
  ];

  year!: number;
  actualDate = moment();

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Mes';
  showYAxisLabel = true;
  yAxisLabel = 'Cantidad';
  colorScheme = {
    domain: this.arrayColorShema()
  };

  dataSourcePresentation!: PresentationProducer[];
  dataSourceArticle!: ArticleProducer[];

  productId!: number;
  product!: Product;

  productions!:any[];

  state: Boolean=false;
  emptyState!: Boolean;
  dataState!: Boolean;

  totalQuantityProduced: number = 0;

  constructor(
    private dialog: MatDialog,
    private route: Router,
    private routerParam: ActivatedRoute,
    private ProductService: ProductService,
    private router: Router,
    private productionService: ProductionService
  ) {
    this.routerParam.params.subscribe(
      (params: Params) => {
        this.productId = params.id;
      }
    );
    const actualYear = this.actualDate.format('YYYY');
    this.year = + actualYear;
  }

  ngOnInit(): void {
    this.getProduct();
    this.fetchProductProduction();
  }

  fetchProductProduction(): void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })
    this.productionService.getProductionsByProductId(this.year, this.productId)
      .subscribe(
        (res: any)=>{
          if(res.length>0){
            this.productions=res;
            this.formatSingle();
          }else{
            this.single=[];
            this.productions=[];
            swalWithBootstrapButtons.fire({
              title: 'Oops!',
              text: "No exiten datos del año: "+this.year,
              icon: 'warning',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 3000
            });
          }
        },(error: any)=>{
          console.error(error)
        }
      )
  }


  getProduct(): void {
    this.state = true;
    this.ProductService.getProduct(this.productId).
    subscribe(
      (res: any) => {
        this.state = false;
        this.dataSourcePresentation=res.presentations;
        this.dataSourceArticle=res.materials;
        this.product = res.product;
        if (!this.product) {
          this.emptyState = true;
          this.dataState = false;
        } else {
          this.emptyState = false;
          this.dataState = true;
        }
        this.state = false;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    )
  }

  formatSingle(): void{
    const res:any[]=[];
    this.months.map((val,index)=>{
      const arr=this.productions.filter((item,i)=>{
        const addDay=new Date(item.date);
        addDay.setDate(addDay.getDate()+1)
        return addDay.getMonth()+1===val.num;
      })
      if(arr.length>1){
        let quantityTotal=0;
        arr.map((item)=>{
          quantityTotal = quantityTotal+item.product_quantity;
        })
        res.push({
          "name":val.name,
          "value":quantityTotal
        });
        this.totalQuantityProduced=this.totalQuantityProduced+quantityTotal;
      }else{
        if(arr.length===0){
          res.push({
            "name":val.name,
            "value":0
          });

        }else{
          res.push({
            "name":val.name,
            "value":arr[0].product_quantity
          });
          this.totalQuantityProduced=this.totalQuantityProduced+arr[0].product_quantity;
        }

      }
    })
    this.single=res;
  };

  editProduct(): void {
    const dialogRef = this.dialog.open(EditProductComponent, {
      width: '952px',
      maxHeight: '490px',
      minHeight: '490px',
      panelClass: 'custom-dialog-container',
      data: {
        data: this.product
      }
    });

    dialogRef.afterClosed().subscribe(
      (result: any ) =>{
          this.getProduct();
      }
    )
  }

  deleteProduct(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el producto?',
      text: "Se perderan todos los datos con relación a este producto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.ProductService.deleteProduct(this.product.id).subscribe(
          (res: any) => {
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu producto se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
        },
        (error: any) => {
          console.log(error);
          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });
        }
        )
        this.route.navigate(['/producer/product']);
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu producto esta a salvo",
          icon: 'error',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
      }
    })
  }

  changeYear(yearSelected: any): void{
    this.year=yearSelected;
    this.totalQuantityProduced=0;
    this.fetchProductProduction();
  }

  onSelect(event) {
    console.log(event);
  }

  arrayColorShema(): string[] {
    const colorSchema: string[] = [];
    for (let index = 0; index < 12; index++) {
      colorSchema.push('#85CE36');
    }
    return colorSchema;
  }

  goBack(): void {
    this.router.navigate(['/producer/product/']);
  }
}


