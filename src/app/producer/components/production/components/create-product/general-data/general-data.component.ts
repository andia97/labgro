import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestorageService } from '@core/services/firestorage/firestorage.service';

@Component({
  selector: 'app-general-data',
  templateUrl: './general-data.component.html',
  styleUrls: ['./general-data.component.css']
})
export class GeneralDataComponent implements OnInit {
  
  url= "./assets/images/data-img/data-in.png";
  name!: string;
  file!: any;
  public formData!: FormGroup;
  msg!: string; 
  error: boolean = false;

  @Output() emitterDataForm: EventEmitter<FormGroup> = new EventEmitter();
  
  constructor(
    public formBuilder:  FormBuilder,
  ) {
    
    this.buildForm();
   }
  
   
 
  ngOnInit(): void {
    
  }
  
  async onselectFile(e){
    if(e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload=(event:any)=>{

        if(e.target.files[0].type === "image/jpeg" 
        || e.target.files[0].type === "image/png" 
        || e.target.files[0].type === "image/jpg"){
          this.url = event.target.result;
          this.error =  false; 
        }else {
          this.msg = "El formato no es el permitido por favor ingresar uno valido ";
          this.error = true;
        }
        

      }
      //const path = "Productos";
     // const name = this.formData.get('name')?.value;
      this.file = e.target.files[0];

    //  const res = await this.firesStoreService.uploadImagen(file,path,name);
      //console.log(res);
    }
   
  }



  onSubmmit(){
    console.log(this.formData);
    this.emitterDataForm.emit(this.formData);
  }

   
  buildForm() {
    this.formData = this.formBuilder.group({
      name  : ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]],
      codigo : ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)
      ]],
      description: ['', [
        Validators.required, 
        Validators.minLength(1),
       
      ]],
     
    });
    
   
  }


}
