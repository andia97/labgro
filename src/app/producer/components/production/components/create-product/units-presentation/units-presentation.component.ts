import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PresentationProducer } from '@core/models/presentation-producer';
import { PresentationService } from '@core/services/presentation/presentation.service';
import Swal from 'sweetalert2';
import { AddeditUnitComponent } from '../addedit-unit/addedit-unit.component';


@Component({
  selector: 'app-units-presentation',
  templateUrl: './units-presentation.component.html',
  styleUrls: ['./units-presentation.component.css']
})
export class UnitsPresentationComponent implements OnInit {

  @Input()
   nameProduct: string = '';


  data: PresentationProducer[] =[];
  displayedColumns: string[] = ['Unidad/Presentacion', 'CostoUnitario', 'PrecioUnitario', 'Edit'];
  dataForm!: FormGroup;
  @Output() emitterDataForm: EventEmitter<PresentationProducer[]> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private servicePresentation: PresentationService,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    console.log(this.nameProduct);
    this.data = this.unitData.value;
  }


  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      unit: this.formBuilder.array([])
    });
  }


  get unitData(): FormArray {
    return this.dataForm.get('unit') as FormArray;
  }

  openUAddEditComponentU(name: string,obj:any,index:string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = this.nameProduct;
    dialogConfig.data = {name, obj };
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddeditUnitComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result : any ) =>{
        if(result.event === "Crear"){

          this.addUnit(result.data);

        }
        if(result.event === "Modificar"){
          this.editUnitData(+index,result.data)
        }
        if(result.event === "Eliminar"){
           this.deleteUnitData(+index);
        }
      }
    )

  }

  editUnitData(index:number,data: any){
    const form = this.formBuilder.group({
      presntation: [data.presentation],
      name: [data.presentation.name],
      unit_cost_production: [data.unit_cost_production],
      unit_price_sale: [data.unit_price_sale]
    });
    this.unitData.removeAt(index)
    this.unitData.push(form);
    this.data = this.unitData.value;
  }


  addUnit(data: any){
    console.log(data);
    const form = this.formBuilder.group({
      presntation: [data.presentation],
      name: [data.presentation.name],
      unit_cost_production: [data.unit_cost_production],
      unit_price_sale: [data.unit_price_sale],
      presentation_unit_id: [data.presentation.id]
    });
    console.log(form.value)
    this.unitData.push(form);
    this.data = this.unitData.value;
  }

  deleteUnitData(i:number){
    this.unitData.removeAt(i)
    this.data = this.unitData.value;
  }

  deleteUnit(unit:PresentationProducer, index: number): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la unidad de presentacion  : ' +unit.name+ ' ?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.unitData.removeAt(index)
        this.data = this.unitData.value;
        swalWithBootstrapButtons.fire({
          title: '¡Eliminado!',
          text: "El unidad de presentacion seleccionado ya no pertenece a tu lista de entradas",
          icon: 'success',
          showConfirmButton: false,
          timer: 1800
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu artículo esta a salvo",
          icon: 'error',
          showConfirmButton: false,
          timer: 1800
        });
      }
    })
  }

  public inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9_ ]+$/;
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z0-9_ ]/g,"");
      }
  }

  onSubmmit(){
    console.log(this.data);
    this.emitterDataForm.emit(this.data);
  }

}

