import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitsPresentationComponent } from './units-presentation.component';

describe('UnitsPresentationComponent', () => {
  let component: UnitsPresentationComponent;
  let fixture: ComponentFixture<UnitsPresentationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnitsPresentationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitsPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
