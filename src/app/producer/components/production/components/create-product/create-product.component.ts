import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FirestorageService } from '@core/services/firestorage/firestorage.service';
import Swal from 'sweetalert2';
import { AddeditUnitComponent } from './addedit-unit/addedit-unit.component';
import { GeneralDataComponent } from './general-data/general-data.component';
import { Router } from '@angular/router';
import { PresentationProducer } from '@core/models/presentation-producer';
import { ArticleProducer } from '@core/models/article-producer';
import { Product } from '@core/models/product.model';
import { ProductService } from '@core/services/product/product.service';
import { UploadService } from '@core/services/firebase/upload.service';
//import { UploadService } from '@core/services/firebase/upload.service';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  @ViewChild('dataGeneral')
  dataGeneral! :GeneralDataComponent;

  path: string = "Productos";

  product: Product = {
    id: 0,
    name: '',
    code: '',
    description: '',
    image: ''
  };
  nameProduct: string = "";
  url: any;

  presentation!: any[];
  article!: any[];

  dataForm!: FormGroup;

  file! : any;
  downloadURL!: Observable<string>;
  constructor(
    private dialog: MatDialog ,
    public firesStoreService : FirestorageService,
    private router: Router,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private  uploadService: UploadService,
    private storage: AngularFireStorage  
    ) {

  }

  ngOnInit(): void {

    this.buildFomr();
  }

  buildFomr(): void{
    this.dataForm = this.formBuilder.group(
      {
        name: [''],
        code: [''],
        description: [''],
        image: [''],
        presentations: this.formBuilder.array([]),
        ingredients: this.formBuilder.array([]),
      }
    )
  }


  get presentations(): FormArray {
    return this.dataForm.get('presentations') as FormArray;
  }



  get ingredients(): FormArray {
    return this.dataForm.get('ingredients') as FormArray;
  }

  addpresentationForm(data: any): void {
    const form = this.formBuilder.group({
      presentation_unit_id: [data.presentation_unit_id],
      unit_price_sale: [data.unit_price_sale],
      unit_cost_production: [data.unit_cost_production],
    });
    this.presentations.push(form);
  }


  addingredientForm(data: any): void {

    const form = this.formBuilder.group({
      quantity: [data.quantity],
      material_id: [data.article_id],
    });

    this.ingredients.push(form);
  }

  getSecondDataForm(event:any[]): void {
    const firstData = event;
    this.presentation = event;
    console.log(event);
  }



  getTreeDataForm( event:any[]){

    this.article = event;
    const name =   "" + this.product.code;
    console.log(this.dataGeneral.file);
    if(this.dataGeneral.file === undefined ){
      this.saveWhitoutImage();
    }else{
      this.storeImage(this.dataGeneral.file,name);
      this.file = this.dataGeneral.file;
    }

  }





  getFirstDataForm(event: FormGroup): void {
    const firstData = event.value;
    this.product.name = event.value.name ;
    this.product.description = event.value.description;
    this.product.image = this.url;
    this.product.code = event.value.codigo;
    this.nameProduct = firstData.name;
  }


  openUAddEditComponentU(name: string,obj:any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = name;
    dialogConfig.data = {name, obj};
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddeditUnitComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result) =>{
        if(result.event !== "close"){
        }
      }
    )
  }





  storeImage(file:any , name:string){
    const path = "Productos" +   this.product.code;
    const fileRef = this.storage.ref(path);
    this.storage.upload(path,file).snapshotChanges().pipe(
      finalize(()=>{
       this.downloadURL=fileRef.getDownloadURL();
       this.downloadURL.subscribe(res=>{
     
     this.product.image = res + "";


    this.dataForm.patchValue(
      {
        name: this.product.name,
        code: this.product.code,
        description: this.product.description,
        image: this.product.image,
      });

      this.presentation.forEach(element => {
        this.addpresentationForm(element)
      });

      this.article.forEach(element => {
        this.addingredientForm(element);
      })
    const data = this.dataForm.value;

    this.productService.addProduct(data).subscribe(
      (valor:any)=>{
       this.router.navigate(['/producer/product']);
      },
      (error)=>{
        console.log(error);
      }
    )
   
  })
})
).subscribe();

  }

  alertData(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de volver atras?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'No, continuar',
      cancelButtonText: 'Si, volver',
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire({
          title: 'Continua configurando',
          text: "No te preocupes, tu datos estan a salvo",
          icon: 'success',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 1800
        });
      } else {

        if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          this.router.navigate(['/producer/product/']);
        //   this.storageService.remove(AuthConstants.DATA_IN_NOTE);
         // this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
        }
      }
    })
  }

  saveWhitoutImage(){
    this.product.image =   this.dataGeneral.url ;

    this.dataForm.patchValue(
      {
        name: this.product.name,
        code: this.product.code,
        description: this.product.description,
        image: this.product.image,
      });

      this.presentation.forEach(element => {
        this.addpresentationForm(element)
      });

      this.article.forEach(element => {
        this.addingredientForm(element);
      })
    const data = this.dataForm.value;


    this.productService.addProduct(data).subscribe(
      (valor:any)=>{
        this.router.navigate(['/producer/product']);
      },
      (error)=>{
        console.log(error);
      }
    )
  }


}
