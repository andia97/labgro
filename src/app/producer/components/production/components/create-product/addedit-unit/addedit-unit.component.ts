import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PresentationProducer } from '@core/models/presentation-producer';
import { Presentation } from '@core/models/presentation.model';
import { PresentationService } from '@core/services/presentation/presentation.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-addedit-unit',
  templateUrl: './addedit-unit.component.html',
  styleUrls: ['./addedit-unit.component.css']
})
export class AddeditUnitComponent implements OnInit {

  public formUnit!: FormGroup;
  notFoud!: boolean;
  error!: boolean;
  presntations !: PresentationProducer;

  $filterOptions!: Observable<PresentationProducer[]>;
  options: PresentationProducer[] = [];


  option!: "";
  get nameData():any{
    return this.formUnit.get('name');
  }

  canRender = false;

  constructor(
    public dialogRef: MatDialogRef<AddeditUnitComponent>,
    public formBuilder:  FormBuilder,
    public presentationService : PresentationService,

    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }


  ngOnInit(): void {
   console.log(this.data)
   this.buildForm();
   this.getPresntations();
   if(this.data.name === "Modificar"){
     console.log(this.data.obj)
      this.fillIn();
   }

  }

  public errorsMessages = {
    articleData: [
      { type: 'required', message: 'Selecciona un articulo de la lista' },
    ],
    quantity: [
      { type: 'required', message: 'La cantidad es requerido' },
      { type: 'min', message: 'Debe ingresar numeros mayores a 0'},
      { type: 'max', message: 'La cantidad supera el stock'},
      { type: 'pattern', message: 'Debe ingresar como máximo 2 decimales'},
    ],
  }



  fillIn(){
   // const category = {} as Category;
   const presntacion = {} as PresentationProducer;
   console.log(this.data.obj)
   this.formUnit.patchValue(
     {
      presentation : this.data.obj.presntation,
      unit_cost_production: this.data.obj.unit_cost_production,
      unit_price_sale: this.data.obj.unit_price_sale,
      unit_id : this.data.obj.presentation_unit_id
     }
   )

   console.log(this.formUnit.value)
  }

  deleteArticle(name:string){
  }



  buildForm() {
   this.formUnit = this.formBuilder.group({
      presentation : [, [
        Validators.required,
      ]],
      unit_cost_production : ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.min(0)
      ]],
      unit_price_sale : ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(7),
        Validators.min(0)
      ]]
    });

  }


  onSubmit(){

    if (typeof  this.formUnit.get('presentation')?.value  === 'object'){
        console.log("obj");
        console.log( this.formUnit.get('presentation')?.value)
    }
    this.dialogRef.close({event:this.data.name,data:this.formUnit.value});
  }


  onClosed(){
    this.dialogRef.close({event:'close',data:this.formUnit.value})
  }


  getPresntations(){
    this.presentationService.getPresentations().subscribe(
      (value)=>{
        this.options = value.presentations;

      }
    )
  }

  addPresntation(namepresent:string){
    var  presntations:  PresentationProducer = {
      id: this.options.length + 1,
      name: namepresent,
      unit_cost_production: 0,
      unit_price_sale: 0
    };

    this.options.push(presntations)

  }


  getUnit(presntation: any): void {
    this.formUnit.controls.unit_id.setValue(presntation.id);
    this.formUnit.controls.presentation.setValue(presntation);
    console.log(this.formUnit.get('unit_id')?.value);
    console.log(presntation);
 }


  displayFn(presentation: PresentationProducer): string {
    return presentation && presentation.name ? presentation.name : '';
  }



  createUnit(data: any){
    this.presentationService.createPresentation(data)

  }

  public inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9 ]+$/;
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g,"");
      }
  }

  compareCategoryObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id == object2.id;
  }

}
