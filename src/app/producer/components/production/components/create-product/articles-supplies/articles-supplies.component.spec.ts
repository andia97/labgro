import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesSuppliesComponent } from './articles-supplies.component';

describe('ArticlesSuppliesComponent', () => {
  let component: ArticlesSuppliesComponent;
  let fixture: ComponentFixture<ArticlesSuppliesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesSuppliesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesSuppliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
