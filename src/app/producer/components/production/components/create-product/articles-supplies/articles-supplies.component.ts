import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ArticleProducer } from '@core/models/article-producer';
import Swal from 'sweetalert2';
import { AddEditArticleComponent } from '../add-edit-article/add-edit-article.component';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Component({
  selector: 'app-articles-supplies',
  templateUrl: './articles-supplies.component.html',
  styleUrls: ['./articles-supplies.component.css']
})
export class ArticlesSuppliesComponent implements OnInit {

  @Input()
  nameProduct: string = '';

  @Input() productId!: number;

  @Output() emitterDataForm: EventEmitter<ArticleProducer[]> = new EventEmitter();

  data: ArticleProducer[] =[];
  displayedColumns: string[] = ['Unidad/Presentacion', 'CostoUnitario', 'PrecioUnitario', 'Edit'];

  dataSource = new MatTableDataSource<ArticleProducer>(this.data);
  dataForm!: FormGroup;

  clicked: number = 0;
  constructor(
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
    private storage: AngularFireStorage
  ) {

  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      unit: this.formBuilder.array([])
    });
  }


  get unitData(): FormArray {
    return this.dataForm.get('unit') as FormArray;
  }



  openUAddEditComponentU(name: string,obj:any,index:string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.id = this.nameProduct;
    dialogConfig.data = {name, obj };
    dialogConfig.panelClass = 'custom-dialog-container';
    dialogConfig.width = '450px'
    const dialogRef = this.dialog.open(AddEditArticleComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(
      (result : any ) =>{
        if(result.event === "Crear"){
          console.log(result.data)
           this.addUnit(result.data);
        }
        if(result.event === "Modificar"){
          console.log(result.data)
          this.editArticle(result.data,+index);
        }
        if(result.event === "Eliminar"){

        }
      }
    )
  }
  ddArticle(): void {

  }


  deleteUnit(unit:any, index: number): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la unidad de presentacion  : ' +unit.name+ ' ?',
      text: "Se perderan los datos que configuró anteriormente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.unitData.removeAt(index)
        this.data = this.unitData.value;
        swalWithBootstrapButtons.fire({
          title: '¡Eliminado!',
          text: "El unidad de presentacion seleccionado ya no pertenece a tu lista de entradas",
          icon: 'success',
          showConfirmButton: false,
          timer: 1800
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu artículo esta a salvo",
          icon: 'error',
          showConfirmButton: false,
          timer: 1800
        });
      }
    })
  }


  addUnit(data: any){
    const form = this.formBuilder.group({
      arti: [data.article],
      article: [data.article.name_article],
      quantity: [data.quantity],
      unit_measure: [data.article.unit_measure],
      article_id : [data.article_id]
    });
    console.log(form)

    this.unitData.push(form);
    console.log(this.unitData);
    this.data = this.unitData.value;
  }

  editArticle(data: any,index:number) {
    console.log(data)
    const form = this.formBuilder.group({
      arti: [data.article],
      article: [data.article.name_article],
      quantity: [data.quantity],
      unit_measure: [data.article.unit_measure],
      article_id : [data.article_id]
    });
    this.unitData.removeAt(index)
    this.unitData.push(form);
    this.data = this.unitData.value;

  }

  onSubmmit(){
      if (this.clicked === 1) {

        this.emitterDataForm.emit(this.data);
        //this.router.navigate(['/producer/product']);
      } else {
        this.clicked = this.clicked + 1;
        // console.log(this.dataForm.value);
        Swal.fire({
          position: 'center',
          icon: 'warning',
          // iconColor: '#85CE36',
          title: 'Verifique que los datos sean correctos, una vez guardados no podra modificar por temas de auditoria',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
          // timer: 2000
        })
      }

  }
}
