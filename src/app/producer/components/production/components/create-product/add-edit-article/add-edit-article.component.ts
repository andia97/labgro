import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '@core/models/article.model';
import { ArticleService } from '@core/services/article/article.service';
import { MaterialService } from '@core/services/material/material.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-edit-article',
  templateUrl: './add-edit-article.component.html',
  styleUrls: ['./add-edit-article.component.css']
})
export class AddEditArticleComponent implements OnInit {
  options: any[] = [];
  canRender = false;

  articleSelected!:Article;

  dataArticleForm!: FormGroup;
  onlyNumber = '^[0-9]+$';
  numRegex = /^-?\d*[.,]?\d{0,2}$/;
  
  articleMeasure!: string;
  name!: string ;

  state!: boolean;
  filteredOptions!: Observable<Article[]>;
  idProduct!: number;
  
  dialog: any;

  materials: any[] = [];

  get articleData():any {
    return this.dataArticleForm.get('article');
  }

  get quantity():any {
    return this.dataArticleForm.get('quantity');
  }
  
  constructor(
    private dialogRef: MatDialogRef<AddEditArticleComponent>,
    private articleService: ArticleService,
    private materialService: MaterialService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { 
    this.buildForm();
    this.idProduct=data.data;
  }

  buildForm(): void {
    this.dataArticleForm = this.formBuilder.group({
      article: [ , 
        [Validators.required]
      ],
      article_id: [ ,
      [Validators.required]
      ],
      quantity: ['', 
        [
          Validators.required,
          
        ]
      ],
      unit_measure: ['']
    });
  }

  ngOnInit(): void {
    //this.getArticles();
    this.getMaterials();
    console.log(this.dialogRef.id)
    this.name = this.dialogRef.id + "";
    if(this.data.name === 'Modificar'){
      this.fillIn();
    }
  }
  
  
  fillIn(){
    this.dataArticleForm .patchValue(
      {
       article : this.data.obj.arti,
       quantity: this.data.obj.quantity ,
       unit_measure: this.data.obj.unit_measure,
       article_id:  this.data.obj.article_id
      }
    )
   }

/*
  getArticles(): void {
    this.state = true;
    this.articleService.getArticles().subscribe(
      (res: Article[]) => {
        this.state = false;
        this.canRender=true;
        this.options = res;
        this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name_article),
          map(name_article => name_article ? this._filter(name_article) : this.options.slice())
        );
      },
      (error: any) => {
        alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
      },
    )
  }*/
 

  getMaterials(){
    this.materialService.getMaterials().subscribe(
      (value:any)=>{
        console.log(value);
        value.raw_material.forEach(
         (value) =>{  
           this.options.push(value)
        }
        );

        value.supplies.forEach(
         (value)=> {
           this.options.push(value);
         }
        );
         this.filteredOptions = this.dataArticleForm.controls.article.valueChanges
         .pipe(
           startWith(''),
           map(value => typeof value === 'string' ? value : value.name_article),
           map(name_article => name_article ? this._filter(name_article) : this.options.slice())
         );
        

      },
      (error: any) => {
        alert('Ocurrio un error al cargar la lista de articulos de nuestra base de datos, contactese con administración');
      },
    );

    
  }
  
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      (option) => {
        const value = option.name_article.toLowerCase().includes(filterValue);
        if (!value) {
          this.dataArticleForm.controls.article_id.reset();
        }
        return value;
      }
    );
  }

  getArticle(article: Article): void {
    this.dataArticleForm.controls.article_id.setValue(article.id); 
  //  this.dataArticleForm.controls.article.setValue(article);
    this.articleSelected = article;
    this.articleMeasure = article.unit_measure;
    this.dataArticleForm.controls.quantity.setValidators(
      [
        Validators.max(8888888), 
        Validators.required
      ])
  }
  


  onSubmit(){
    console.log(this.dataArticleForm.value)
    this.perfomOperation()
    this.dialogRef.close({event:this.data.name,data:this.dataArticleForm.value});
  }

  onClosed(){
    this.dialogRef.close({event:'close' ,data:this.dataArticleForm.value})
  }

  saveData(): void {
   
  }

  displayFn(article: Article): string {
    return article && article.name_article ? article.name_article : '';
  }

  
  public inputValidator(event: any) {
    const pattern = /^[0-9-+./*()]+$/;   
    console.log(event.target.value);
    if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^0-9-+/.*()]/g,"");
      }
  }
  
  perfomOperation(){
    console.log(this.dataArticleForm.get('quantity')?.value);
    
    var operation = this.dataArticleForm.get('quantity')?.value;
    var numberInput = operation.split('');
    var numer = '';
    var numeros: any[] = [] ;
    var oprations: any[] = []; 
    console.log(numberInput) 
    for (let i of  numberInput){
           if(i === '-' || i === '+' ||
             i === '/' || i === '*'){
            numeros.push(numer);
            numeros.push(i);
            oprations.push(i)
            numer = ' ';

          }else{
            console.log(i);
            numer = numer + i;
          } 
      }
   
    numeros.push(numer);
    console.log(numeros);
    if (oprations.length !== 0){
      this.operation(numeros, oprations);
    }else{
      this.dataArticleForm .patchValue(
        {
         quantity: operation,
        }
      )
    }
  }
   
  operation(number: any[], operations :any[]){
    console.log(eval(number.join(' ')));
    var result = eval(number.join(' '));
    console.log(result);
    this.dataArticleForm .patchValue(
      {
       quantity: result,
      }
    )
  }

  


}
