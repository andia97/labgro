import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable} from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { filter, map, shareReplay } from 'rxjs/operators';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { ProductService } from '@core/services/product/product.service';

import * as _moment from 'moment';
import { Product } from '@core/models/product.model';
import Swal from 'sweetalert2';

const moment = _moment;
moment.locale('es');

@Component({
  selector: 'app-product-layout',
  templateUrl: './product-layout.component.html',
  styleUrls: ['./product-layout.component.css']
})
export class ProductLayoutComponent implements OnInit {


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  month: number;
  year: number;
  value: string;

  state: boolean = false;

  productList!: Product[];
  dataSource: any;

  emptyState: boolean = false;
  dataState: boolean = false;

  actualDate = moment();

  @ViewChild('paginator') paginator!: MatPaginator;
  pageEvent!: PageEvent;
  currentPage: number = 0;
  lastPage: any;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private producService: ProductService,
    ) {
    const actualMonth = this.actualDate.format('MMMM');
    const actualYear = this.actualDate.format('YYYY');
    this.year = + actualYear;
    this.month = this.convertMonth(actualMonth);
    this.value = "";
  }
  ngOnInit(): void {
    this.getProducts('','');
  }

  getProducts(year,month): void{
    this.state = true;
    this.producService.getProducts(this.value, month, year,  this.currentPage + 1)
      .subscribe(
        (res: any)=>{
          this.state = false;
          this.dataSource = res;
          this.productList=res;
          if(this.productList.length === 0){
            this.emptyState = true;
            this.dataState = false;
          }else{
            this.emptyState = false;
            this.dataState = true;
          }
        },
        (error: any) => {
          this.state = false;
          this.dataSource = [];
          this.productList=[];
          if(error==="No se encontraron resultados"){
            Swal.fire({
              position: 'center',
              icon: 'warning',
              title: 'No existen productos creados',
              showConfirmButton: true,
              confirmButtonColor: '#85CE36',
            });
          }else{
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Ocurrio un error inesperado, contactese con administración',
              showConfirmButton: true,
              confirmButtonColor: '#85CE36',
            });
          }

        }
      )
  }

  filterDate(event: number, option: string): void {
    if (option == 'month') {
      this.month = event;
    } else {
      this.year = + event;
    }
    // this.currentPage = 0;
    // this.paginator.pageIndex = 0;
    // console.log(this.month, this.year);
    this.getProducts(this.year,this.month);
  }

  convertMonth(actualMonth: string): number {
    let month;
    switch (actualMonth) {
      case 'enero':
        month = 1;
        break;
      case 'febrero':
        month = 2;
        break;
      case 'marzo':
        month = 3;
        break;
      case 'abril':
        month = 4;
        break;
      case 'mayo':
        month = 5;
        break;
      case 'junio':
        month = 6;
        break;
      case 'julio':
        month = 7;
        break;
      case 'agosto':
        month = 8;
        break;
      case 'septiembre':
        month = 9;
        break;
      case 'octubre':
        month = 10;
        break;
      case 'noviembre':
        month = 11;
        break;
      case 'diciembre':
        month = 12;
        break;
        default:
        month = 0;
      break;
    }
    return month;
  }

  onPaginatorChanges(event: PageEvent): void {
    // console.log(event);
    let page = event.pageIndex;
    this.currentPage = event.pageIndex;
    page = page + 1;
    // this.state = true;
    this.getProducts(this.year,this.month);
  }
}
