import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';


import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { ProductService } from '@core/services/product/product.service';
import { Product } from '@core/models/product.model';
import { UploadService } from '@core/services/firebase/upload.service';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
@Component({
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  dataGeneralForm!: FormGroup;
  name!:string;
  code!: string;
  description!: string;
  image!: string;
  imageChanged = false;
  downloadURL!: Observable<string>;
  state: boolean = false;
  // downloadURL!: Observable<string>;
  // uploadPercent!: Observable<any>;
  File!: any;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditProductComponent>,
    private productService: ProductService,
    private uploadService: UploadService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private storage: AngularFireStorage
  ) {
    this.buildForm();

  }

  private buildForm(): void {
    this.dataGeneralForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.min(1), Validators.maxLength(40)]],
      code: ['', [Validators.required, Validators.maxLength(50)]],
      description: [ '', [Validators.maxLength(99999)]],
    });
  }

  ngOnInit(): void {
    const dataForm = this.data.data;
    this.name=dataForm.name;
    this.code=dataForm.code;
    this.description=dataForm.description;
    this.image = dataForm.image;
    this.dataGeneralForm.patchValue(dataForm);
  }

  valueChange(newValue, field: string) {
    switch (field) {
      case 'name':
        newValue.length < 24 ? this.name=newValue:"";
        break;
      case 'code':
        this.code=newValue;
        break;
      case 'img':

        const reader = new FileReader();
        this.File=newValue;
        if(newValue.target.files && newValue.target.files.length && newValue.target.files[0].type!="application/pdf") {
          const [file] = newValue.target.files;
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.image = reader.result as string;
          };
        }
        break;
      default:
        break;
    }
  }


  saveData(): void {
    this.state = true;

    if(this.image != this.data.data.image){
      this.dialogRef.close();

      const file = this.File.target.files[0];
      const {data:prod} = this.data;
      const filePath = "Productos/" + this.code;
      const fileRef = this.storage.ref(filePath);
      this.storage.upload(filePath,file).snapshotChanges().pipe(
        finalize(()=>{
         this.downloadURL=fileRef.getDownloadURL();
         this.downloadURL.subscribe(res=>{
           this.editProduct(res);
         })
        })
        ).subscribe();
      }else{
      this.dialogRef.close();
      this.data.var=1;
      this.editProduct(this.data.data.img);
      }
    }

  editProduct(urlImg) {


    const {data:prod} = this.data;
    const item: Product = {
      id: prod.id,
      name: this.dataGeneralForm.value.name,
      code: this.dataGeneralForm.value.code,
      description: this.dataGeneralForm.value.description,
      image: urlImg,
    };

    this.productService.updateProduct(item)
    .subscribe(
      (res: any)=>{
        this.state = false;
        Swal.fire({
          title: 'Editado correctamente!',
          text: 'Los datos de la materia prima/insumo se guardaron correctamente',
          icon: 'success',
          timer: 2000,
          showConfirmButton: false,
        });
        this.data.event.emit();
      },(error: any)=>{
        this.state = false;
        Swal.fire({
          title: '¡Error!',
          text: 'Ocurrio un problema inseperado, contactese con administración',
          icon: 'error',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        });
      }
    )
  }
}
