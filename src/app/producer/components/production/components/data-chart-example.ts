export var single = [
  {
    "name": "Ene",
    "value": 894
  },
  {
    "name": "Feb",
    "value": 500
  },
  {
    "name": "Mar",
    "value": 720
  },
  {
    "name": "Abr",
    "value": 856
  },
  {
    "name": "May",
    "value": 235
  },
  {
    "name": "Jun",
    "value": 963
  },
  {
    "name": "Jul",
    "value": 752
  },
  {
    "name": "Ago",
    "value": 552
  },
  {
    "name": "Sep",
    "value": 321
  },
  {
    "name": "Oct",
    "value": 568
  },
  {
    "name": "Nov",
    "value": 0
  },
  {
    "name": "Dic",
    "value": 853
  },
];

export var multi = [
  {
    "name": "Germany",
    "series": [
      {
        "name": "2010",
        "value": 7300000
      },
      {
        "name": "2011",
        "value": 8940000
      }
    ]
  },

  {
    "name": "USA",
    "series": [
      {
        "name": "2010",
        "value": 7870000
      },
      {
        "name": "2011",
        "value": 8270000
      }
    ]
  },

  {
    "name": "France",
    "series": [
      {
        "name": "2010",
        "value": 5000002
      },
      {
        "name": "2011",
        "value": 5800000
      }
    ]
  }
];
