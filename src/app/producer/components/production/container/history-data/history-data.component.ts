import {Component, Input, OnInit} from '@angular/core';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-history-data',
  templateUrl: './history-data.component.html',
  styleUrls: ['./history-data.component.css']
})
export class HistoryDataComponent implements OnInit {

  @Input() productionsData!: any;

  displayedColumns: string[] = ['date', 'unit', 'quantity' ];
  dataSource!: any[];
  constructor() { }

  ngOnInit(): void {
  }

}
