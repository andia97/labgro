import {Component, Input, OnInit, EventEmitter, Output, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditProductComponent } from '../../components/edit-product/edit-product.component';
import { Product } from '@core/models/product.model';
import { ProductService } from '@core/services/product/product.service';

import Swal from 'sweetalert2';
import {CalendarProductionContainer} from "@producer/components/production-detail/container/calendar-production/calendar-production.container";
@Component({
  selector: 'app-product',
  templateUrl: './product.container.html',
  styleUrls: ['./product.container.css']
})
export class ProductContainer implements OnInit {
  @Input() product!: Product | any;
  @Output() emitProductEventUpdate: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
    private ProductService:ProductService,
  ) { }

  ngOnInit(): void {
  }

  editNote(): void {
    const dialogRef = this.dialog.open(EditProductComponent, {
      width: '1000px',
      height: '500px',
      panelClass: 'custom-dialog-container',
      data: {
        data: this.product,
        event: this.emitProductEventUpdate,
      }
    });

  }

  deleteNote(): void {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn-swat btn-success',
        cancelButton: 'btn-swat btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el producto: ' + this.product.name + ' ?',
      text: "Una vez eliminado no podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {

        this.ProductService.deleteProduct(this.product.id).subscribe(
          (res: any) => {
            swalWithBootstrapButtons.fire({
              title: '¡Eliminado!',
              text: "Tu producto se eliminó correctamente",
              icon: 'success',
              // iconColor: '#FE821D',
              showConfirmButton: false,
              timer: 2000
            });
            this.emitProductEventUpdate.emit();
        },
        (error: any) => {
          swalWithBootstrapButtons.fire({
            title: '¡Oops!',
            text: "Tu sub proceso no se eliminó correctamente, intenta nuevamente",
            icon: 'error',
            // iconColor: '#FE821D',
            showConfirmButton: false,
            timer: 1800
          });

        }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: '¡Cancelado!',
          text: "No te preocupes, tu producto esta a salvo",
          icon: 'error',
          // iconColor: '#FE821D',
          showConfirmButton: false,
          timer: 2000
        });
      }
    })
  }
}
