import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { ProductionService } from '@core/services/production/production.service';
import { Moment } from 'moment';
import Swal from "sweetalert2";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-production-summary',
  templateUrl: './production-summary.component.html',
  styleUrls: ['./production-summary.component.css']
})
export class ProductionSummaryComponent implements OnInit {

  date: Date = new Date();
  dateLabel: Date = new Date();
  firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
  startDate: FormControl = new FormControl(
    {
      value: this.firstDay,
      disabled: true
    }
  );
  endDate: FormControl = new FormControl(
    {
      value: this.lastDay,
      disabled: true
    }
  );

  isBoxClicked: boolean = false;
  isCodeClicked: boolean = false;

  boxForm: FormControl = new FormControl('1');
  codeForm: FormControl = new FormControl('Agr-prod-tec-12-619');

  displayedColumns: string[] = [
    'nro',
    'code',
    'product',
    'unit',
    'cost',
    'quantity',
    'amount',
    'second_quantity',
    'second_amount',
    'third_quantity',
    'third_amount',
  ];
  dataSource!: any[];

  constructor(
    private productionService: ProductionService,
  ) {

  }

  ngOnInit(): void {
    const firstDate = `${this.startDate.value.getFullYear()}-${this.startDate.value.getMonth()+1}-${this.startDate.value.getDate()}`;
    const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
    this.fetchProductionSummary(firstDate,endDate);

  }

  fetchProductionSummary(firstDate, endDate){
    this.productionService.getProductionSummary(firstDate, endDate)
      .subscribe(
        (res: any)=>{
          const arrAux: any[]=[];
          res.map((elem,i)=>{
            let newElement: any={...elem,'nro':1+i};
            arrAux.push(newElement)
          })
          this.dataSource=arrAux;
        },(err)=>{
          console.error(err)
        }
      )
  }

  dateChanged(typeOfDate,date: any): void{
    if(typeOfDate===1){
      this.startDate.setValue(date.value);
      if(Date.parse(date.value)<Date.parse(this.endDate.value)){
        const firstDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        this.fetchProductionSummary(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha menor a la FECHA FIN',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }

    }
    if(typeOfDate===2){
      this.endDate.setValue(date.value);
      if(Date.parse(this.startDate.value)<Date.parse(date.value)){
        const firstDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        const endDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        this.fetchProductionSummary(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha mayor a la FECHA INICIO',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }
    }
  }

  isClicked(option: string): void {
    switch (option) {
      case 'box':
        if (this.isBoxClicked) {
          this.isBoxClicked = false;
        } else {
          this.isBoxClicked = true;
        }
        break;
      case 'code':
        if (this.isCodeClicked) {
          this.isCodeClicked = false;
        } else {
          this.isCodeClicked = true;
        }
        break;
      default:
        console.log('Dont nothing');
        break;
    }

  }

  calculateBackground(i: number): string {
    if (i % 2 === 0) {
      return '#D7DDE4'
    } else {
      return '#ffffff'
    }
  }

  public downloadPDF(): void {
   
    const doc = new jsPDF('L', 'mm', 'A4');
    const DATA : any = document.getElementById('tbl-sumary'); 
    console.log(DATA);
  
    const options = {
      background: 'white',
      scale: 3
    }

    html2canvas(DATA, options).then(
      (canvas) => {
        
      const img = canvas.toDataURL('image/PNG');

     
      const bufferX = 15;
      const bufferY = 1;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth + 4) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }
}
