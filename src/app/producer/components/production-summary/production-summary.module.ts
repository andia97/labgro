import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionSummaryRoutingModule } from './production-summary-routing.module';
import { ProductionSummaryComponent } from './production-summary.component';

import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProductionSummaryComponent
  ],
  imports: [
    CommonModule,
    ProductionSummaryRoutingModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ProductionSummaryModule { }
