import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { StorageService } from '@core/services/storage/storage.service';
import { AuthConstants } from '@core/config/auth-constanst';
import { MatSidenav } from '@angular/material/sidenav';
import { SideNavService } from '@shared/services/side-nav.service';

@Component({
  selector: 'app-layout-producer',
  templateUrl: './layout-producer.component.html',
  styleUrls: ['./layout-producer.component.css']
})
export class LayoutProducerComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  @ViewChild('sidenav') public sidenav!: MatSidenav;

  rolUser: string = 'Lacteos';
  title!: string;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private sideNavService: SideNavService,
    private storageService: StorageService,
  ) {
    this.storageService.get(AuthConstants.TITLE_NAV)
    .then(
      (res: string) => {
          this.title = res;
      }
    )
    .catch(
      (error: any) => {
          this.title = 'Inicio';
      }
    )
    
  }

  ngAfterContentInit(): void {
    setTimeout(
      ()=> {
        this.sideNavService.sideNavToggleSubject.subscribe(()=> {
          this.sidenav.open();
        });
      }, 0);
  }


  changeTitle(event): void {
    this.title = event; 
  }
}
