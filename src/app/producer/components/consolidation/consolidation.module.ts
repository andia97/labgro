import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsolidationRoutingModule } from './consolidation-routing.module';
import { ConsolidationComponent } from './consolidation.component';
import { SharedModule } from '@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@material/material.module';


@NgModule({
  declarations: [
    ConsolidationComponent
  ],
  imports: [
    CommonModule,
    ConsolidationRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ]
})
export class ConsolidationModule { }
