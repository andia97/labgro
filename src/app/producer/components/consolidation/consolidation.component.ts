import { Component, OnInit,  LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common'
import localeES from '@angular/common/locales/es'
import { FormControl } from '@angular/forms';

import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
const moment = _rollupMoment || _moment;

import {ProductionService} from "@core/services/production/production.service";
import {MatDatepicker} from '@angular/material/datepicker';
import Swal from "sweetalert2";

registerLocaleData(localeES, 'es');

@Component({
  selector: 'app-consolidation',
  templateUrl: './consolidation.component.html',
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ],
  styleUrls: ['./consolidation.component.css']
})
export class ConsolidationComponent implements OnInit {
  date: Date = new Date();
  dateLabel: Date = new Date();
  firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  lastDay = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
  startDate: FormControl = new FormControl(
    {
      value: this.firstDay,
      disabled: true
    }
  );
  endDate: FormControl = new FormControl(
    {
      value: this.lastDay,
      disabled: true
    }
  );


  isBoxClicked: boolean = false;
  isCodeClicked: boolean = false;

  boxForm: FormControl = new FormControl('1');
  codeForm: FormControl = new FormControl('Agr-prod-tec-12-619');

  displayedColumns: string[] = [
    'nro',
    'code',
    'product',
    'unit',
    'cost',
    'sale',
    'quantity',
    'amount',
    'second_quantity',
    'second_amount',
    'third_quantity',
    'third_amount',
    'utility',
  ];
  dataSource!: any[];

  constructor(
    private productionService: ProductionService,
  ) {

  }

  ngOnInit(): void {
    const firstDate = `${this.startDate.value.getFullYear()}-${this.startDate.value.getMonth()+1}-${this.startDate.value.getDate()}`;
    const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
    this.fetchConsolidationTable(firstDate,endDate);
  }

  fetchConsolidationTable(firstDate, endDate){
    this.productionService.getReportConsolidation(firstDate, endDate)
      .subscribe(
        (res: any)=>{
          const arrAux: any[]=[];
          res.map((elem,i)=>{
            let newElement: any={...elem,'nro':1+i};
            arrAux.push(newElement)
          })
          this.dataSource=arrAux;
        },(err)=>{
          console.error(err)
        }
      )
  }

  dateChanged(typeOfDate,date: any): void{
    if(typeOfDate===1){
      this.startDate.setValue(date.value);
      if(Date.parse(date.value)<Date.parse(this.endDate.value)){
        const firstDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        const endDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        this.fetchConsolidationTable(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha menor a la FECHA FIN',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }

    }
    if(typeOfDate===2){
      this.endDate.setValue(date.value);
      if(Date.parse(this.startDate.value)<Date.parse(date.value)){
        const firstDate = `${this.endDate.value.getFullYear()}-${this.endDate.value.getMonth()+1}-${this.endDate.value.getDate()}`;
        const endDate = `${date.value.getFullYear()}-${date.value.getMonth()+1}-${date.value.getDate()}`;
        this.fetchConsolidationTable(firstDate,endDate);
      }else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: 'Seleccione una fecha mayor a la FECHA INICIO',
          showConfirmButton: true,
          timer: 3000,
          confirmButtonColor: '#85CE36',
        });
      }
    }
  }


  isClicked(option: string): void {
    switch (option) {
      case 'box':
        if (this.isBoxClicked) {
          this.isBoxClicked = false;
        } else {
          this.isBoxClicked = true;
        }
        break;
      case 'code':
        if (this.isCodeClicked) {
          this.isCodeClicked = false;
        } else {
          this.isCodeClicked = true;
        }
        break;
      default:
        console.log('Dont nothing');
        break;
    }

  }

  calculateBackground(i: number): string {
    if (i % 2 === 0) {
      return '#D7DDE4'
    } else {
      return '#ffffff'
    }
  }
}
