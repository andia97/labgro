import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesSummaryRoutingModule } from './sales-summary-routing.module';
import { SalesSummaryComponent } from './sales-summary.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SalesSummaryComponent
  ],
  imports: [
    CommonModule,
    SalesSummaryRoutingModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class SalesSummaryModule { }
