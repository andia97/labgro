import { Article } from "./article.model";
import { PresentationProducer } from "./presentation-producer";

export interface PresentationUnitProduct {
  id: number,
  name: string;
  unit_cost_production: number;
  unit_price_sale: number;
  presetation_unit_id: number;
  presentation: PresentationProducer;
}
