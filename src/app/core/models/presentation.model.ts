export interface Presentation {
  id: number,
  name: string;
  quantity: number;
  stock_min: number;
}