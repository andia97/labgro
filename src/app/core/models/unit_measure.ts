export interface UnitMeasure {
    id : number,
    unit_measure :  String,
    kind: string
}