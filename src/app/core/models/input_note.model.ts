export interface InputNote {
  id: number;
  receipt: number;
  provider: string;
  total: number;
  user_id: number;
  created_at: string;
  order_number: string;
}