export interface ArticleArea{
    delivery_date: string;
    name_article: string;
    quantity: number;
    unit_measure: string;
    name: string;
}