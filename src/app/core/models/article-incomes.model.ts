import { Article } from "./article.model";

export interface ArticleIncome {
  id: number,
  article_id: number,
  article: Article,
  quantity: number,
  unit_price: number,
  total_price: number
}
