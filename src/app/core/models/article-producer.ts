export interface ArticleProducer {
  id: number;
  article_name: string;
  quantity: number;
  unit_measure: string;
}
