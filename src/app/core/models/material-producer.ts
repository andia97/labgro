export interface MaterialProducer {
  id: number;
  name_article: string;
  cod_article: string;
  initial_stock: number;
  minimal_stock: number;
  color: string;
  stock_total: number;
  unit_measure: string;
}