import {Rol} from '@core/models/rol.mudel'

export class User{
   id!: number;
   name!: string;
   email!: string;
   password!: string;
   rol!: Rol;
   rol_id!: number;
   image!: string;
    constructor(){
        this.email = "";
        this.password = "";
    }

}