export interface PresentationProducer {
  id: number;
  name: string;
  unit_cost_production: number;
  unit_price_sale: number;
}