export interface OutputNote {
  id: number;
  section_id: number;
  receipt: number;
  order_number: number;
  delivery_date: string;
  order_date: string;
  created_at: string;
  updated_at: string;
  name: string;
  total: string;
}