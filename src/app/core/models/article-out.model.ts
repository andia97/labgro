import { Article } from "./article.model";

export interface ArticleOut {
  id: number,
  article_id: number,
  article: Article,
  quantity: number,
  budget_output: number,
  total_price: number
}
