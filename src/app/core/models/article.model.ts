export interface Article{
    id: number;
    cod_article: string;
    name_article: string;
    stock: number;
    unit_id: number;
    category_id: number;
    created_at: string; 
    name: string;
    unit_price: number;
    unit_measure: string;
    kind: string;
    stock_min: number;
    is_low: boolean;
}

