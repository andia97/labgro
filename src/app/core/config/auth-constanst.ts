export class AuthConstants {
  public static readonly AUTH = 'authToken';
  public static readonly TITLE_NAV = 'title_nav';
  public static readonly USER = 'user';
  public static readonly ROL = 'rol';
  public static readonly DATA_IN_NOTE = 'data_in_note';
  public static readonly DATA_IN_ARTICLE = 'data_in_article';
  public static readonly PAGE = 'page';
  public static readonly SHOPPING_CART = 'articles_cart';
}
