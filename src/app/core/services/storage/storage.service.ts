import { Injectable } from '@angular/core';
import {AuthConstants} from "@core/config/auth-constanst";
import {CookieService} from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class StorageService {


  constructor(
    private cookieService: CookieService,

  ) { }

  async store(storagekey: string, value: any) {
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await localStorage.setItem(
      storagekey,
      encryptedValue
    );
  }

  async get(storageKey: string) {
    const ret: any = await localStorage.getItem(
      storageKey
    );
    return JSON.parse(unescape(atob(ret)));
  }

  async remove(storageKey: string) {
    await localStorage.removeItem(
      storageKey
    );
  }

  async clear() {
    await localStorage.clear();
  }

  getCookie(storageKey: string) {
    const ret: any = this.cookieService.get(
      storageKey
    );
    return ret;
  }
}
