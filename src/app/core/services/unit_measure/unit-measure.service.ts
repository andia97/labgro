import { Injectable } from '@angular/core';
import { UnitMeasure } from '@core/models/unit_measure';
import { environment } from '@evironments/environment';
import { map, tap } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { MessageService } from '../menssage/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UnitMeasureService {

  constructor(
    private httpService: HttpService,
    private messageService: MessageService,
    private http: HttpClient

  ) { }


  public getUnitMeasure(id: number){
   
  }


  public getUnitMeasures() {
    return this.httpService.get('units')
    .pipe(
      tap(_ => this.log('unit mesures'))
    ) ;
  }

  public addUnitMeasure (dataForm: UnitMeasure){
    return this.httpService.post('units', dataForm)
    .pipe(
      tap(_ => this.log('unit mesures'))
    );
  
  }

  public updateUnitMeasure (unit : UnitMeasure ) {
    return this.httpService.put('units', unit )
   .pipe(
     tap(_ => this.log('unit mesures'))
   );
  }

  deleteUnitMeasure(id: string) {
    const url = 'units/' + id 
    return this.httpService.delete(url)
    .pipe( 
      tap(_ => this.log('unit mesures'))
    );
  }
  
  private log(message: string) {
    this.messageService.add(`${message}`);
  }
}
