import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '@evironments/environment';

import { User } from '@core/models/user.model';
import { MessageService } from '@core/services/menssage/message.service';
import { CookieService } from 'ngx-cookie-service';

const httpOptions = { 
  headers: new HttpHeaders({
     'Content-Type': 'application/json', 
     'Access-Control-Allow-Origin': '*' }) };



@Injectable({
  providedIn: 'root'
})
export class LoginService {

  curent_user:string = 'curent_user';

  constructor(
    private cookieService:CookieService, 
    private http: HttpClient ,
    private messageService:MessageService) { 
  }


  public login(email:string, password:string ) {
    return this.http.post<any>( environment.apiUrl+"login", {email, password}).
    pipe(
     
    );
  }
  
  public save_rol(id_rol: number ){
    const id = id_rol.toString();
    console.log(id);
    try {
      localStorage.setItem( 'current_rol', id);
      
    } catch (e){
      console.log(e);
    }
  }
  

  public get_user(){
    const tokens:string = this.cookieService.get('token');
    return this.http.get<any>( environment.apiUrl+"get_user/"+tokens ).
    pipe(
      catchError(this.handleError<any>('login faild'))
    )  

   // try{
    //  let user = JSON.stringify(localStorage.getItem('curent_user'));
   //   return user;
   /// } catch(e){get_user
    ///  console.log(e);
   ///   return {};
    //} 
  }

  public get_rol(){
       let user =  localStorage.getItem( 'current_rol');
       return user;
   }

  logout(){

  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`LoginService: ${message}`);
  }
}
