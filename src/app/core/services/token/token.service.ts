import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@evironments/environment.prod';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from '@core/config/auth-constanst';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
  ) { }

  saveToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken(): string | null {
    return this.cookieService.get(AuthConstants.AUTH);
  }


}
