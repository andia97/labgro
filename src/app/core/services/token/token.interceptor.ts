import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { TokenService } from './token.service';

import { catchError, filter, switchMap, take } from 'rxjs/operators';
import {StorageService} from "@core/services/storage/storage.service";
@Injectable({ providedIn: 'root' })
export class InterceptorToken implements HttpInterceptor {
  constructor(
    private cookieService: CookieService,
    private router: Router,
    private tokenService: TokenService,
    private storageService: StorageService,

  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let re = req;
    const token: string = this.cookieService.get('authToken');
    // console.log(this.tokenService.getToken());
    // console.log(token);

    if (token) {
      re = req.clone({
        setHeaders: {
          'Access-Control-Allow-Headers': 'Origin',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
    }
    return next.handle(re)
      .pipe(catchError((error) => {
        console.warn(error)

        if(error === 'Token has expired' || error === 'Unauthenticated.'){
          this.storageService.clear();
          this.cookieService.deleteAll();
          this.router.navigate(['/']);
        }
        return throwError(error);
       }));
  }
}
