import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { MessageService } from '../menssage/message.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpService } from '../http/http.service';
import { StorageService } from '../storage/storage.service';
import { InterceptorToken } from '../token/token.interceptor';
import { Presentation } from '@core/models/presentation.model';


const httpOptions = { 
  headers: new HttpHeaders({
     'Content-Type': 'application/json', 
     'Access-Control-Allow-Origin': '*',
     'Authorization':'bearer' + localStorage.getItem("authToken"),
    }) };

@Injectable({
  providedIn: 'root'
})
export class PresentationService {
  
  headers = new Headers();
  constructor(
    private http: HttpClient,
    private httpClient: HttpClient,
  ) { }

  public createPresentation(presentation: any){
    const UserUrl = environment.apiUrl+"presentations";
    return this.http.post<Presentation>(UserUrl, JSON.stringify(presentation));
  };
  
  public editPresentation(idPresentation: number, presentation: any){
    const UserUrl = environment.apiUrl+"presentations/"+idPresentation;
    return this.http.put<Presentation>(UserUrl, JSON.stringify(presentation));
  };

  public getPresentations(): Observable<any>{
    const UserUrl = environment.apiUrl+"presentations";
    return this.httpClient.get(UserUrl);
  };

  public deletePresentation(idPresentation: number){
    const UserUrl = environment.apiUrl+"presentations/"+idPresentation;
    return this.httpClient.delete(UserUrl);
  };
}
