import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { MessageService } from '../menssage/message.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  
  apiUrl = environment.apiUrl; 

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) {

   }
   

   public getOrderss(month:number, year:number){
    let params = new HttpParams();
    params = params.append('month' , Number(month));
    params = params.append('year', Number(year));
    const url = environment.apiUrl+"orders";
    return this.httpClient.get(url , {params})
    .pipe(
      map((res: any) => res.details)
    );
  };

  public getOrders(): Observable<any[]>{
    return this.httpService.get('orders');
  };



  public getOrder(orderId): Observable<any[]>{
    return this.httpService.get('orders/'+orderId);
  };
 
  /*
  public setOrderReprobed(orderId: string, datafrom: any): Observable<any[]>{
    return this.httpService.get('order_reprobate/'+orderId, datafrom)
  }
*/ 
  
  public setOrderReprobed(orderId: string, description: any){
    let params = new HttpParams();
    params = params.append('description',String(description))
    const urlset = this.apiUrl + 'order_reprobate/' + orderId;
    return this.httpClient.get(urlset , {params})
  }

  public postOrder(material: any): Observable<any>{
    const UserUrl=environment.apiUrl+"orders";
    return this.http.post<any>(UserUrl,JSON.stringify(material))

  };

  public updateOrder(order: any, id: number){
    const UserUrl=environment.apiUrl+"orders/" +id;
    return this.http.put<any>(UserUrl, order);
  };

  public deleteOrder(orderId: number){
    return this.httpService.delete('orders/'+orderId);
  };

  
}
