import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Area } from '@core/models/area.model';
import { ArticleArea } from '@core/models/article-area.model';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MessageService } from '../menssage/message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AreaService {


  constructor(
    private http: HttpClient,
    private messageService:MessageService) { 
    
  }

  public getArea(id: number): Observable<Area>{
    const getPerson = environment.apiUrl+"sections/" + id;
    return this.http.get<Area>(getPerson, httpOptions).pipe(
      tap(_ => this.log('fetched user' + id))
    );
  }


  public getAreas (): Observable<Area[]> {
    const  UserUrl = environment.apiUrl+"sections" ;
    return this.http.get<Area[]>(UserUrl,httpOptions).pipe(
      tap(_ => this.log('fetched users' ))
    ); 
  }

  public addArea (area : Area):Observable<Area> {

    return this.http.post<Area>(environment.apiUrl+"sections", area, httpOptions)
    .pipe(
      tap(_ => this.log('fetched' ))
    )
    ;
  }

  public updateArea (id:string,area : Area) {
    const updateUrl = environment.apiUrl+"sections/"+id;
    return this.http.put<Area>(updateUrl,area,httpOptions)
    .pipe ( 
      tap(_ => this.log('fetched update' ))
    );
  }

  public deleteArea(id: string) {
    const deleteUrl = environment.apiUrl+"sections/"+ id;
    return this.http.delete(deleteUrl, httpOptions)
    .pipe( 
      tap(_ => this.log('fetched delete' + id)),
    );
  }
  private log(message: string) {
    this.messageService.add(`${message}`);
  }


  public findArea (page: number, size: number) {
     let   params = new HttpParams();

    params = params.append('page', String(page));
    params = params.append('limit', String(size));

    return this.http.get(environment.apiUrl+"areas", {params}).pipe(
      map(
        (userData: 
        any) => userData),
    )
  }

  public findArticle(id: number){
    const url = environment.apiUrl+"output/articles/"+ id; 
    return this.http.get<ArticleArea[]>(url,httpOptions)
    .pipe(
      tap(_ => this.log('fetched')),
    )
  }
  

}

