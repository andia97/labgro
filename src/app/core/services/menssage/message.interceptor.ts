import { Injectable} from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class InterceptorMessage implements HttpInterceptor{

    constructor(
        private cookieService:CookieService, 
        private router: Router){
    }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        return next.handle(req)
        .pipe( 
            catchError((error: HttpErrorResponse) =>{
                //const errormen = this.setError(error);
                console.error(error);
                return throwError(error)
            })
        ); 
    }
    /** 
    setError (error: HttpErrorResponse): string{
        let errorMessge  = "error desconocido";
        
            errorMessge = error.error.message
       
            if( error.status == 0){
               errorMessge = error.error.errorMessge
            }
            
    
        return errorMessge;
    }
    */

}
