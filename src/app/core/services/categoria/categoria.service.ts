import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from '@core/models/category.model';
import { environment } from '@evironments/environment';
import { MessageService } from '@core/services/menssage/message.service';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = { 
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }) };

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(
    private http: HttpClient,
    private messageService:MessageService) { 
    
  }

  public getCategory(id: number): Observable<Category>{
    const getPerson = environment.apiUrl+"categories/" + id;
    return this.http.get<Category>(getPerson, httpOptions).pipe(
      tap(_ => this.log('fetched category' + id)),
    );
  }


  public getCategories (): Observable<Category[]> {
    const  UserUrl = environment.apiUrl+"categories" ;
    return this.http.get<Category[]>(UserUrl,httpOptions).pipe(
       tap(_ => this.log('fetched categories' ))
    ); 
  }

  public addCategory (category : Category):Observable<Category> {
  return this.http.post<Category>(environment.apiUrl+"categories", category, httpOptions)
    .pipe(
      tap(_ => this.log('fetched' ))
    )
    ;
  }

  public updateCategory ( id: String  ,article : Category ) {
    const updateUrl = environment.apiUrl+"categories/"+id;
    return this.http.put<Category>(updateUrl,article,httpOptions)
    .pipe ( 
      tap(_ => this.log('fetched update')),
    );
  }

  deleteCategory(id: string) {
    const deleteUrl = environment.apiUrl+"categories/" + id;
    return this.http.delete(deleteUrl, httpOptions)
    .pipe( 
      tap(_ => this.log('fetched delete' + id))
    );
  }
  
  private log(message: string) {
    this.messageService.add(`${message}`);
  }
}
