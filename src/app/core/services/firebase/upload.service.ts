import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  constructor(private storage: AngularFireStorage) {}

  uploadImage(File: any){
    const file = File.target.files[0];
    const filePath = file.name;//idProducto + codigoProducto 
    const fileRef = this.storage.ref(filePath);
    return this.storage
      .upload(filePath, file)
      .snapshotChanges()
      .pipe(
        finalize(() => {return fileRef.getDownloadURL()})
        );
  }
}
