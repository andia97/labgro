import { TestBed } from '@angular/core/testing';

import {  OutputArticleService } from './out-article.service';

describe('OutputArticleService', () => {
  let service: OutputArticleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutputArticleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
