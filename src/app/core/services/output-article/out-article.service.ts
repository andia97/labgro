import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@evironments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { MessageService } from '../menssage/message.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
     'Access-Control-Allow-Headers': 'Origin' }),
  params: new HttpParams()
};


@Injectable({
  providedIn: 'root'
})
export class OutputArticleService {

  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) { }

  apiUrl = environment.apiUrl;
  public getOutputNotes(outputvalue: string, month: number, year: number, page: number): Observable<any> {
    let params = new HttpParams();
    // params = params.append('value', String(value));
    params = params.append('outputvalue', String(outputvalue));
    params = params.append('month', String(month));
    params = params.append('year', String(year));
    params = params.append('page', String(page));

    return this.httpClient.get(this.apiUrl + 'output', {params})
    .pipe(
      map((res: any) => {
        // console.log(res);
        return res.incomes
        }
      )
    );
  }

  public getAreas(): Observable<any> {
    return this.httpService.get('sections')
    .pipe(
      map((res: any) => res)
    );
  }

  public addOutputNote (dataForm: any){
    return this.httpService.post('output/create', dataForm)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res) => res)
    );

  }
  public searchNote(value: string){
    let params = new HttpParams();
    // params = params.append('value', String(value));
    params = params.append('value', String(value));

    return this.httpClient.get(this.apiUrl + 'output', {params})
    .pipe(
      map((res: any) => res.incomes)
    );

  }

  public updateOutputNote (incomeNote: any  ) {
    return this.httpService.put('output/update', incomeNote )
   .pipe(
     map((res) => res.response)
   );
  }

  deleteOutputNote(id: number) {
    const url = 'output/delete/' + id
    return this.httpService.delete(url)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res) => res.data)
    );
  }

  getDataOutNote(id: number) {
    return this.httpService.get('output/' + id)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res: any) => res)
    );
  }


  private handleError(httpResponse: any) {
    // console.log(httpResponse[0]);

    // // const error = httpResponse.error;
    // if (httpResponse.status === 422) {
    //   return throwError('El número de comprobante ya existe, prueba con otro')
    // } else {
      return throwError('Oops, un problema inesperado. Si el error persiste, contactese con administración')
    // }
  }


  getOuputArticle(id:string,page:number,mounthone:number,mounttwo:number, year:number){
    const url = 'outputs/getOutputArticleByDate';
    let params = new HttpParams();

    params = params.append('page', String(page));
    params = params.append('id', String(id));
    params = params.append('mounthone', Number(mounthone));
    params = params.append('mounttwo', Number(mounttwo));
    params = params.append('year', Number(year));
    httpOptions.params = params;

    return this
    .httpClient.get(this.apiUrl + url ,  httpOptions)
    .pipe(
        
    );

  }


  public addOutNoteOrder(dataForm: any,id: string){
    const url = 'output/create/';
    return this.httpService.post(url, dataForm)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res) => res)
    );
  
  }

}
