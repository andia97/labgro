import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@core/models/product.model';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { MessageService } from '../menssage/message.service';

import { HttpService } from '../http/http.service';
import { PresentationProducer } from '@core/models/presentation-producer';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) {

  }



  public addProduct(product: Product):Observable<Product> {
    const UserUrl = environment.apiUrl+"products";
    return this.httpService.post("products", product )
    .pipe(
        tap(_ => this.log('fetched' ))
    );
  }

  public getProduct(id: number): Observable<Product>{
    const UserUrl = environment.apiUrl+"products/"+id;
    return this.http.get<Product>(UserUrl,httpOptions).pipe(
      map((res: any) => res)
    );
  }
  public getProducts (value: any="", month: any, year: any, page: any): Observable<any>{
    let params = new HttpParams();
    // params = params.append('value', String(value));
    params = params.append('value', String(value));
    params = params.append('month', month);
    params = params.append('year', year);
    params = params.append('page', String(page));
    const UserUrl = environment.apiUrl+"products";
    return this.httpClient.get(UserUrl, {params})
    .pipe(
      map((res: any)=>res)
    );
    // return this.http.get<Product[]>(UserUrl,httpOptions);
  }

  public updateProduct (product: Product  ) {
    return this.httpService.put('products', product )
   .pipe(
     map((res) => res.response)
   );
  }

  public deleteProduct(id: number) {
    const deleteUrl = environment.apiUrl+"products/"+ id;
    return this.http.delete(deleteUrl, httpOptions);
  }
  private log(message: string) {
    this.messageService.add(`${message}`);
  }

  /*Produt Articles CRUD*/
  public createProductArticle(productId: number, materialId: number, quantity: number){
    const productMaterial={
      quantity,
      material_id: materialId
    };
    const UserUrl=environment.apiUrl+"product/"+productId+"/materials";
    return this.http.post(UserUrl, JSON.stringify(productMaterial));
  }

  public updateProductArticle(productId: number, materialId: number, quantity: number){
    const productMaterial={
      quantity
    }
    const UserUrl=environment.apiUrl+"product/"+productId+"/materials/"+materialId;
    return this.http.put(UserUrl, JSON.stringify(productMaterial));
  }

  public getProductMaterials(productId:number): Observable<any>{
    const UserUrl = environment.apiUrl+"product/"+productId+"/materials";
    return this.httpClient.get(UserUrl);
  }

  public deleteProductMaterial(productId: number, materialId: number){
    const UserUrl = environment.apiUrl+"product/"+productId+"/materials/"+materialId;
    return this.httpClient.delete(UserUrl);
  }

  /*Produt Presentations CRUD*/
  public getProductPresentations(productId: number){
    const UserUrl = environment.apiUrl+"product/"+productId+"/presentations";
    return this.httpClient.get(UserUrl);
  }

  public createProductPresentation(productId: number, presentationId: number, unit_cost_production: number, unit_price_sale: number){
    const productPresentation={
      unit_cost_production,
      unit_price_sale,
      presentation_unit_id:presentationId
    };
    const UserUrl=environment.apiUrl+"product/"+productId+"/presentations";
    return this.http.post<PresentationProducer>(UserUrl, JSON.stringify(productPresentation));
  }
  public updateProductPresentation(productId: number, presentationId: number, unit_cost_production: number,unit_price_sale: number){
    let params = new HttpParams();
    const productPresentation={
      unit_cost_production,
      unit_price_sale,
    };
    const UserUrl=environment.apiUrl+"product/"+productId+"/presentations/"+presentationId;
    return this.http.put<PresentationProducer>(UserUrl, JSON.stringify(productPresentation));
  }
  public deleteProductPresentation(productId: number, presentationId: number){
    const UserUrl = environment.apiUrl+"product/"+productId+"/presentations/"+presentationId;
    return this.httpClient.delete(UserUrl);
  }


}
