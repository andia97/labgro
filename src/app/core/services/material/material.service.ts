import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@core/models/product.model';
import { environment } from '@evironments/environment';
import { Observable, throwError } from 'rxjs';
import { tap, map, catchError, retry } from 'rxjs/operators';
import { MessageService } from '../menssage/message.service';
import { MaterialProducer } from '@core/models/material-producer';
import { HttpService } from '../http/http.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) {

  }

  public getMaterials(): Observable<MaterialProducer[]>{
    return this.httpService.get('materials');
  };

  public getMaterial(materialId): Observable<MaterialProducer[]>{
    return this.httpService.get('materials/'+materialId);
  };

  public postMaterial(material: any): Observable<any>{
    const UserUrl=environment.apiUrl+"materials";
    return this.http.post<MaterialProducer>(UserUrl,JSON.stringify(material))

  };

  public updateMaterial(material: any, id: number){
    const UserUrl=environment.apiUrl+"materials/"+id;
    return this.http.put<MaterialProducer>(UserUrl, JSON.stringify(material));
  };

  public deleteMaterial(materialId: number){
    return this.httpService.delete('materials/'+materialId);
  };

  public getMaterialIncomes(materialId: number,year: number){
    const UserUrl = environment.apiUrl +"material/"+materialId+"/incomes";
    let params = new HttpParams();
    params = params.append('year', String(year));
    return this.httpClient.get(UserUrl, {params});
  };

  public getMaterialOutputs(materialId: number,year: number){
    const UserUrl = environment.apiUrl +"material/"+materialId+"/outputs";
    let params = new HttpParams();
    params = params.append('year', String(year));
    return this.httpClient.get(UserUrl, {params});
  };

  public verifyStockMaterials(product_id:number, quantity:number) {
    const UserUrl = environment.apiUrl+"verifyMaterials";
    let params = new HttpParams();
    params = params.append('product_id', String(product_id));
    params = params.append('quantity', String(quantity));
    return this.httpClient.get(UserUrl, {params});
  }


}
