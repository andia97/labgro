import { Article } from '@core/models/article.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import Swal from 'sweetalert2';
import { StorageService } from '../storage/storage.service';
import { AuthConstants } from '@core/config/auth-constanst';
import { HttpService } from '../http/http.service';


export interface Orders {
  section_id: number;
  receipt: number;
  order_number: number;
  date_issue: Date;
  details: MaterialOrder[];
}
export interface MaterialOrder {
  material_id: number;
  quantity: number;
}


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  private articles: Article[] | any[] = [];
  private cart = new BehaviorSubject<Article[] | any[]>([]);

  cart$ = this.cart.asObservable();
  constructor(
    private snackBar: MatSnackBar,
    private storageService: StorageService,
    private httpService: HttpService,
  ) {

    this.storageService.get(AuthConstants.SHOPPING_CART)
    .then(
      (articles: Article[] | any[]) => {
        this.articles = articles;
        this.cart.next(this.articles);
        // console.log(this.articles);
      }
      )
      .catch(
        (error: any) => {
          this.articles = [];
          this.cart.next(this.articles);
          // console.log(this.articles);
        }
    )
  }


  addCart(article: Article) {
    // console.log(article);
    // console.log(this.articles);
    let isExist = false;
    if (this.articles.length !== 0 ) {
      if (this.isExist(article.id)) {
        Swal.fire({
          icon: 'warning',
          title: '¡Oops...!',
          text: 'La materia prima/insumo: ' + article.name_article + '  ya existe en el carrito. Selecciona otra materia prima/insumo.',
          showConfirmButton: true,
          confirmButtonColor: '#85CE36'
        });
      } else {
        const dataToForm = {
          article_name: article.name_article,
          material_id: article.id,
          unit: article.unit_measure,
          quantity: 0
        }
        this.articles = [...this.articles, dataToForm];
        this.cart.next(this.articles);
        this.storageService.store(AuthConstants.SHOPPING_CART, this.articles);
        this.snackBar.open(article.name_article + ' agregado al carrito', 'Ok', {
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 2000,
        });
      }
    } else {
      const dataToForm = {
        article_name: article.name_article,
        material_id: article.id,
        unit: article.unit_measure,
        quantity: 0
      }
      this.articles = [...this.articles, dataToForm];
      this.cart.next(this.articles);
      this.storageService.store(AuthConstants.SHOPPING_CART, this.articles);
      this.snackBar.open(article.name_article + ' agregado al carrito', 'Ok', {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 2000,
      });
    }
  }

  editCart(article: Article | any) {
    // console.log(article);
    // console.log(this.articles);
    let isExist = false;
    if (this.articles.length !== 0 ) {
      if (this.isExist(article.material_id)) {
        this.deleteCartAux(article.material_id);
        this.articles = [...this.articles, article];
        this.cart.next(this.articles);
        this.storageService.store(AuthConstants.SHOPPING_CART, this.articles);
        Swal.fire({
          icon: 'success',
          title: '¡Modificado!',
          text: 'Los datos se modificaron correctamente',
          showConfirmButton: false,
          confirmButtonColor: '#85CE36',
          timer: 2000
        });
      }
    }
  }


  isExist(article_id: number): boolean {
    let isExist = false;
    this.articles.forEach((article: Article | any) => {
      if (article.material_id === article_id) {
        isExist = true;
      }
    });
    return isExist;
  }

  deleteCart(article: Article): void {
    const indice = this.articles.indexOf(article);
    console.log(indice);
    this.articles.splice(indice, 1);
    this.cart.next(this.articles);
    this.storageService.store(AuthConstants.SHOPPING_CART, this.articles);
  }

  deleteCartAux(id: number): void {
    this.articles.forEach(element => {
      if (element.material_id === id ) {
        const indice = this.articles.indexOf(element.material_id);
        this.articles.splice(indice, 1);
        this.cart.next(this.articles);
      } else {
        console.log('no ejecutado');
        
      }
    });
  }
  

  sendData(dataForm: any): Observable<any> {
    // console.log(dataForm);
    const data: Orders = {
      section_id: dataForm.section_id,
      receipt: dataForm.receipt,
      order_number: dataForm.order_number,
      date_issue: dataForm.date_issue,
      details: this.constructorMaterialOrder(dataForm.details),
    }
    console.log(data);
    
    return this.httpService.post('orders', data);
  }
  
  clearData(): void {
    this.articles = [];
    this.cart.next(this.articles);
    this.storageService.remove(AuthConstants.SHOPPING_CART);
  }

  constructorMaterialOrder(materials: any[]): MaterialOrder[] {
    let res: MaterialOrder[] = [];
    materials.forEach(element => {
      const material: MaterialOrder = {
        material_id: element.material_id,
        quantity: element.quantity,
      }
      res.push(material);
    });
    return res;
  }
}
