import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@evironments/environment';
import { MessageService } from '@core/services/menssage/message.service';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Article } from '@core/models/article.model';
import { CookieService } from 'ngx-cookie-service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json','Accept': 'application/json', 'Access-Control-Allow-Headers': 'Origin' })
};

@Injectable({
  providedIn: 'root'
})
export class ArticleService {


  constructor(
    private http: HttpClient,
    private messageService:MessageService,
    private cookieService: CookieService,
    ) {

  }

  public getArticle(id: number): Observable<Article>{
    const getPerson = environment.apiUrl+"articles/" + id;
    return this.http.get<Article>(getPerson, httpOptions).pipe(
      tap(_ => this.log('fetched article' + id))
    );
  }


  public getArticles (): Observable<Article[]> {

    const  UserUrl = environment.apiUrl+"articles" ;
    return this.http.get<Article[]>(UserUrl)
    .pipe(
      tap(_ => this.log('fetched articles' ))
    );
  }
  handleError(handleError: any): import("rxjs").OperatorFunction<Article[], Article[]> {
    throw new Error('Method not implemented.');
  }

  public addArticle (article : Article):Observable<Article> {

    return this.http.post<Article>(environment.apiUrl+"articles", article, httpOptions)
    .pipe(
      tap(_ => this.log('fetched' ))
    )
    ;
  }

  public updateArticle (id:string,article : Article) {
    const updateUrl = environment.apiUrl+"articles/"+id;
    return this.http.put<Article>(updateUrl,article,httpOptions)
    .pipe (
      tap(_ => this.log('fetched' ))
    );
  }

  public deleteArticle(id: string) {
    const deleteUrl = environment.apiUrl+"articles/"+ id;
    return this.http.delete(deleteUrl, httpOptions)
    .pipe(
      tap(_ => this.log('fetched')),

    );
  }
  private log(message: string) {
    this.messageService.add(`${message}`);
  }


  public findArticles (page: number, size: number) {
     let   params = new HttpParams();

    params = params.append('page', String(page));
    params = params.append('limit', String(size));

    return this.http.get(environment.apiUrl+"articles", {params}).pipe(
      map(
        (userData:
        any) => userData),
    )
  }
  public getIncomesArticlesHistoric(year: number): Observable<any>{
    const url=environment.apiUrl+"material/incomes_graphics";
    let   params = new HttpParams();

    params = params.append('year', String(year));
    return this.http.get(url,{params});
  }

}
