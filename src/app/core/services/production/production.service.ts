import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@core/models/product.model';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { MessageService } from '../menssage/message.service';

import { HttpService } from '../http/http.service';
import { PresentationProducer } from '@core/models/presentation-producer';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class ProductionService {

  constructor(
    private messageService: MessageService,
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) {

  }

  public getProductionsDates(year: number, month: number){
    const UserUrl = environment.apiUrl+"productions";
    let params=new HttpParams();
    params = params.append('month', month);
    params = params.append('year', year);
    return this.httpClient.get(UserUrl, {params});
  }

  public createProduction(){
    const UserUrl = environment.apiUrl+"productions";
    const date=new Date();
    const dateProduction={
      date_production: `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`
    }
    return this.httpClient.post(UserUrl,JSON.stringify(dateProduction));
  }

  public updateProduction(production_id: number, product_id: number, quantity: any){
    const userUrl = environment.apiUrl+"production/"+production_id+"/products/"+product_id;
    const productionProduct = {
      quantity
    };
    return this.httpClient.put(userUrl, JSON.stringify(productionProduct));
  }

  public deleteProduction(production_id: number, product_id: number){
    const userUrl = environment.apiUrl+"production/"+production_id+"/products/"+product_id;
    return this.httpClient.delete(userUrl);
  }

  public createProductionProducts(production_id: number, product_id: number, quantity: number){
    const UserUrl = environment.apiUrl+"production/"+production_id+"/products";
    const production={
      product_id,
      quantity
    }
    return this.httpClient.post(UserUrl,JSON.stringify(production));
  }

  public createProductionProductPresentations(production_id: number, product_id: number, presentation: any){
    const UserUrl = environment.apiUrl+"production/"+production_id+"/product/"+product_id+"/presentations";
    const presentationProduct={
      presentation_id: presentation.unit.id,
      quantity: presentation.quantity,
      unit_cost_production: presentation.unit_cost_production,
      unit_price_sale: presentation.unit_price_sale
    }
    return this.httpClient.post(UserUrl,JSON.stringify(presentationProduct));
  }

  public updateProductionProductPresentations(production_id: number, product_id: number, presentation_id: number, presentation: any){
    const UserUrl = environment.apiUrl+"production/"+production_id+"/product/"+product_id+"/presentations/"+presentation_id;
    const presentationProduct={
      quantity: presentation.quantity,
      unit_cost_production: presentation.unit_cost_production,
      unit_price_sale: presentation.unit_price_sale
    }
    return this.httpClient.put(UserUrl,JSON.stringify(presentationProduct));
  }

  public deleteProductionProductPresentations(production_id: number, product_id: number, presentation_id: number){
    const UserUrl = environment.apiUrl+"production/"+production_id+"/product/"+product_id+"/presentations/"+presentation_id;
    return this.httpClient.delete(UserUrl);
  }

  public getReportConsolidation(firstDate, endDate){
    const UserUrl = environment.apiUrl+"getConsolidate";
    let params=new HttpParams();
    params = params.append('start_date', firstDate);
    params = params.append('end_date', endDate);
    return this.httpClient.get(UserUrl, {params});
  }

  public getProductionsByProductId(year: number,product_id: number){
    const UserUrl = environment.apiUrl+"productions_details/"+product_id;
    let params=new HttpParams();
    params = params.append('year', year);
    return this.httpClient.get(UserUrl, {params});
  }

  public getProductionsByDate(date: string){
    const UserUrl = environment.apiUrl+"details/productions";
    let params = new HttpParams();
    params = params.append('date',date);
    return this.httpClient.get(UserUrl, {params});
  }

  public getProductionSummary(firstDate, endDate){
    const userUrl = environment.apiUrl+"getSummaryProduction";
    let params = new HttpParams();
    params = params.append('start_date',firstDate);
    params = params.append('end_date',endDate);
    return this.httpClient.get(userUrl, {params});
  }

  public getProductionDetails(firstDate, endDate){
    const userUrl = environment.apiUrl+"getDetailProduction";
    let params = new HttpParams();
    params = params.append('start_date',firstDate);
    params = params.append('end_date',endDate);
    return this.httpClient.get(userUrl, {params});
  }

}
