import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '@evironments/environment';

import { User } from '@core/models/user.model';
import { MessageService } from '@core/services/menssage/message.service';

  const httpOptions = { 
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }) };


@Injectable({
  providedIn: 'root'
})
export class UserService {

 
  constructor(
    private http: HttpClient,
    private messageService:MessageService) { 
    
  }



  public getUser(id: number): Observable<User>{
    const getPerson = environment.apiUrl+"/user" + id;
    return this.http.get<User >(getPerson, httpOptions).pipe(
      tap(_ => this.log('fetched user' + id)),
      catchError(this.handleError<User>('getPerson id' + id ))
    );
  }


  public getUsers(): Observable<User[]> {
    const  UserUrl = environment.apiUrl+"/user" ;
    return this.http.get<User[]>(UserUrl,httpOptions).pipe(
      tap(_ => this.log('fetched users' )),
      catchError(this.handleError('getUsers',[]))
    ); 
  }

  public addUser(user : User):Observable<User > {
    return this.http.post<User>(environment.apiUrl+"/user", user, httpOptions)
    .pipe(
      catchError(this.handleError('addUserError', user ))
    )
    ;
  }

  public updateUser( user : User) {
    const updateUrl = environment.apiUrl+"/user" + user.email;
    return this.http.put<User>(updateUrl, httpOptions)
    .pipe ( 
      tap(_ => this.log('fetched update' )),
      catchError(this.handleError('updateUserError', user ))
    );
  }

  deleteUser(id: string) {
    const deleteUrl = environment.apiUrl+"/user" + id;
    return this.http.delete(deleteUrl, httpOptions)
    .pipe( 
      tap(_ => this.log('fetched delete' + id)),
      catchError(this.handleError('deleteUserEror', id  ))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
  
  private log(message: string) {
    this.messageService.add(`PersonService: ${message}`);
  }

  

}
