import { TestBed } from '@angular/core/testing';

import { IncomesArticleService } from './incomes-article.service';

describe('IncomesArticleService', () => {
  let service: IncomesArticleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncomesArticleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
