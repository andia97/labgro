import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@evironments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { MessageService } from '../menssage/message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json','Accept': 'application/json', 'Access-Control-Allow-Headers': 'Origin' }),
  params: new HttpParams()
};

@Injectable({
  providedIn: 'root'
})
export class IncomesArticleService {

  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) { }

  apiUrl = environment.apiUrl;
  public getIncomesNotes(value: string, month: number, year: number, page: number): Observable<any> {
    let params = new HttpParams();
    params = params.append('value', String(value));
    params = params.append('incomevalue', String(value));
    params = params.append('month', String(month));
    params = params.append('year', String(year));
    params = params.append('page', String(page));
    return this.httpClient.get(this.apiUrl + 'incomes', {params})
    .pipe(
      map((res: any) =>{
        // console.log(res);
        return res.incomes
      })
    );
  }

  public getIncomeNote(id: number): Observable<any> {
    let params = new HttpParams();
    // params = params.append('value', String(value));
    params = params.append('id', String(id));
    return this.httpClient.get(this.apiUrl + 'getDetailsIncome', {params})
    .pipe(
      map((res: any) => res.details)
    );
  }

  public addIncomeNote (dataForm: any){
    return this.httpService.post('incomes',dataForm)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res) => {
        return res.response;
  })
    );

  }
  public searchNote(value: string){
    let params = new HttpParams();
    // params = params.append('value', String(value));
    params = params.append('incomevalue', String(value));

    return this.httpClient.get(this.apiUrl + 'incomes', {params})
    .pipe(
      map((res: any) => res.incomes)
    );

  }

  public updateIncomeNote (incomeNote: any  ) {
    return this.httpService.put('incomes', incomeNote )
   .pipe(
     map((res) => res.response)
   );
  }

  deleteIncomeNote(id: number) {
    return this.httpService.delete('incomes/' + id)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res: any) => res.data)
    );
  }

  getDataIncomeNote(id: number) {
    return this.httpService.get('incomes/' + id)
    .pipe(
      catchError(
        this.handleError
      ),
      map((res: any) => res)
    );
  }


  private handleError(httpResponse: any) {
    // console.log(httpResponse[0]);

    // // const error = httpResponse.error;
    // if (httpResponse.status === 422) {
    //   return throwError('El número de comprobante ya existe, prueba con otro')
    // } else {
      return throwError('Oops, un problema inesperado. Si el error persiste, contactese con administración')
    // }
  }

  getIncomeArticle(id:string,mounthone:number,mounttwo:number, year:number): Observable<any>{
     const url = "income/getIncomeArticleByDate" ;

    let params = new HttpParams();

     params = params.append('id', String(id));
     params = params.append('mounthone', Number(mounthone));
     params = params.append('mounttwo', Number(mounttwo));
     params = params.append('year', Number(year));
     httpOptions.params = params;


     return this.httpClient.get(this.apiUrl + url, {params} )
     .pipe(

     );
  }


  public peripheralReport(trimestre: string, year: number): Observable<any>{
    const url=environment.apiUrl+"peripheralReport";
    let params = new HttpParams();
    params = params.append('trimestre', trimestre);
    params = params.append('year', year);
    return this.httpClient.get(url,{params});
  }
}
