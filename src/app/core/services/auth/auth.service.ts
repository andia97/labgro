import {HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from '@core/config/auth-constanst';
import { AuthUser } from '@core/models/auth.model';
import { User } from '@core/models/user.model';
import { CookieService } from 'ngx-cookie-service';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { StorageService } from '../storage/storage.service';
import {environment} from "@evironments/environment.prod";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private cookieService: CookieService,
  ) { }

  login(dataForm: AuthUser): Observable<any>{
    return this.httpService.post('login', dataForm)
    .pipe(
     map(
        (res: any) => {
          this.storageService.store(AuthConstants.USER, res.user)
          this.cookieService.set(AuthConstants.AUTH, res.token);
          this.storageService.store(AuthConstants.AUTH, res.token);
          this.storageService.store(AuthConstants.ROL, res.role_id)
          return res.role_id;
        },
    ));
  }

  logout(): Observable<any> {
    return this.httpService.get('logout')
    .pipe(
      map(
         (res: any) => {
          console.log(res);
          return res;
         },(err)=>{
           console.log("errorrrr")
        }
     ));
  }

  refreshToken(token: string) {
    const AUTH_API = environment.apiUrl;

    //return this.httpClient.post(AUTH_API + 'refresh', {
    //  refreshToken: token
    //}, httpOptions);
  }

}
