import { Component} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthConstants } from '@core/config/auth-constanst';
import { AuthUser } from '@core/models/auth.model';
import { User } from '@core/models/user.model';
import { AuthService } from '@core/services/auth/auth.service';
import { MessageService } from '@core/services/menssage/message.service';
import { StorageService } from '@core/services/storage/storage.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  // show message variable incomeing backend
  errorMessage: string = '';

  // password variable: hide or show
  hide: boolean = true;

  // httpResponse error vaiable: hide or show
  stateError: Boolean = false;

  // progress bar vaiable
  state: boolean = false;

  // User data for
  loginForm!: FormGroup;


  //Validators when user touchet type data on input
  get email():any {
    return this.loginForm.get('email');
  }

  get password():any {
    return this.loginForm.get('password');
  }

  // Messages for validators
  public errorsMessages = {
    email: [
      { type: 'required', message: 'Correo electrónico es obligatorio.' },
      { type: 'minlength', message: 'Debe introducir como mínimo 15 caracteres.'},
      { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
      { type: 'email', message: 'Debe ingresar un correo electrónico con el siguiente formato: elemplo@ejemplo.com.'},
      { type: 'pattern', message: 'Solo se permite letras de la (a-z), caracteres numéricos del (0-9), y el caracter especial del punto "."'},
    ],
    password: [
      { type: 'required', message: 'La contraseña es obligatorio.' },
      { type: 'minlength', message: 'Debe introducir como mínimo 8 caracteres.'},
      { type: 'maxlength', message: 'Debe introducir como máximo 25 caracteres.'},
    ],
  }

  constructor(
    private formBuilder: FormBuilder,
    private loginService: AuthService,
    private router: Router,
    private storageService: StorageService,
    private serviceMessage : MessageService,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.storageService.get(AuthConstants.AUTH).then(
      (token: string)=>{
        if(token){
          this.storageService.get(AuthConstants.ROL).then(
            (rol: number) => {
              this.verifyRol(rol);
            }
          );
        }
      }
    ).catch(err=>{

    })
  }

  private buildForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['',
      [
        Validators.required,
        Validators.email,
        Validators.minLength(15),
        Validators.maxLength(50),
        Validators.pattern('[a-z0-9.A-Z@]*')
      ]],
      password: ['',
      [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(25)
      ]]
    });
  }


  login() {
    this.state = true;
    const userAuth: AuthUser = this.loginForm.value;
    this.loginService.login(userAuth).subscribe(
      (res) => {
        this.verifyRol(res);
        Swal.fire({
          position: 'center',
          icon: 'success',
          iconColor: '#85CE36',
          title: 'Bienvenido',
          showConfirmButton: false,
          timer: 2000
        })
        this.state = false;
      },
      (error: any) => {
        this.state = false;
        this.stateError = true;
        console.error(error);
        const errorAux: any = this.serviceMessage.get();
        this.errorMessage = errorAux.message;
      }
    );
  }

  verifyRol(rol: number): void {

    if( rol === 	3 ){
      this.router.navigate(['warehouse']);
      this.storageService.store(AuthConstants.TITLE_NAV, 'Articulos');
    }
    
    if( rol === 4 ){
      this.router.navigate(['producer']);
      this.storageService.store(AuthConstants.TITLE_NAV, 'Inicio');
    }

    if( rol === 5 ){
      this.router.navigate(['producer']);
    }

    if( rol === 2 ){
      this.router.navigate(['producer']);
    }
  }
}
