import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuardGuard } from '@shared/guards/user-guard.guard';
import { UserGuestGuard } from '@shared/guards/user-guest.guard';

import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '' , component: LoginComponent},
  // {
  //   path: '',
  //   canActivate:[UserGuestGuard],
  //   component: LayoutComponent,
  // },

  {
    path: 'warehouse',
    canActivate: [ UserGuardGuard ],
    data:{
      
    },
    loadChildren: () => import('./warehouse/warehouse.module').then(m => m.WarehouseModule)
  },

  {
    path: 'producer',
    canActivate: [ UserGuardGuard ],
    loadChildren: () => import('./producer/producer.module').then(m => m.ProducerModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
