import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { SearchPipe } from './pipes/search.pipe';
import { AppRoleDirective } from './directives/app-role.directive';
import { HeaderComponent } from './components/header/header.component';
import { SideNavService } from './services/side-nav.service';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { MaterialModule } from '@material/material.module';
import { ConstructionComponent } from './components/construction/construction.component';
import { YearComponent } from './components/year/year.component';
import { MonthComponent } from './components/month/month.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRolePDirective } from './directives/app-role-p.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { KardexConfigurationComponent } from './components/kardex-configuration/kardex-configuration.component';



@NgModule({
  declarations: [
    MainNavComponent,
    SearchPipe,
    AppRoleDirective,
    AppRolePDirective,
    HeaderComponent,
    EmptyStateComponent,
    ConstructionComponent,
    YearComponent,
    MonthComponent,
    AutofocusDirective,
    KardexConfigurationComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule
  ],
  
  exports: [ 
    AppRoleDirective,
    AppRolePDirective,
    MainNavComponent,
    HeaderComponent,
    EmptyStateComponent,
    ConstructionComponent,
    YearComponent,
    MonthComponent,
    AutofocusDirective,
    SearchPipe
  ],

  providers:[
    SideNavService,
  ]
})
export class SharedModule { }
