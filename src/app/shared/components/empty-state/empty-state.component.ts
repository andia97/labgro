import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

export interface EmptyData {
  title: string,
  subtitle: string,
  btn: string,
  assets: string,
}

@Component({
  selector: 'app-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.css']
})
export class EmptyStateComponent implements OnInit {
  @Input() emptyData!: EmptyData;
  @Output() btnNewEmitter: EventEmitter<any> = new EventEmitter();
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  actionBtn(): void {
    this.btnNewEmitter.emit(true);
  }


}
