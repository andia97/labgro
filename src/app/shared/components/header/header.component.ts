import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from '@core/config/auth-constanst';
import { User } from '@core/models/user.model';
import { AuthService } from '@core/services/auth/auth.service';
import { StorageService } from '@core/services/storage/storage.service';
import { SideNavService } from '@shared/services/side-nav.service';
import { CookieService } from 'ngx-cookie-service';
import { Observable, timer } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnChanges {

  @Input() title!: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  user!: User | any;
  state: boolean = false;

  constructor(
    private storageService: StorageService,
    private breakpointObserver: BreakpointObserver,
    private sideNavService: SideNavService,
    private router: Router,
    private cookieService: CookieService,
    private authService: AuthService,
  ) {
    this.storageService.get(AuthConstants.USER).then(
      (user: User) => {
        // console.log(user);
        this.user = user;
      }
    );
  }

  ngOnInit(): void {
    this.getNotifications();
  }
  getNotifications() {
    throw new Error('Method not implemented.');
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // console.log(this.title);
    
  }

  clickMenu() {
    this.sideNavService.toggle();
  }

  logOut(): void {
    this.state = true;
    this.authService.logout().subscribe(
      (res: any) => {
        this.state = false;
        this.storageService.clear();
        this.cookieService.deleteAll();
        this.router.navigate(['/']);
      },
      (error: any) => {
        this.storageService.clear();
        this.cookieService.deleteAll();
        this.router.navigate(['/']);
        console.warn(error);
      },
    );
  }

}
