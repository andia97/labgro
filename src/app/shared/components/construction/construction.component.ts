import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-construction',
  templateUrl: './construction.component.html',
  styleUrls: ['./construction.component.css']
})
export class ConstructionComponent implements OnInit {

  @Input() title: any;
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  changeRoute(route: string): void {
    this.router.navigateByUrl('warehouse/' + route, {skipLocationChange: true})
    .then(
      () => {
        this.router.navigate(['warehouse/'])
      }
    );
  }
}
