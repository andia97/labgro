import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

const moment = _rollupMoment || _moment;
moment.locale('es');
export const MY_FORMATS = {
  parse: {
    dateInput: 'MMMM',
  },
  display: {
    dateInput: 'MMMM',
    monthYearLabel: 'MMMM',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM',
  },
};


@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class MonthComponent implements OnInit {

  dataForm: FormGroup = this.formBuilder.group({
    date: [{
      value: moment(),
      disabled: true
    }]
  })
  
  month!: number;
  @Output() emitterMonthEvent: EventEmitter<number> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.dataForm.controls.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.dataForm.controls.date.setValue(ctrlValue);
    datepicker.close();
    const formatOnlyMonth = ctrlValue.format('MMMM');
    // console.log(form atOnlyMonth);
    
    switch (formatOnlyMonth) {
      case 'enero':
        this.month = 1;
        break;
      case 'febrero':
        this.month = 2;
        break;
      case 'marzo':
        this.month = 3;
        break;
      case 'abril':
        this.month = 4;
        break;
      case 'mayo':
        this.month = 5;
        break;
      case 'junio':
        this.month = 6;
        break;
      case 'julio':
        this.month = 7;
        break;
      case 'agosto':
        this.month = 8;
        break;
      case 'septiembre':
        this.month = 9;
        break;
      case 'octubre':
        this.month = 10;
        break;
      case 'noviembre':
        this.month = 11;
        break;
      case 'diciembre':
        this.month = 12;
        break;
    
      default:
        break;
    }
    this.emitterMonthEvent.emit(this.month);
  }
  

}
