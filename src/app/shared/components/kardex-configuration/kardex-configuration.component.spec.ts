import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KardexConfigurationComponent } from './kardex-configuration.component';

describe('KardexConfigurationComponent', () => {
  let component: KardexConfigurationComponent;
  let fixture: ComponentFixture<KardexConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KardexConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KardexConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
