import { Component, EventEmitter, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LoginService } from '@core/services/login/login.service';
import { JsonPipe } from '@angular/common';
import { AuthConstants } from '@core/config/auth-constanst';
import { StorageService } from '@core/services/storage/storage.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent  {
  
  typesOfShoes: string[] = ['Boots'];
  rol!: number;
  @Output() titleNewEvent: EventEmitter<string> = new EventEmitter();
 
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );



  constructor(
    private breakpointObserver: BreakpointObserver,
    private loginservice: LoginService,
    private storageService: StorageService,
  ) {
    this.storageService.get(AuthConstants.ROL).then(
      (rol: number) => {
        this.rol = rol;
      }
    );
  }

  cahngeTitle(value: string) {
    this.titleNewEvent.emit(value);
    this.storageService.store(AuthConstants.TITLE_NAV, value);
    if (value === 'Notas de entrega' || value === 'Notas de recepción') {
      this.storageService.remove(AuthConstants.DATA_IN_NOTE);
      this.storageService.remove(AuthConstants.DATA_IN_ARTICLE);
    }
  }

}