import { Pipe, PipeTransform } from '@angular/core';
import { Article } from '@core/models/article.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(articles: Article[], searchValue: string, category: string): Article[] {
    if (!articles || !searchValue || !category) {
      return articles;
    }
    console.log(searchValue, category);
    
    return articles.filter((article: Article) => {
      article.name_article.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()) ||
      article.cod_article.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      article.name_article.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()) ||
      article.name.toLocaleLowerCase().includes(category.toLocaleLowerCase())
    });
  }

}
