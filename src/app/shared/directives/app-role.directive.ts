import {Directive, ElementRef,Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import { AuthConstants } from '@core/config/auth-constanst';

import {Rol } from '@core/models/rol.mudel'
import { LoginService } from '@core/services/login/login.service';
import { MainNavComponent } from '@shared/components/main-nav/main-nav.component';

@Directive({
  selector: '[AppRole]'
})
export class AppRoleDirective implements OnInit {
  
  private mainNav! : MainNavComponent
  private rol! : string;
  public user!:  {};

  constructor( 
    private loginService: LoginService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) 
  {
  
 }
  ngOnInit(): void {
   
   let user = JSON.stringify(localStorage.getItem(AuthConstants.ROL));
   //console.log( localStorage.getItem( 'current_rol'));
 
  }
 
  @Input() set AppRole(val: number ){
    // console.log(val);
    
      if (val === 3) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
  }
  


 
}
