import { Directive, ElementRef, Input, AfterViewInit, AfterContentInit } from '@angular/core';

@Directive({
  selector: '[appAutofocus]'
})
export class AutofocusDirective implements AfterContentInit {
  @Input() public autoFocus: boolean = false;
  constructor(
    private el: ElementRef
  ) { }

  ngAfterContentInit(): void {
    this.el.nativeElement.focus();
  }
}
